FRONTEND_URL="http://10.0.100.31"
FRONTEND_BASEPATH="/fdi/"

BACKEND_URLAPI="http://10.0.100.32:4000"

# ==================================
sudo apt-get install ca-certificates
sudo apt-get install build-essential
sudo apt-get install curl

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm install v10.24.1

npm install

git config --global user.name "usuario"
git config --global user.email usuario@yopmail.com
git config --list

echo "{" > config.json
echo "  \"server\": \"${BACKEND_URLAPI}\"," >> config.json
echo "  \"serverAuth\": \"${BACKEND_URLAPI}\"," >> config.json
echo "  \"timeSessionExpired\": 30," >> config.json
echo "  \"debug\": false," >> config.json
echo "  \"onbeforeunload\": false," >> config.json
echo "  \"port\": 3100," >> config.json
echo "  \"subDomain\": \"${FRONTEND_BASEPATH}\"," >> config.json
echo "  \"portDev\": 8880" >> config.json
echo "}" >> config.json


npm run build

rm -rf produccion

mv dist produccion
