#!/bin/bash
# ACMETv3.1.0
# Script para actualizar codigo fuente desde repositorio

export GIT_SSL_NO_VERIFY=true
FECHA=`date +"%Y%m%d-%H%M%S"`
REPO='origin'
# Extras
## Configuracion para Para obtener backups enlinea
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color


## Configuracion por defecto de ramas y carpetas para actualizar codigo fuente
RAMAFRONT='develop'
DIRFRONTEND='/home/usuario/fdi-proyectos-frontend'

RAMABACK='develop'
DIRBACKEND='/home/usuario/fdi-proyectos-backend'
DIRFILES='/home/usuario/fdi-proyectos-backend/uploads'
NAMEBACKEND='seguimiento-api'

## Configuracion por defecto para obtener backups
BACKUPURL='/spukcab'
BACKUPDIR='/home/usuario/backups'
USER='usuario'
DBASE='fdi_seguimiento'
HOST='localhost'

#clear
SOURCE="${BASH_SOURCE[0]}"
SOURCEDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd $SOURCEDIR
pwd
if [ -f "actualizar.conf.sh" ]; then
  source actualizar.conf.sh
  echo ""
  echo "---> Utilizando archivo configuración personalizada."
fi

# Obtiene ultimos cambios del repositorio, si existe algun error retorna a la version anterior
frontend() {
  LAST_COMMIT=$(git log --pretty=format:'%h' -n 1)
  echo "Ultimo commit: $LAST_COMMIT"
  git log --pretty=format:'%C(bold blue)%h %Cgreen(%cr) %Creset %s' -n 1
  echo -e "        ${GREEN}Usuario de GITLAB${NC}"
  git pull $REPO $1
  if [ $? -eq 0 ]; then
    NEW_COMMIT=$(git log --pretty=format:'%h' -n 1)
    echo "$LAST_COMMIT != $NEW_COMMIT"
    if [ "$LAST_COMMIT" != "$NEW_COMMIT" ]; then
      NEW_PACKAGES=$(git diff --name-only $LAST_COMMIT..$NEW_COMMIT | grep "package\.json")
      echo "Nuevos paquetes? $NEW_PACKAGES"
      if [ "$NEW_PACKAGES" != "" ]; then
        npm install # Si se necesita instalar nuevas dependencias realizarlo manualmente
      fi
      #rm -rf dist
      mv dist dist.bak
      npm run build
      if [ -d "dist" ]; then
        tar -czf produccion.$FECHA.tar.gz produccion
        if [ -e produccion.$FECHA.tar.gz ]; then
          rm -rf produccion
          cp -R dist produccion
          rm -rf dist.bak
        else
          echo -e "${RED}-- No se puede crear el backup de produccion! --${NC}"
        fi
      else
        echo -e "${RED}-- Existe un error en el codigo fuente! --${NC}"
        mv dist.bak dist
        git reset --hard $LAST_COMMIT
      fi
      echo ""
    else
      echo -e "${CYAN}No hay cambios en el codigo. ${NC}"
      echo ""
      return 1
    fi
  else
    echo -e "${RED}Error al conectarse con gitlab.geo.gob.bo ${NC}"
    echo ""
    return 1
  fi
}

backend() {
  LAST_COMMIT=$(git log --pretty=format:'%h' -n 1)
  echo "Ultimo commit: $LAST_COMMIT"
  git log --pretty=format:'%C(bold blue)%h %Cgreen(%cr) %Creset %s' -n 1
  echo -e "        ${GREEN}Usuario de GITLAB${NC}"
  git pull $REPO $1
  if [ $? -eq 0 ]; then
    NEW_COMMIT=$(git log --pretty=format:'%h' -n 1)
    echo "$LAST_COMMIT != $NEW_COMMIT"
    if [ "$LAST_COMMIT" != "$NEW_COMMIT" ]; then
      NEW_PACKAGES=$(git diff --name-only $LAST_COMMIT..$NEW_COMMIT | grep "package\.json")
      echo "Nuevos paquetes? $NEW_PACKAGES"
      if [ "$NEW_PACKAGES" != "" ]; then
        npm install
      fi
      NEW_MIGRATIONS=$(git diff --name-only $LAST_COMMIT..$NEW_COMMIT | grep "migrations")
      echo "Migraciones? $NEW_MIGRATIONS"
      if [ "$NEW_MIGRATIONS" != "" ]; then
        npm run update
      fi
      pm2 status
    else
      echo -e "${CYAN}No hay cambios en el codigo. ${NC}"
      echo ""
      return 1
    fi
  else
    echo -e "${RED}Error al conectarse con gitlab.geo.gob.bo ${NC}"
    echo ""
    return 1
  fi
  echo "Este Script esta en desarrollo. puede que se necesiten ejecutar comandos extras..."
  echo -e "Ejemplo: ${GREEN}pm2 restart App_name${NC}"
  echo ""
}


# Esta parte se debe modificar de acuerdo al directorio, donde se encuentra este script y los proyectos
ide_frontend() {
  echo ""
  echo "===== FRONTEND ====="
  cd
  cd $DIRFRONTEND
  if [ $? -eq 0 ]; then
    pwd
    git checkout $RAMAFRONT
    frontend $RAMAFRONT;
    if [ $? -eq 0 ]; then
      # Ejecutar alguna accion si se desplego correctamente.
      echo -e "Estado ${GREEN}OK${NC}"
      # pm2 reload registro-frontend
    fi
  else
    echo ""
    echo -e "${RED} Error ${NC} no existe el directorio del codigo fuente"
  fi
}

PROCESS="$( pm2 status | grep "$NAMEBACKEND" )"
ide_backend() {
  echo ""
  echo "===== BACKEND ====="
  cd
  cd $DIRBACKEND
  if [ $? -eq 0 ]; then
    pwd
    if [ "$PROCESS" != "" ]; then
      echo -e "$PROCESS ${GREEN}OK${NC}"
      git checkout $RAMABACK
      backend $RAMABACK;
      if [ $? -eq 0 ]; then
        pm2 reload $NAMEBACKEND
      fi
    else
      echo ""
      echo -e "${RED} Error ${NC} no esta registrado el proceso '$NAMEBACKEND' en PM2"
    fi
  else
    echo ""
    echo -e "${RED} Error ${NC} no existe el directorio del codigo fuente"
  fi
}


## Para obtener backups enlinea
ide_backupdb() {
  if [ -d "$BACKUPDIR" ]; then
    echo "Backup SQL"
    echo -e "${GREEN}Usuario:${NC} $USER"
    #/usr/bin/pg_dump --host localhost --username "postgres" --role "postgres"  --format plain --encoding UTF8 --verbose --file "$BACKUPDIR/backup_s$FECHA.sql" "dev_upre"
    cd $BACKUPDIR
    /usr/bin/pg_dump --host $HOST --username $USER --role $USER  --format plain --encoding UTF8 --file "$BACKUPDIR/backup_s$FECHA.sql" "$DBASE"
    if [ $? -eq 0 ]; then
      pwd
      tar -czf "$BACKUPDIR/backup_s$FECHA.sql.tar.gz" "backup_s$FECHA.sql"
      rm "backup_s$FECHA.sql"

      if [ -d "$DIRFILES" ]; then
        echo "Backup Archivos"
        DIRNAME="$( dirname "$DIRFILES" )"
        DIRBASE="$( basename "$DIRFILES" )"
        cd $DIRNAME
        pwd
        tar -czf "$BACKUPDIR/backup_a$FECHA.tar.gz" $DIRBASE/
        ls -lah $BACKUPDIR
      else
        echo -e "${RED}No existe el directorio de archivos.${NC}"
      fi

      echo -e "${GREEN}Descargar archivos:${NC}"
      echo "SQL      -> [url-api] $BACKUPURL/backup_s$FECHA.sql.tar.gz"
      if [ -d "$DIRFILES" ]; then
        echo "ARCHIVOS -> [url-api] $BACKUPURL/backup_a$FECHA.tar.gz"
      fi
      echo -e "${GREEN}Eliminar archivos si/no [si]: ${NC}"
      read linea
      if [ "$linea" == "no" ]; then
        echo "No se elimino ningun archivo"
      else
        echo "Eliminado archivos de backup"
        rm "$BACKUPDIR/backup_s$FECHA.sql.tar.gz"
        if [ -d "$DIRFILES" ]; then
          rm "$BACKUPDIR/backup_a$FECHA.tar.gz"
        fi
      fi
    else
      echo -e "${RED}Revise la configuración base de datos.${NC}"
    fi
  else
   echo -e "${RED}No existe la carpeta para backups.${NC}"
  fi
}

while :
  do
    echo ""
    echo -e  "${CYAN}============================================="
    echo -e "${GREEN}              MENU DE OPCIONES${NC}"
    echo ""
    echo -e "        ${GREEN}Escoja una opción${NC}"
    echo "1. Actualizar Front"
    echo "2. Actualizar Back"
    echo "8. Backup DB/Archivos"
    echo "9. Ver uso de RAM"
    echo "0. Salir"
    echo -n "Número? [1 - 5]: "
    read opcion
    case $opcion in
      1) echo "Actualizando Frontend ...";
        ide_frontend;;
      2) echo "Actualizando Backend ..."
        ide_backend;;
      0) echo "...";
        exit 1;;
      8) echo "Obteniendo Backups ..."
        ide_backupdb;;
      9) echo "El uso de RAM:";
        free;;
      *) echo "$opcion no es una opcion válida.";
        echo "Presiona una tecla para continuar...";
        read foo;;
    esac
  done
