'use strict';

import config from '../../config.json';

const server = config.server;
const domain = config.subDomain;

const Config = angular
  .module('app.config', [])
  .constant('appName', 'FDI') // prefijo del sistema para el Storage y Sessiones
  .constant('systemName', 'Sistema de Proyectos  SISPRO')
  .constant('timeSessionExpired', config.timeSessionExpired) //Tiempo en minutos para que la sesión se cierre automáticamente si existe i$
  .constant('onbeforeunload', config.onbeforeunload)
  .constant('debug', config.debug)
  .constant('PageNoLogin', ['login', 'restablecer-contrasenia', 'consultasqr']) // Rutas que no requieren autenticación
  .constant('authUrl', `${config.serverAuth}/autenticar`) // URL para autenticación
  .constant('authEmpresasUrl', `${config.serverAuth}/autenticar/empresa`) // URL para autenticación de empresas
  .constant('apiUrl', `${server}/api/v1/`) // Rest principal del sistema
  .constant('apiUrlPublic', `${server}/public/`) // URL publica del sistema
  .constant('baseUrl', `${server}/`) // Ruta base
  .constant('subDomain', `${domain}`)
  .constant('decimales', 2) // Número de decimales para campos numéricos
  .name;

export default Config;
