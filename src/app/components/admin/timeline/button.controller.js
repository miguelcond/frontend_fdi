'use strict';

import modalTimelineController from './modal/timeline.controller';
import modalTimelineTemplate from './modal/timeline.html';

class ModalTimelineController {
  constructor(Modal) {
    'ngInject';
    this.Modal = Modal;
  }

  $onInit() {
  }

  verRegistro() {
    this.Modal.show({
      template: modalTimelineTemplate,
      controller: modalTimelineController,
      data: this.proyecto,
      size: 'lg'
    });
  }

}

export default ModalTimelineController;
