'use strict';

import angular from 'angular';
import CarteraComponent from './cartera.component';

const Cartera = angular
  .module('app.admin.cartera', [])
  .component('carteraComponent', CarteraComponent)
  .config(($stateProvider) => {
    'ngInject';
    $stateProvider.state('cartera', {
      url: '/cartera',
      component: 'carteraComponent',
    });
    //$urlRouterProvider.otherwise('/');
  })
  .name;

export default Cartera;
