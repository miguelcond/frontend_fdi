'use strict';

class CarteraController {
  constructor(DataService, Message, $state) {
    'ngInject';
    this.DataService = DataService;
    this.Message = Message;
    this.$state = $state;
  }
  $onInit() {
    this.DataService.get(`cartera/`,{}).then(response=>{
      if(response){
        this.carteras = response.datos;
      }
    })

  }
  guardar() {
    const datos = {
      num_cartera: this.cartera,
      descripcion: this.descripcion,
      _usuario_creacion: 1
    };
    this.DataService.post(`cartera/`,datos)
      .then(response=>{
        if(response){
          this.Message.success('Registrado Correctamente...')
          this.$state.reload();
        }
      })
  }
  // ...
  metodoN() {
  }
}
export default CarteraController;
