'use strict';

import controller from './cartera.controllers';
import template from './cartera.html';

const carteracomponent = {
  controller,
  template,
};

export default carteracomponent;
