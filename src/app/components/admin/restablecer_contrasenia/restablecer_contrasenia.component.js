'use strict';

import controller from './restablecer_contrasenia.controller';
import template from './restablecer_contrasenia.html';

const LoginComponent = {
    bindings: {},
    controller,
    template
};

export default LoginComponent;