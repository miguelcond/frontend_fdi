'usr strict';

import controller from './usuarios.controller';
import template from './usuarios.html';

const UsuariosComponent = {
  bindings: {},
  controller,
  template
};

export default UsuariosComponent;
