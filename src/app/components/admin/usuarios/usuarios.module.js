'use strict';

import usuariosComponent from './usuarios.component';
import Usuario from './usuario/usuario.module';
import reconfirmacionUsuarioComponent from './reenviar-confirmacion/reconfirmacion.component';
import UsuariosService from './usuarios.service';

const usuarios = angular
    .module('app.usuarios', [
      Usuario
    ])
    .component('usuarios', usuariosComponent)
    .component('reconfirmacionUsuario', reconfirmacionUsuarioComponent)
    .service('Usuarios', UsuariosService)
    .config(($stateProvider) => {
      $stateProvider
          .state('usuarios', {
            url: '/usuarios',
            component: 'usuarios'
          });
    })
    .name;

export default usuarios;
