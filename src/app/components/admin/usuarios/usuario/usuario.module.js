'use strict';

import usuarioComponent from './usuario.component';

const usuario = angular
    .module('app.usuario', [])
    .component('usuario', usuarioComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('usuario', {
            url: '/usuario/:idUsuario',
            component: 'usuario'
          });
    })
    .name;

export default usuario;
