'use strict';

class UsuariosController {
  constructor($state, $log, DataService, Modal, Message) {
    'ngInject';
    this.$state = $state;
    this.$log = $log;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
  }

  $onInit() {
    // this.getProyecto();
    this.cargarTabla();
  }

  cargarTabla() {
    var colors = {
      INACTIVO: 'warning',
      // ENVIADO: 'primary',
      ACTIVO: 'success',
      // INACTIVO: 'warning',
      // OBSERVADO: 'danger',
    };
    this.usuarios = {
      url: `usuario`,
      queryExtra: { fid_proyecto: this.idProyecto, origen: 'bandeja' },
      data: {
        formly: [
          {
            key: 'id_usuario',
            type: 'input',
            templateOptions: {
              label: 'ID',
              type: 'number'
            }
          },
          // {
          //   key: 'fid_persona',
          //   type: 'fid_persona',
          //   templateOptions: {
          //     label: 'ID proyecto',
          //     type: 'number'
          //   }
          // },
          {
            key: 'usuario',
            type: 'input',
            templateOptions: {
              label: 'Usuario',
              type: 'text'
            }
          },
          {
            key: 'numero_documento',
            type: 'input',
            templateOptions: {
              label: 'Nro. de documento',
              type: 'text'
            }
          },
          {
            key: 'nombres',
            type: 'input',
            templateOptions: {
              label: 'Nombre(s)',
              type: 'text'
            }
          },
          {
            key: 'primer_apellido',
            type: 'input',
            templateOptions: {
              label: 'Ap. Paterno',
              type: 'text'
            }
          },
          {
            key: 'segundo_apellido',
            type: 'input',
            templateOptions: {
              label: 'Ap. Materno',
              type: 'text'
            }
          },
          {
            key: 'correo',
            type: 'input',
            templateOptions: {
              label: 'Email',
              type: 'text'
            }
          },
          {
            key: 'roles',
            type: 'extra',
            templateOptions: {
              label: 'Roles',
              type: 'hidden'
            }
          },
          {
            key: 'departamentos',
            type: 'extra',
            templateOptions: {
              label: 'Departamentos',
              type: 'hidden'
            }
          },
          {
            key: 'estado',
            type: 'select',
            templateOptions: {
              label: 'Estado',
              type: 'select',
              options: [
                { value: 'ACTIVO', name: 'ACTIVO' },
                { value: 'INACTIVO', name: 'INACTIVO' },
                { value: 'ELIMINADO', name: 'ELIMINADO' },
                // { value: 'OBSERVADO', name: 'OBSERVADO' }
              ]
            }
          },
          // {
          //   key: '_fecha_modificación',
          //   type: 'datepicker',
          //   templateOptions: {
          //     label: 'Fecha de modificación',
          //     type: 'text'
          //   }
          // }
        ]
      },
      fields: ['id_usuario', 'usuario', 'fid_persona', 'numero_documento', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo', 'roles', 'departamentos', 'estado', '_fecha_modificación'],
      hiddenFields: ['id_usuario', 'fid_persona'],
      // fks: ['fid_formulario'],
      permission: {
        create: true,
        update: true,
        delete: false,
        filter: false
      },
      labels: { create: 'NUEVO USUARIO' },
      eventCreate: () => { this.$state.go('usuario',{ idUsuario: null }); },
      eventEdit: (usuario) => { this.$state.go('usuario',{ idUsuario: usuario.id_usuario }); },
      clases: {
        'estado': item => {
          // this.$log.log(item);
          return ['badge', 'badge-'+colors[item.estado]];
        }
      }
    };
  }

}

export default UsuariosController;
