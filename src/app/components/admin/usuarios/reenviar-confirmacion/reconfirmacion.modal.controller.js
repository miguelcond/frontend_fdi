'use strict';

class ReconfirmacionUsuarioController {
  constructor($uibModalInstance, DataService, _, Util, Message, data) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this._ = _;
    this.Util = Util;
    this.Message = Message;
    this.usuario = data.usuario;
    this.apiExtra = data.apiExtra;
  }

  $onInit() {
    this.url = this.apiExtra? this.apiExtra : `usuario/reenviar-confirmacion`;
  }

  reenviarCorreo() {
    this.DataService.post(this.url, {correo: this.usuario.persona.correo, id_usuario: this.usuario.id_usuario}).then(response => {
      if (response && response.finalizado) {
        this.Message.success(response.mensaje);
        this.$uibModalInstance.close(true);
      }
    });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default ReconfirmacionUsuarioController;
