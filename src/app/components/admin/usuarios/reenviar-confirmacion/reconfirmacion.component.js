'usr strict';

import controller from './reconfirmacion.controller';
import template from './reconfirmacion.html';

const ReconfirmacionUsuariosComponent = {
  bindings: {
    usuario: '=',
    api: '@?'
  },
  controller,
  template
};

export default ReconfirmacionUsuariosComponent;
