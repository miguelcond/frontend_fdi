'use strict';
import modalController from './reconfirmacion.modal.controller';
import modalTemplate from './reconfirmacion.modal.html';

class ReconfirmarUsuarioController {
  constructor(Modal, DataService, _) {
    'ngInject';
    this.Modal = Modal;
    this.DataService = DataService;
    this._ = _;
  }

  $onInit() {

  }

  reenviarCorreo() {
    const USUARIO = angular.copy(this.usuario);
    const REENVIO = this.Modal.show({
      template: modalTemplate,
      controller: modalController,
      data: {
        usuario: this.usuario,
        apiExtra: this.api
      }
    });
    REENVIO.result.then((correoReenviado) => {
      if (!correoReenviado) {
        this.usuario = USUARIO;
      }
    }).catch(() => {});
  }

}

export default ReconfirmarUsuarioController;
