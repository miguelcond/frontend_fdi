'use strict';

import adminTechosPresupuestariosComponent from './techos_presupuestarios.component';
import TechoPresupuestario from './techo_presupuestario/techo_presupuestario.component';

const adminTechosPresupuestarios = angular
    .module('app.techos_presupuestarios', [])
    .component('adminTechosPresupuestarios', adminTechosPresupuestariosComponent)
    .component('techoPresupuestario', TechoPresupuestario)
    .config(($stateProvider) => {
      $stateProvider
          .state('admin-techos-presupuestarios', {
            url: '/admin-techos-presupuestarios',
            component: 'adminTechosPresupuestarios',
            params: {
              gestion: 0
            }
          })
          .state('techo_presupuestario', {
            url: '/techo_presupuestario/:codigo',
            component: 'techoPresupuestario',
            params: {
              gestion: 0
            }
          });
    })
    .name;

export default adminTechosPresupuestarios;
