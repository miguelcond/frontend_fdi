/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class TechosPresupuestariosController {
  constructor($state, $stateParams, DataService, Modal, Message) {
    'ngInject';

    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
  }

  $onInit() {
    // this.$$obtenerLista();
    this.gestiones = [];
    let gestion = new Date();
    for (let nombre=2018; nombre<=1900+gestion.getYear(); nombre++) {
      this.gestiones.push({nombre});
      this.gestion = this.gestiones[this.gestiones.length-1];
    }
    if (this.$stateParams.gestion) this.gestion = this.gestiones.find((g)=>{return g.nombre==this.$stateParams.gestion});
    this.cargarTabla();
  }

  cargarTabla() {
    var colors = {
      INACTIVO: 'warning',
      ACTIVO: 'success',
      ELIMINADO: 'danger'
    };
    this.techos_presupuestarios = {
      url: `techo_presupuestario`,
      queryExtra: { gestion: this.gestion.nombre },
      data: {
        formly: [
          {
            key: 'codigo',
            type: 'input',
            templateOptions: {
              label: 'Código',
              type: 'number'
            }
          },
          {
            key: 'nombre_municipio',
            type: 'input',
            templateOptions: {
              label: 'Municipio',
              type: 'text'
            }
          },
          {
            key: 'techo_presupuestal',
            type: 'input',
            templateOptions: {
              label: 'Techo Presupuestario [Bs]',
              type: 'number'
            }
          },
          {
            key: 'monto_usado',
            type: 'input',
            templateOptions: {
              label: 'Monto usado',
              type: 'number'
            }
          },
          {
            key: 'monto_disponible',
            type: 'input',
            templateOptions: {
              label: 'Monto Disponible',
              type: 'number'
            }
          },
          {
            key: 'estado',
            type: 'select',
            templateOptions: {
              label: 'Estado',
              type: 'select',
              options: [
                { value: 'ACTIVO', name: 'ACTIVO' },
                { value: 'INACTIVO', name: 'INACTIVO' },
                { value: 'ELIMINADO', name: 'ELIMINADO' }
              ]
            }
          },
        ]
      },
      fields: ['codigo', 'nombre_municipio', 'techo_presupuestal', 'monto_usado', 'monto_disponible', 'estado'],
      permission: {
        create: false,
        update: true,
        delete: false,
        filter: false
      },
      labels: { update: 'Recargar tabla' },
      eventEdit: (techo_presupuestario) => { this.$state.go('techo_presupuestario',{ codigo: techo_presupuestario.codigo, gestion: this.gestion.nombre }); },
      clases: {
        'estado': item => {
          return ['badge', `badge-${colors[item.estado]}`];
        }
      }
    };
  }

  $$obtenerLista() {
    this.DataService.get(`techo_presupuestario`).then((resp) => {
      if (resp && resp.finalizado) {
        this.techos_presupuestarios = [];
        for (let i in resp.datos) {
          resp.datos[i].codigo_ant = resp.datos[i].codigo;
          resp.datos[i].nombre_municipio_ant = resp.datos[i].nombre_municipio;
          resp.datos[i].techo_presupuestal_ant = resp.datos[i].techo_presupuestal;
          resp.datos[i].monto_usado_ant = resp.datos[i].monto_usado;
          resp.datos[i].monto_disponible_ant = resp.datos[i].monto_disponible;
          this.techos_presupuestarios.push(resp.datos[i]);
        }
      }
    });
  }

  cambiarGestion(a) {
    this.techos_presupuestarios.queryExtra.gestion = this.gestion.nombre;
    this.$stateParams.gestion = a.nombre;
    this.$state.reload();
    // this.$state.go(`admin-techos-presupuestarios`,{gestion:a.nombre},{reload:true});
  }

  actualizar(techo) {
    if (techo.codigo && techo.nombre_municipio && techo.techo_presupuestal &&
        (techo.nombre_municipio != techo.nombre_municipio_ant || techo.techo_presupuestal != techo.techo_presupuestal_ant)) {
      // if (!techo.codigo_ant) {
      //   this.Modal.confirm(`¿Está seguro/a de agregar el nuevo código <b>${techo.codigo}</b> del parametro? No podra modificar el código mas adelante.`, ()=>{
      //     this.DataService.post(`parametricas/UNIDAD`, techo).then((resp)=>{
      //       if (resp && resp.finalizado) {
      //         techo.codigo_ant = techo.codigo;
      //         techo.nombre_ant = techo.nombre;
      //         techo.descripcion_ant = techo.descripcion;
      //       }
      //     });
      //   }, null, null, 'Si estoy seguro/a', '');
      // } else {
      if (techo.techo_presupuestal < techo.monto_usado) {
        this.Message.error('El valor del techo presupuestario no puede ser inferior al monto usado actualmente');
      } else {
        let actualizarValor = JSON.parse(JSON.stringify(techo));
        const codigo = techo.codigo;
        delete actualizarValor.codigo;
        this.DataService.put(`techo_presupuestario/${codigo}`, techo).then((resp) => {
          if (resp && resp.finalizado) {
            this.Message.success('Se ha realizado el cambio correctamente');
            techo.nombre_municipio_ant = techo.nombre_municipio;
            techo.techo_presupuestal_ant = techo.techo_presupuestal;
          }
        });
      }
      // }
    }
  }

  reestablecerValores() {
    this.Modal.confirm(`¿Está seguro/a de reestablecer los valores de los montos usados en cada municipio? Este cambio no podrá ser revertido.`, () => {
      this.DataService.put(`techo_presupuestario/reestablecer`, {reestablecer: true, gestion: this.gestion.nombre}).then((resp) => {
        if (resp && resp.finalizado) {
          // this.$$obtenerLista();
          this.Message.success('Se han reestablecido los valores por defecto correctamente');
          this.$state.reload();
        }
      });
    }, null, null, 'Si estoy seguro/a', '');
  }
}

export default TechosPresupuestariosController;
