'usr strict';

import controller from './techo_presupuestario.controller';
import template from './techo_presupuestario.html';

const techoPresupuestarioComponent = {
  bindings: {},
  controller,
  template
};

export default techoPresupuestarioComponent;
