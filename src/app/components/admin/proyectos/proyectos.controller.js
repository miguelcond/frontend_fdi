'use strict';

class ProyectosController {
  constructor($state, Message) {
    'ngInject';

    this.$state = $state;
    this.Message = Message;
  }

  $onInit() {
    var colors = {
      INACTIVO: 'warning',
      ACTIVO: 'success',
    };
    this.proyectos = {
      url: `adminproyectos`,
      queryExtra: { fid_proyecto: this.idProyecto, origen: 'bandeja' },
      data: {
        formly: [
          {
            key: 'id_proyecto',
            type: 'input',
            templateOptions: {
              label: 'ID',
              type: 'number'
            }
          },
          {
            key: 'version',
            type: 'input',
            templateOptions: {
              label: 'Versión',
              type: 'text'
            }
          },
          {
            key: 'nro_convenio',
            type: 'input',
            templateOptions: {
              label: 'Código',
              type: 'text'
            }
          },
          {
            key: 'nombre',
            type: 'input',
            templateOptions: {
              label: 'Nombre',
              type: 'text'
            }
          },
          {
            key: 'estado_proyecto',
            type: 'input',
            templateOptions: {
              label: 'estado',
              type: 'text'
            }
          },
          {
            key: 'estado',
            type: 'select',
            templateOptions: {
              label: 'Estado',
              type: 'select',
              options: [
                { value: 'ACTIVO', name: 'ACTIVO' },
                { value: 'INACTIVO', name: 'INACTIVO' },
              ]
            }
          },
          {
            key: '_fecha_modificacion',
            type: 'datepicker',
            templateOptions: {
              label: 'Fecha de modificación',
              type: 'datepicker'
            }
          }
        ]
      },
      fields: ['id_proyecto', 'version', 'codigo', 'nro_convenio', 'tipo', 'nombre', 'estado_proyecto', 'estado', '_fecha_modificacion'],
      hiddenFields: ['id_proyecto'],
      // fks: ['fid_formulario'],
      permission: {
        create: false,
        update: true,
        delete: false,
        filter: false
      },
      labels: { create: 'NUEVO USUARIO' },
      eventCreate: () => { this.$state.go('adminproyectos',{ codigo: null }); },
      eventEdit: (proyecto) => { this.$state.go('adminproyecto',{ codigo: proyecto.id_proyecto }); },
      clases: {
        'estado': item => {
          // this.$log.log(item);
          return ['badge', 'badge-'+colors[item.estado]];
        }
      }
    };
  }

}

export default ProyectosController;
