/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalController from '../modal/reasignarusuario.controller';
import modalTemplate from '../modal/reasignarusuario.html';

class ProyectoController {
  constructor($state, $stateParams, DataService, Modal) {
    'ngInject';

    this.$state = $state;
    this.$stateParams = $stateParams;
    this.DataService = DataService;
    this.Modal = Modal;    
    this.proyecto = {};
  }

  $onInit() {
    this.$$cargarDatos();
  }

  $$cargarDatos() {
    this.DataService.get(`adminproyectos/${this.$stateParams.codigo}/usuarios`).then((resp)=>{
      if (resp && resp.finalizado) {
        this.proyecto = resp.datos;
      }
    });
  }

  reasignarUsuario(transicion) {
    const REENVIO = this.Modal.show({
      template: modalTemplate,
      controller: modalController,
      data: {
        transicion
      }
    });
    REENVIO.result.then((result) => {
      if (result) {
        this.$state.reload();
      }
    }).catch(() => {});
  }

  reasignarUsuarioProy(proyecto) {
    let transicion = {
      objeto: proyecto.id_proyecto,
      tipo: 'proyecto',
      usuario_asignado: proyecto.usuario,
      usuario_asignado_rol: proyecto.usuario_rol,
    }
    const REENVIO = this.Modal.show({
      template: modalTemplate,
      controller: modalController,
      data: {
        transicion
      }
    });
    REENVIO.result.then((result) => {
      if (result) {
        this.$state.reload();
      }
    }).catch(() => {});
  }

  usuarioEnTransiciones() {
    const idBuscar = this.proyecto.usuario && this.proyecto.usuario.nro ? this.proyecto.usuario.nro : null;
    let existe = false;
    if (idBuscar) {
      for (let i in this.proyecto.transicion) {
        let usuario = this.proyecto.transicion[i].usuario_asignado;
        if (usuario.nro === idBuscar) {
          existe = true;
        }
      }
    }
    return existe;
  }

}

export default ProyectoController;
