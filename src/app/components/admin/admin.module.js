'use strict';

import Dashboard from './dashboard/dashboard.module';
import Login from './login/login.module';
import Consultasqr from './consultasqr/consultasqr.module';
import User from './user/user.module';
import Usuarios from './usuarios/usuarios.module';
import Proyectos from './proyectos/proyectos.module';
import Parametricas from './parametricas/parametricas.module';
import OrgInstitucion from './parametricas/org-institucion.module';
import TechosPresupuestarios from './techos_presupuestarios/techos_presupuestarios.module';
import RestablecerContrasenia from './restablecer_contrasenia/restablecer_contrasenia.module';
import Estados from './estados/estados.module';
import Cartera from './cartera/cartera.module';
const Admin = angular
    .module('app.admin', [
        Dashboard,
        Login,
        Consultasqr,
        User,
        Usuarios,
        Proyectos,
        Parametricas,
        OrgInstitucion,
        TechosPresupuestarios,
        RestablecerContrasenia,
        Estados,
	Cartera
    ])
    .name;

export default Admin;
