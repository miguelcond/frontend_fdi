'use strict';

import controller from './button.controller';
import template from './button.html';

const CodigoConvenioComponent = {
  bindings:{
    proyecto: '<'
  },
  controller,
  template
};

export default CodigoConvenioComponent;
