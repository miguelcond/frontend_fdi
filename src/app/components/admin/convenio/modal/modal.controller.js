'use strict';

class CambiarCodigoController {
  constructor($uibModalInstance, data, DataService, Message, Modal, $state) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Message = Message;
    this.proyecto = data;
    this.Modal = Modal;
    this.duplicados = [];
    this.$state = $state;
  }

  $onInit() {
      this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/duplicados`, {}).then((resp)=>{
        if (resp && resp.finalizado) {
          this.duplicados = resp.datos;
        }
      });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  corregir(){
    this.Modal.confirm('¿Esta seguro/a de modificar los datos? Deberá regenerar los documentos de evaluacion y/o informes.', ()=>{
      let datos = {
        id_proyecto: this.proyecto.id_proyecto,
        nro_convenio: `${this.proyecto.nro_convenio}`
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}`, datos).then((resp)=>{
        if (resp && resp.finalizado) {
          this.Message.success(resp.mensaje);          
          this.$uibModalInstance.close(false);
          this.$state.reload();
        }
      });
    }, null, 'Modificar datos', 'Si, estoy seguro/a');
  }

}

export default CambiarCodigoController;
