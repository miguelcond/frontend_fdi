'usr strict';

import controller from './estados.controller';
import template from './estados.html';

const EstadoComponent = {
  bindings: {},
  controller,
  template
};

export default EstadoComponent;
