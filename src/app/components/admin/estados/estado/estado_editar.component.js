'usr strict';

import controller from './estado_editar.controller';
import template from './estado_editar.html';

const estadoEditarComponent = {
  bindings: {},
  controller,
  template
};

export default estadoEditarComponent;
