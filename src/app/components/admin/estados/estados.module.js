'use strict';

import adminEstadosComponent from './estados.component';
import estadoEditar from './estado/estado_editar.component';

const adminEstados = angular
    .module('app.estados', [])
    .component('adminEstados', adminEstadosComponent)
    .component('estadoEditar', estadoEditar)
    .config(($stateProvider) => {
      $stateProvider
          .state('admin-estados', {
            url: '/admin-estados',
            component: 'adminEstados'
          })
          .state('estados/editar', {
            url: '/estados/editar/:codigo',
            component: 'estadoEditar'
          });
    })
    .name;

export default adminEstados;
