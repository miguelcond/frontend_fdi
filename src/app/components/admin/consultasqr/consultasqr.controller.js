/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import template from './modal.html';
import controller from './modal.controller';
class consultaqrController {

    constructor($http,$stateParams,$rootScope, $location, $scope, Storage, Message, baseUrl,$sce,Modal) {
        'ngInject';

        this.$stateParams = $stateParams;
        this.$http = $http;
        this.$rootScope = $rootScope;
        this.$location = $location;
        this.$scope = $scope;
        this.Storage = Storage;
        this.Message = Message;
        this.baseUrl = baseUrl;
        this.$sce = $sce;
        this.Modal = Modal;
    }

    $onInit() {
        console.log("fecha_inicio")
        this.codProyecto = this.$stateParams.codigo;
        console.log(this.codProyecto)
        this.proyecto = null;
        this.buscarProyecto();
        console.log(this.baseUrl,"base urlllllllllll")

    }

    verArchivo(){
      this.$http({
        method: 'GET',
        url:this.baseUrl+`proyectos/${this.proyecto[0].id_proyecto}/doc/C`
      }).then((response)=>{
        console.log(response,"asdfdfsdfd")
        //this.urlIframe = this.$sce.trustAsResourceUrl(`data:application/pdf;base64,${response.data.datos.base64}`)
        this.urlIframe = (`data:application/pdf;base64,${response.data.datos.base64}`)
        this.Modal.show({data: this.urlIframe, template, controller, size:'lg'})
      }).catch((error)=>{
        console.log(error,"error")
        this.Message.error("No existe!")
      })
    }
    buscarProyecto() {
      this.$http({
        method: 'GET',
        url: this.baseUrl+`proyect?codigo=`+this.codProyecto,
      }).then((response)=>{
        console.log(response, "respuesta con $http angular")
        this.proyecto = response.data.datos;
        console.log("antes del return::", this.proyecto)
      })
      /*this.DataService.post(`proyect/scan`,datos)
        .then(response=>{
          if(response){
            this.Message.success('QR Generado Correctamente...')
          }
        })*/
        
      
  }
}

export default consultaqrController;
