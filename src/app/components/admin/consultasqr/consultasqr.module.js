'use strict';

import consultasqrcomponent from './consultasqr.component';



const Consultasqr = angular
    .module('app.consultasqr', [])
    .component('appconsultasQR', consultasqrcomponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('consultasqr', {
                url: '/consultasqr/{codigo: string}',
                component: 'appconsultasQR',
                params: {
                  urlProcesado: ''
                }
            });
    })
    .name;

export default Consultasqr;
