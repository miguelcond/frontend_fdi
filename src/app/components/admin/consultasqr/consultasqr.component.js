'use strict';

import controller from './consultasqr.controller';
import template from './consultasqr.html';

const consultasqrComponent = {
    bindings: {},
    controller,
    template
};

export default consultasqrComponent;