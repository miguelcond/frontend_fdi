'usr strict';

import controller from './org-institucion.controller';
import template from './org-institucion.html';

const OrgInstitucionComponent = {
  bindings: {},
  controller,
  template
};

export default OrgInstitucionComponent;
