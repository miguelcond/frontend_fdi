'usr strict';

import controller from './parametricas.controller';
import template from './parametricas.html';

const ParametricasComponent = {
  bindings: {},
  controller,
  template
};

export default ParametricasComponent;
