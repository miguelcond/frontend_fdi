/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ParametricasController {
  constructor($state, DataService, Modal, Message) {
    'ngInject';

    this.$state = $state;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
  }

  $onInit() {
    this.$$obtenerUnidades();
  }

  $$obtenerUnidades() {
    this.DataService.get(`parametricas/UNIDAD`).then((resp) => {
      if (resp && resp.finalizado) {
        this.unidades = [];
        for (let i in resp.datos.rows) {
          resp.datos.rows[i].codigo_ant = resp.datos.rows[i].codigo;
          resp.datos.rows[i].nombre_ant = resp.datos.rows[i].nombre;
          resp.datos.rows[i].descripcion_ant = resp.datos.rows[i].descripcion;
          this.unidades.push(resp.datos.rows[i]);
        }
      }
    });
  }

  obtenerIncompletos() {
    return this.unidades.find((unidad) => {
      return !unidad.codigo || !unidad.nombre || !unidad.descripcion;
    });
  }

  obtenerModificados() {
    return this.unidades.find((unidad) => {
      return unidad.codigo != unidad.codigo_ant || unidad.nombre != unidad.nombre_ant || unidad.descripcion != unidad.descripcion_ant;
    })
  }

  nuevo() {
    if (!this.obtenerIncompletos() && !this.obtenerModificados()) {
      this.unidades.push({});
    } else {
      this.Message.warning('Debe guardar los cambios antes de agregar un nuevo parametro.');
    }
  }

  actualizar(unidad) {
    if (unidad.codigo && unidad.nombre && unidad.descripcion &&
        (unidad.codigo != unidad.codigo_ant || unidad.nombre != unidad.nombre_ant || unidad.descripcion != unidad.descripcion_ant)) {
      if (!unidad.codigo_ant) {
        this.Modal.confirm(`¿Está seguro/a de agregar el nuevo código <b>${unidad.codigo}</b> del parametro? No podra modificar el código mas adelante.`, () => {
          this.DataService.post(`parametricas/UNIDAD`, unidad).then((resp) => {
            if (resp && resp.finalizado) {
              unidad.codigo_ant = unidad.codigo;
              unidad.nombre_ant = unidad.nombre;
              unidad.descripcion_ant = unidad.descripcion;
            }
          });
        }, null, null, 'Si estoy seguro/a', '');
      } else {
        this.DataService.put(`parametricas/UNIDAD`, unidad).then((resp) => {
          if (resp && resp.finalizado) {
            unidad.nombre_ant = unidad.nombre;
            unidad.descripcion_ant = unidad.descripcion;
          }
        });
      }
    }
  }
}

export default ParametricasController;
