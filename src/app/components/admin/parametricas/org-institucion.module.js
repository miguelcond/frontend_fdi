'use strict';

import adminOrgInstitucionComponent from './org-institucion.component';

const adminproyectos = angular
    .module('app.org-instituciones', [])
    .component('adminOrgInstitucion', adminOrgInstitucionComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('admin-org-instituciones', {
            url: '/admin-org-instituciones',
            component: 'adminOrgInstitucion'
          });
    })
    .name;

export default adminproyectos;
