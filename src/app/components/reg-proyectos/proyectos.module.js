'use strict';

import Bandeja from './bandeja/bandeja.module';
import Proyecto from './proyecto/proyecto.module';
import ProyectoService from './proyectos.service';
import ComponentsProyectos from './c-proyectos/c-proyectos.module';

const Proyectos = angular
    .module('app.rproyectos', [
        Bandeja,
        Proyecto,
        ComponentsProyectos
    ])
    .service('Proyecto', ProyectoService)
    .name;

export default Proyectos;
