'use strict';

import controller from './lista-convenio.controller';
import template from './lista-convenio.html';

const listConvComponent = {
	binding: {},
	controller,
	template
};

export default listConvComponent;