'use strict';



class listConvController{
	constructor($log,DataService,Message){
		'ngInject';
		this.log = $log;
		this.DataService = DataService;
		this.Message = Message;
	}

	$onInit(){
		this.carteraSelected = 3;
		this.carteras = [
			{
				id_cartera: 3,
				num_cartera: 3,
				descripcion: 'Cartera 3'
			},{
				id_cartera: 4,
				num_cartera: 4,
				descripcion: 'Cartera 4'
			}
		]
	}

	buscar(dpa,car){
		if(dpa && dpa.departamento){
			let _ruta = dpa.departamento.codigo;
			if(dpa.municipio){
				_ruta += `/${dpa.municipio.codigo}`;
			}
			this.log.log(dpa);
			this.log.log(car);
			// this.log.log(this.dpa)
			// this.log.log(this.carteraSelected)
			this.DataService.get(`proyectos/municipios/${_ruta}?cartera=${car}`).then(resp=>{
				if(resp && resp.finalizado){
					this.datos = resp.datos;
					this.fechaRep = resp.fechaRep;
					this.titulo = resp.titulo;
				}
			});
		}else{
			this.Message.warning("Seleccione un departamento");
		}

	}

	generarPDF(){
		let tabla = document.getElementById('tabla1');

		this.DataService.pdf(`reportes/consejo`, {html:tabla.outerHTML}, true).then((resp)=>{
			if (resp) {
				window.open(resp,'ventana1','menu=0,tooltip=0,width=900px,height=700px,resize=0')
				// console.log(resp)
			}
		})
	}
}

export default listConvController;