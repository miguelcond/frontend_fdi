'use strict';

import BandejaMAEComponent from './mae-fdi/listado.component';
import ProyectosMunicipioMAEComponent from './mae-fdi-resumen/municipio.component';
import BandejaPendientesComponent from './pendientes/pendientes.component';
import BandejaProcesadosComponent from './procesados/procesados.component';
import ListaSupervisionesComponent from './supervisiones/supervisiones.component';
import ListaModificacionesComponent from './modificaciones/button.component';
import ButtonTimelineComponent from '../../admin/timeline/button.component';
import ButtonEstadoComponent from '../../admin/estados/button.component';
import ButtonCambiarCodigoComponent from '../../admin/convenio/button.component';
import ButtonParalizarComponent from './paralizar/button.component';
import ProyectosEjecutados from './consejo-consultivo-reporte/consejo-consultivo.component';
import listConvComponent from './lista-convenios-reporte/lista-convenio.component';

const Bandeja = angular
    .module('app.rbandeja', [])
    .component('rBandejaMAE', BandejaMAEComponent)
    .component('rProyectosMunicipioMAE', ProyectosMunicipioMAEComponent)
    .component('rBandejaPendientes', BandejaPendientesComponent)
    .component('rBandejaProcesados', BandejaProcesadosComponent)
    .component('listaSupervisiones', ListaSupervisionesComponent)
    .component('listaModificaciones', ListaModificacionesComponent)
    .component('buttonTimeline', ButtonTimelineComponent)
    .component('buttonEstado', ButtonEstadoComponent)
    .component('buttonCambiarCodigo', ButtonCambiarCodigoComponent)
    .component('buttonParalizar', ButtonParalizarComponent)
    .component('rProyectosEjecutados',ProyectosEjecutados)
    .component('rListaConvenios', listConvComponent)
    .config(($stateProvider, $urlRouterProvider) => {
        $stateProvider
            .state('lista-proyectos', {
              url: '/lista-proyectos',
              component: 'rBandejaMAE'
            })
            .state('lista-proyectos-municipio', {
              url: '/lista-proyectos-municipio',
              component: 'rProyectosMunicipioMAE'
            })
            .state('proyectos-pendientes', {
                url: '/proyectos-pendientes',
                component: 'rBandejaPendientes'
            })
            .state('proyectos-registrados', {
                url: '/proyectos-registrados',
                component: 'rBandejaProcesados'
            })
            .state('proyectos-ejecutados', {
                url: '/proyectos-ejecutados',
                component: 'rProyectosEjecutados'
            })
            .state('lista-proyectos-convenios',{
                url: '/lista-proyectos-convenios',
                component: 'rListaConvenios'
            });
        $urlRouterProvider.otherwise('/proyectos-pendientes');
    })
    .name;

export default Bandeja;
