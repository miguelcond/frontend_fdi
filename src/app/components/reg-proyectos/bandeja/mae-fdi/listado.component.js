'usr strict';

import controller from './listado.controller';
import template from './listado.html';

const ListadoComponent = {
  bindings: {},
  controller,
  template
};

export default ListadoComponent;
