/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalModificacionesController from '../modificaciones/modal.controller';
import modalModificacionesTemplate from '../modificaciones/modal.html';

class NuevaModificacionModalController {
  constructor($uibModalInstance, DataService, Storage, data, apiUrl, $state, Usuarios, Modal) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.apiUrl = apiUrl;
    this.$state = $state;
    this.proyecto = data;
    this.Usuarios = Usuarios;
    this.DataService = DataService;
    this.Modal = Modal;
  }

  $onInit() {
    this.usuario = this.Storage.getUser();
  }

  puedeVerModificaciones(adjudicacion) {
    if ((this.Usuarios.esFiscal() || this.Usuarios.esSupervisor() || this.Usuarios.esResponsableProyecto()) && adjudicacion.version > 1) {
      return true;
    }
    return (this.Usuarios.esSupervisor()) && adjudicacion;
  }

  nuevaModificacion(proyecto,adjudicacion) {
    this.$uibModalInstance.close(false);
    this.proyecto.adjudicacion = adjudicacion;
    this.Modal.show({
      template: modalModificacionesTemplate,
      controller: modalModificacionesController,
      data: this.proyecto,
      size: 'lg'
    });
  }

  irPlanilla(idSupervision) {
    this.$state.go('planilla-proyecto', { idSupervision });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
}

export default NuevaModificacionModalController;
