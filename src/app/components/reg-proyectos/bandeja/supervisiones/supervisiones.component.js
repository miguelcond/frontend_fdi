'use strict';

import controller from './supervisiones.controller';
import template from './supervisiones.html';

const ListaSupervisionesComponent = {
  bindings:{
    proyecto: '<'
  },
  controller,
  template
};

export default ListaSupervisionesComponent;
