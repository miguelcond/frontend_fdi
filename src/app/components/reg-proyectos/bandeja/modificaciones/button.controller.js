'use strict';

import modalModificacionesController from './modal.controller';
import modalModificacionesTemplate from './modal.html';

class ListaModificacionesController {
  constructor(Modal) {
    'ngInject';
    this.Modal = Modal;
  }

  $onInit() {

  }

  verModificaciones() {
    this.Modal.show({
      template: modalModificacionesTemplate,
      controller: modalModificacionesController,
      data: this.proyecto,
      size: 'lg'
    });
  }

}

export default ListaModificacionesController;
