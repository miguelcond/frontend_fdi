'use strict';

import controller from './pendientes.controller';
import template from './pendientes.html';

const BandejaPendientesComponent = {
  bindings:{},
  controller,
  template
};

export default BandejaPendientesComponent;
