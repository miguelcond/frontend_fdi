'use strict';

class ResumenProyectoController {
  constructor($state, $uibModalInstance, DataService, Storage, data, Util, $filter, $timeout, Message, Loading) {
    'ngInject';
    this.$state = $state;
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Util = Util;
    this.$filter = $filter;
    this.$timeout = $timeout;
    this.Message = Message;
    this.proyecto = data;
    this.Loading = Loading;
    this.conEstados = false;
    this.user = Storage.getUser();
  }

  $onInit(){    
    this.opciones = {};
  }

  tieneRegistro(){
    return ['ASIGNACION_TECNICO_FDI','REGISTRO_CONVENIO_FDI','ASIGNACION_EQUIPO_TECNICO_FDI','REGISTRO_DESEMBOLSO_FDI','APROBADO_FDI','CERRADO_FDI','MODIFICADO_FDI','EN_EJECUCION_FDI'].indexOf(this.proyecto.estado_proyecto)!==-1;
  }

  tieneConvenio(){
    return ['REGISTRO_CONVENIO_FDI','ASIGNACION_EQUIPO_TECNICO_FDI','REGISTRO_DESEMBOLSO_FDI','APROBADO_FDI','CERRADO_FDI','MODIFICADO_FDI','EN_EJECUCION_FDI'].indexOf(this.proyecto.estado_proyecto)!==-1;
  }

  tieneDesembolso(){
    return ['ASIGNACION_EQUIPO_TECNICO_FDI','REGISTRO_DESEMBOLSO_FDI','APROBADO_FDI','CERRADO_FDI','MODIFICADO_FDI','EN_EJECUCION_FDI'].indexOf(this.proyecto.estado_proyecto)!==-1;
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }  

}

export default ResumenProyectoController;
