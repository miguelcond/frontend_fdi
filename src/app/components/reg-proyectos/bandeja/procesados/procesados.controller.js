/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

// import modalController from './crear-proyecto.modal.controller';
// import crearProyectoTemplate from './crear-proyecto.modal.html';
// import modalAdjudicacionTemplate from './adjudicacion.modal.html';
// import modalAdjudicacionController from './adjudicacion.modal.controller';
import fdiModalController from './fdi-crear-proyecto.modal.controller';
import fdiCrearProyectoTemplate from './fdi-crear-proyecto.modal.html';

import modalModificacionesController from '../supervisiones/modificaciones.modal.controller';
import modalModificacionesTemplate from '../supervisiones/modificaciones.modal.html';
import modalModificacionesAdjudicacionController from '../modificaciones/modal.controller';
import modalModificacionesAdjudicacionTemplate from '../modificaciones/modal.html';

class BandejaProcesadosController {
  constructor(Storage, DataService, apiUrl, $location, Modal, $log, $state, DataTable, Usuarios) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.apiUrl = apiUrl;
    this.$location = $location;
    this.$log = $log;
    this.Modal = Modal;
    this.$state = $state;
    this.DataTable = DataTable;
    this.Usuarios = Usuarios;
  }

  $onInit() {
    this.datos = {};
    this.usuario = angular.fromJson(this.Storage.getUser());
    this.config = this.obtenerConfiguracion();
    // this.esEmpresa = this.esUsuarioEmpresa();
    this.esSupervisor = this.Usuarios.esSupervisor();
    this.verModificacion = this.puedeVerModificacion();
    const QUERY = {rol:this.Storage.get('rol').id_rol}; // this.esUsuarioEmpresa()? {matricula: this.usuario.matricula} : {rol:this.Storage.get('rol').id_rol};
    this.tableParams = this.DataTable.getParams(`proyectos/procesados`, QUERY);
  }

  obtenerConfiguracion() {
    var configuracion = {};
    if (this.usuario.rol) {
      switch (this.usuario.rol.nombre) {
        // case 'LEGAL_FDI':
        //   configuracion = {
        //     opciones: [{icon:'plus', tooltip:'Nuevo proyecto', md:'success', metodo: this.fdicrearProyecto.bind(this)}],
        //   };
        //   break;
        case 'RESP_RECEPCION_FDI':
        case 'TECNICO_FDI':
          configuracion = {
            opciones: [{icon:'plus', tooltip:'Nuevo proyecto', md:'success', metodo: this.fdiCrearProyecto.bind(this)}],
          };
          break;
        // case 'EMPRESA':
        //   configuracion = {
        //     opciones: [{icon: 'plus', tooltip: 'Adjudicar', md: 'success', metodo: this.nuevaAdjudicacion.bind(this)}],
        //   };
        //   break;
        default:
      }
      configuracion.acciones = [
        {icon: 'eye', tooltip: 'Ver', md: 'primary', metodo: this.observarProy.bind(this)}
      ];
    }
    return configuracion;
  }

  eventoOpcion(event, item, data) {
    if (typeof item.metodo === 'function') {
      item.metodo(event, data);
    }
  }

  // crearProyecto() {
  //   this.Modal.show({
  //     template: crearProyectoTemplate,
  //     controller: modalController,
  //     data: {},
  //     size: 'xlg'
  //   });
  // }

  fdiCrearProyecto() {
    this.Modal.show({
      template: fdiCrearProyectoTemplate,
      controller: fdiModalController,
      data: {},
      size: 'xlg'
    });
  }

  fdiCrearProyectoModificado(proyecto) {
    this.Modal.show({
      template: fdiCrearProyectoTemplate,
      controller: fdiModalController,
      data: {id_proyecto_referencia: proyecto.id_proyecto, nro_convenio_referencia: proyecto.nro_convenio},
      size: 'xlg'
    });
  }

  fdiAgregarAdjudicacion(proyecto) {
    // Se agrega un flag a la url para la consulta en backend
    this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto, urlProcesado: 'usar-flag/agregar-adjudicaciones'});
  }

  // nuevaAdjudicacion() {
  //   this.Modal.show({
  //     template: modalAdjudicacionTemplate,
  //     controller: modalAdjudicacionController,
  //     size: 'md'
  //   }).result.then( result => {
  //     this.$state.go('documentacion-proyecto', {codigo: result.id_proyecto});
  //   }).catch( () => {});
  // }

  observarProy(event, proyecto) {
    // if (this.esEmpresa() && proyecto.estado_proyecto === 'REGISTRO_EMPRESA') {
    //   return this.documentarProyecto(proyecto);
    // }
    this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto, urlProcesado: 'procesados'});
  }

  verUsuarios(proyecto) {
    this.$state.go('adminproyecto', { codigo: proyecto.id_proyecto });
  }

  // documentar() {

  // }

  nuevaPlanilla(proyecto) {
    const data = {
      fid_proyecto: proyecto.id_proyecto,
      computo_metrico: []
    };
    this.DataService.post(`supervisiones`, data).then(response => {
      if (response) {
        this.irPlanilla(response.datos.id_supervision);
      }
    });
  }

  irPlanilla(idSupervision) {
      this.$state.go('planilla-proyecto', { idSupervision });
  }

  // esUsuarioEmpresa() {
  //   return !angular.isUndefined(this.usuario.nit);
  // }

  esUsuarioRegistro() {
    return this.Usuarios.esRecepcion() || this.Usuarios.esTecnicoResponsable();
  }

  puedeCrearPlanilla(proyecto) {
    if (proyecto.supervisiones.length > 0) {
      return proyecto.supervisiones[0].estado_supervision === 'EN_ARCHIVO_FDI';
    }
    return true;
  }

  puedeVerConsolidado(proyecto) {
    return ['RESP_GAM_GAIOC','FINANCIERO_FDI','TECNICO_FDI','FISCAL_FDI'].indexOf(this.usuario.rol.nombre) !== -1 && ['APROBADO_FDI','EN_EJECUCION_FDI'].indexOf(proyecto.estado_proyecto) !== -1;
  }

  urlPlanillaConsolidada(proyecto) {
    return this.urlPlanillaConsolidadaDescarga = `${this.apiUrl}proyectos/${proyecto.id_proyecto}/doc/PAC_${proyecto.id_proyecto}/descargar`;
  }

  puedeVerSupervision(proyecto) {
    if (proyecto) {
      return proyecto.supervisiones && proyecto.supervisiones.length > 0 && ['APROBADO_FDI','EN_EJECUCION_FDI','MODIFICADO_FDI','CERRADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0;
    }
    return false;
  }

  puedeVerModificacion(proyecto) {
    let enSupervision = false;
    if (proyecto) {
      if (proyecto.supervisiones) {
        enSupervision = proyecto.supervisiones.find((sup) => { return ['PENDIENTE_FDI','EN_FISCAL_FDI'].indexOf(sup.estado_supervision) != -1; });
      }
      return !enSupervision && ['SUPERVISOR_FDI','TECNICO_FDI','JEFE_REVISION_FDI'].indexOf(this.usuario.rol.nombre) >= 0 && ['APROBADO_FDI','EN_EJECUCION_FDI','REVISION_MODIFICADO_FDI','MODIFICADO_FDI','CERRADO_FDI','PARALIZADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0;
    }
    return false;
  }

  puedeParalizar(proyecto) {
    if (proyecto) {
      return (this.Usuarios.esJefeRevision() || this.Usuarios.esTecnicoResponsable()) && ['APROBADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0;
    }
    return false;
  }

  // puedeAgregarAdjudicacion(proyecto) {
  //   if (proyecto) {
  //     return this.Usuarios.esResponsableGAM() && ['APROBADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0;
  //   }
  //   return false;
  // }

  puedeReferenciarProyecto(proyecto) {
    if (proyecto) {
      /* return ['RESP_RECEPCION_FDI','TECNICO_FDI'].indexOf(this.usuario.rol.nombre) >= 0
              && ['RECHAZADO_FDI'].indexOf(proyecto.estado_proyecto) >= 0 && proyecto.proyectos.length == 0; */
    }
    return false;
  }

  puedeVerModificaciones(proyecto) {
    if (!proyecto.orden_proceder) return false;
    if ((this.Usuarios.esSupervisor() || this.Usuarios.esFiscal() || this.Usuarios.esResponsableProyecto() || this.Usuarios.esJefeRevision() )) return true;
    return false;
  }

  irAModificaciones(proyecto) {
    if (proyecto.adjudicaciones.length > 1) {
      if (proyecto.id_proyecto > 0) {
        this.Modal.show({
          template: modalModificacionesTemplate,
          controller: modalModificacionesController,
          data: proyecto,
          size: 'lg'
        });
      }
    } else {
      proyecto.adjudicacion = proyecto.adjudicaciones[0];
      this.Modal.show({
        template: modalModificacionesAdjudicacionTemplate,
        controller: modalModificacionesAdjudicacionController,
        data: proyecto,
        size: 'lg'
      });
    }
  }
}

export default BandejaProcesadosController;
