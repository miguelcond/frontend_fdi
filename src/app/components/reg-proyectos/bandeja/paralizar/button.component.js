'use strict';

import controller from './button.controller';
import template from './button.html';

const ButtonParalizarComponent = {
  bindings:{
    proyecto: '<'
  },
  controller,
  template
};

export default ButtonParalizarComponent;
