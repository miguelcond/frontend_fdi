'use strict';

import modalParalizarController from './modal.controller';
import modalParalizarTemplate from './modal.html';

class ButtonParalizarController {
  constructor(Modal) {
    'ngInject';
    this.Modal = Modal;
  }

  $onInit() {

  }

  verMensaje() {
    this.Modal.show({
      template: modalParalizarTemplate,
      controller: modalParalizarController,
      data: this.proyecto,
      size: 'lg'
    });
  }

}

export default ButtonParalizarController;
