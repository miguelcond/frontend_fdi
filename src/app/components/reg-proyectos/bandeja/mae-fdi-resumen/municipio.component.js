'usr strict';

import controller from './municipio.controller';
import template from './municipio.html';

const ListadoComponent = {
  bindings: {},
  controller,
  template
};

export default ListadoComponent;
