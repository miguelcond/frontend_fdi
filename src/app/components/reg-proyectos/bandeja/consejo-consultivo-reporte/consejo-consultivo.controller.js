'use strict'
import resumenProyectoController from '../procesados/resumen-proyecto.modal.controller';
import resumenProyectoTemplate from '../procesados/resumen-proyecto.modal.html';

// private
let local = {
  calcularTotales: ($ctrl, datos)=>{
    $ctrl.totales = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totales.total = local.Util.sumar([$ctrl.totales.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totales.fdi = local.Util.sumar([$ctrl.totales.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totales.gam = local.Util.sumar([$ctrl.totales.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totales.beneficiarios = local.Util.sumar([$ctrl.totales.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  calcularTotalesEval: ($ctrl, datos)=>{
    $ctrl.totalesEval = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totalesEval.total = local.Util.sumar([$ctrl.totalesEval.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totalesEval.fdi = local.Util.sumar([$ctrl.totalesEval.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totalesEval.gam = local.Util.sumar([$ctrl.totalesEval.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totalesEval.beneficiarios = local.Util.sumar([$ctrl.totalesEval.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  calcularTotalesConv: ($ctrl, datos)=>{
    $ctrl.totalesConv = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totalesConv.total = local.Util.sumar([$ctrl.totalesConv.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totalesConv.fdi = local.Util.sumar([$ctrl.totalesConv.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totalesConv.gam = local.Util.sumar([$ctrl.totalesConv.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totalesConv.beneficiarios = local.Util.sumar([$ctrl.totalesConv.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  calcularTotalesDesem: ($ctrl, datos)=>{
    $ctrl.totalesDesem = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
    };
    for (let i in datos) {
      $ctrl.totalesDesem.total = local.Util.sumar([$ctrl.totalesDesem.total, datos[i].financiamiento.monto_total_proyecto]);
      $ctrl.totalesDesem.fdi = local.Util.sumar([$ctrl.totalesDesem.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totalesDesem.gam = local.Util.sumar([$ctrl.totalesDesem.gam, datos[i].financiamiento.monto_contraparte]);
      $ctrl.totalesDesem.beneficiarios = local.Util.sumar([$ctrl.totalesDesem.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
    }
  },
  calcularTotalesEjec: ($ctrl, datos)=>{
    $ctrl.totalesEjec = {
      total: 0,
      fdi: 0,
      gam: 0,
      beneficiarios: 0,
      ejecucion: 0,
      avance_fisico: 0,
      primer_desembolso:0,
      segundo_desembolso:0,
      tercer_desembolso:0,
      transferidos:0
    };
    let promedioEjec = 0; 
    for (let i in datos) {
      /*Inicio calculos para fecha finalizacion y ejecucion presupuestario FDI*/
      let finfecha = new Date(datos[i].orden_proceder);
      let totalTransferido = local.Util.sumar([datos[i].cronograma_desembolsos.monto_1,datos[i].cronograma_desembolsos.monto_2,datos[i].cronograma_desembolsos.monto_3])
      let ejecupress = local.Util.sumar([datos[i].cronograma_desembolsos.porcentaje_1,datos[i].cronograma_desembolsos.porcentaje_2,datos[i].cronograma_desembolsos.porcentaje_3])
      datos[i].ejecuPres = ejecupress;
      datos[i].totalTransferido = totalTransferido;
      finfecha.setDate(finfecha.getDate() + parseInt(datos[i].plazo_ejecucion))
      datos[i].fecha_conclusion = finfecha

      promedioEjec = local.Util.sumar([promedioEjec, ejecupress])
      /*Fin de calculos para fecha finalizacion y ejecucion presupuestario FDI*/
      $ctrl.totalesEjec.fdi = local.Util.sumar([$ctrl.totalesEjec.fdi, datos[i].financiamiento.monto_fdi]);
      $ctrl.totalesEjec.primer_desembolso = local.Util.sumar([$ctrl.totalesEjec.primer_desembolso, datos[i].cronograma_desembolsos.monto_1]);
      $ctrl.totalesEjec.segundo_desembolso = local.Util.sumar([$ctrl.totalesEjec.segundo_desembolso, datos[i].cronograma_desembolsos.monto_2]);
      $ctrl.totalesEjec.tercer_desembolso = local.Util.sumar([$ctrl.totalesEjec.tercer_desembolso, datos[i].cronograma_desembolsos.monto_3]);
      $ctrl.totalesEjec.beneficiarios = local.Util.sumar([$ctrl.totalesEjec.beneficiarios, datos[i].financiamiento.monto_beneficiarios]);
      $ctrl.totalesEjec.transferidos = local.Util.sumar([$ctrl.totalesEjec.transferidos, totalTransferido]);
    }
    $ctrl.totalesEjec.ejecucion = Math.round(promedioEjec / datos.length)
  },
  obtenerResumen: ($ctrl, dpa)=>{
    if(dpa && dpa.departamento) {
      let _ruta = dpa.departamento.codigo;
      if(dpa.municipio){
        _ruta += `/${dpa.municipio.codigo}`;
      }
      local.DataService.get(`proyectos/municipios/${_ruta}?cartera=${$ctrl.carteraSelected}`).then(resp=>{
        // local.$log.log('resp', resp);
        if (resp && resp.finalizado) {
          $ctrl.datos = resp.datos;
          local.calcularTotales($ctrl, resp.datos);
          $ctrl.titulo = resp.titulo;
          $ctrl.fechaRep = resp.fechaRep;
          $ctrl.datosEval = resp.datosEval;
          $ctrl.datosConv = resp.datosConv;
          $ctrl.datosDesem = resp.datosDesem;
          $ctrl.datosEjec = resp.datosEjec;
          $ctrl.datosRechazados = resp.datosRechazados;
          $ctrl.datoslista = resp.datoslista;
          local.calcularTotalesEval($ctrl, resp.datosEval);
          local.calcularTotalesConv($ctrl, resp.datosConv);
          local.calcularTotalesDesem($ctrl, resp.datosDesem);
          local.calcularTotalesEjec($ctrl, resp.datosEjec);
          local.calcularTotalesEjec($ctrl, resp.datoslista);
        }
      });
    } else {
      $ctrl.datos = [];
      $ctrl.datosEval = [];
      $ctrl.datosRechazados = [];
      $ctrl.datosConv = [];
      $ctrl.datosDesem = [];
      $ctrl.datosEjec = [];
    }
  }
};

class ConsejoController {
  constructor($log, DataService, $state, Util, Modal, Loading) {
    'ngInject';

    local.$log = $log;
    local.DataService = DataService;
    local.Util = Util;
    local.titulo = '';
    local.Loading = Loading;
    this.$state = $state;
    this.Modal = Modal;
  }
  $onInit() {
    let gestion = new Date();
    this.gestiones = [];
    for (let nombre=2018; nombre<=1900+gestion.getYear(); nombre++) {
      this.gestiones.unshift({nombre});
      this.gestion = this.gestiones[0];
    }
    local.DataService.get(`cartera/`,{}).then(response=>{
      if(response){
        this.carteras = response.datos;
        // console.log('hay '+this.carteras.length+' carteras :',this.carteras)
      }
    })
    this.carteraSelected = 3;
  }

  $doCheck() {
    let d1 = this.dpa && this.dpa.departamento;
    let d2 = local.dpa && local.dpa.departamento;

    let m1 = this.dpa && this.dpa.municipio;
    let m2 = local.dpa && local.dpa.municipio;

    if ( ((d1 && !d2) || (!d1 && d2) || (d1 && d2 && d1.codigo!=d2.codigo) ) || ((m1 && !m2) || (!m1 && m2) || (m1 && m2 && m1.codigo!=m2.codigo) )) {
      local.dpa = angular.copy(this.dpa);
      local.obtenerResumen(this, local.dpa);
    }
  }

  cambiarGestion() {
    local.obtenerResumen(this, local.dpa);
  }

  verProyecto(proyecto) {
    //this.$state.go('reg-proyecto', {codigo: proyecto.id_proyecto, urlProcesado: 'procesados'});
    local.DataService.get(`proyectos/${proyecto.id_proyecto}/procesados`).then(resp=>{
      this.Modal.show({
        template: resumenProyectoTemplate,
        controller: resumenProyectoController,
        data: resp.datos,
        size: 'xlg'
      });
    });
  }
  generarPDF() {
    let tabla = document.getElementById('tabla1');

    local.DataService.pdf(`reportes/consejo`,{html:tabla.outerHTML},true).then((resp) => {
      if (resp) {
        
        window.open(resp,'ventana1','menu=0,tooltip=0,width=900px,height=700px,resize=0')
        // console.log(resp)
      }
    });
  }
  fichaProyecto(proyecto) {
    local.Loading.show(`Se estan obteniendo los datos, espere por favor`, true);
    local.DataService.get(`reportes/ficha_tecnica/${proyecto.id_proyecto}`).then((resp) => {
      if (resp && resp.finalizado) {
        local.Loading.hide();
        const base64 = `data:application/pdf;base64,${resp.datos.base64}`;
        const aLink = document.createElement("a");
        const fileName = `Ficha_Tecnica_${proyecto.nro_convenio.replace(/\//g,'.')}.pdf`;

        aLink.href = base64;
        aLink.download = fileName;
        aLink.click();
      } else {
        local.Loading.hide();
      }
    });
  }

}

export default ConsejoController;
