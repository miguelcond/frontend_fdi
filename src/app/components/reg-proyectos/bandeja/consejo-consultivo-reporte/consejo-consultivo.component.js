'usr strict';

import controller from './consejo-consultivo.controller';
import template from './consejo-consultivo.html';

const ConsejoComponent = {
  bindings: {},
  controller,
  template
};

export default ConsejoComponent;
