'use strict';

import controller from './asignacion-responsable.controller';
import template from './asignacion-responsable.html';

const AsignacionResponsable = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default AsignacionResponsable;
