/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ModuloController {
    constructor($uibModalInstance, data, title, icon, DataService, Message) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
        this.title = title;
        this.icon = icon;
        this.DataService = DataService;
        this.Message = Message;
    }
    $onInit() {
      this.modulos = [];
      this.modulo = this.data;
      this.modulo.componente = this.modulo.referencia.toUpperCase();

      // this.opcionesComponente = this.obtenerOpcionesComponente();
    }

    // obtenerOpcionesComponente() {
    //   let opcionesComponente = [];
    //   if (this.modulo && this.modulo.referencia) {
    //     opcionesComponente = this.modulo.referencia.split(',');
    //     for (let i in opcionesComponente) {
    //       opcionesComponente[i] = {
    //         nombre: opcionesComponente[i].trim().toUpperCase()
    //       };
    //     }
    //   }
    //   return opcionesComponente;
    // }

    guardar() {
      // if (typeof this.modulo.componente === 'object') {
      //   this.modulo.nombre = this.modulo.componente.nombre +'|'+ this.modulo.resultado;
      // } else {
      this.modulo.nombre = this.modulo.componente +'|'+ this.modulo.modulo;
      // }
      this.DataService.post(`modulos`, this.modulo).then( resp => {
        if (resp) {
          this.Message.success("Se registró el módulo correctamente.");
          this.$uibModalInstance.close(resp);
        }
      });
    }

    actualizar() {
      // if (typeof this.modulo.componente === 'object') {
      //   this.modulo.nombre = this.modulo.componente.nombre +'|'+ this.modulo.resultado;
      // } else {
      this.modulo.nombre = this.modulo.componente +'|'+ this.modulo.modulo;
      // }
      this.DataService.put(`modulos/${this.modulo.id_modulo}`, this.modulo).then( resp => {
        if (resp) {
          this.Message.success("Se actualizó el módulo correctamente.");
          this.$uibModalInstance.close(resp);
        }
      });
    }

    cerrar() {
      this.$uibModalInstance.dismiss('cancel');
    }
}

export default ModuloController;
