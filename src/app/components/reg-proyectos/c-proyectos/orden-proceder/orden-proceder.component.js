'use strict';

import controller from './orden-proceder.controller';
import template from './orden-proceder.html';

const OrdenProcederComponent = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default OrdenProcederComponent;
