'use strict';

class DesembolsoController {
  constructor($scope, Proyecto, _, Datetime) {
    'ngInject';
    this.$scope = $scope;
    this.Proyecto = Proyecto;
    this._ = _;
    this.Datetime = Datetime;
  }

  $onInit() {
    this.ordenProcederOriginal = this.proyecto.orden_proceder;
    this.prepararValidacionesFecha();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.proyecto.adj.orden_proceder', () => {
      this.proyecto.orden_proceder = this.ordenProcederOriginal;
      // if (this.proyecto.adj) {
      //   console.log(new Date(this.proyecto.adj.orden_proceder), this.proyecto.orden_proceder, this.proyecto.adj.orden_proceder < this.proyecto.orden_proceder);
      //   console.log(this.ordenProcederOriginal);
      // }
      if (this.proyecto.adj && this.proyecto.adj.orden_proceder) {
        if (!this.proyecto.orden_proceder || this.proyecto.adj.orden_proceder < this.proyecto.orden_proceder) {
          this.proyecto.orden_proceder = this.proyecto.adj.orden_proceder
        } else {
          this.proyecto.orden_proceder = this.ordenProcederOriginal;
        }
      }
    });
  }

  prepararValidacionesFecha() {
    let fechaDesde, mensajeError = 'La fecha debe ser mayor a la fecha';
    if (this.tieneBoletas()) {
      fechaDesde = this.fechaMasAntiguaDeBoletas();
      mensajeError = `${mensajeError} de inicio de la boleta de anticipo más antigua`
    } else if (this.proyecto.tipo === 'TP_CIF') {
      fechaDesde = this.proyecto.fecha_desembolso;
      mensajeError = `${mensajeError} del desembolso`
    } else {
      fechaDesde = this.proyecto.convenio.fecha_suscripcion;
      mensajeError = `${mensajeError} del convenio`
    }
    fechaDesde = this.Datetime.getFormatDate(fechaDesde);
    this.validacion = {
      fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaDesde), 1),
      mensaje_fecminima: `${mensajeError} (${this.Datetime.parseDate(fechaDesde, '/')})`
    };
  }

  tieneBoletas() {
    return this.proyecto.boletas.length > 0;
  }

  fechaMasAntiguaDeBoletas() {
    return this._.sortBy(this.proyecto.boletas, (boleta) => {
      return boleta.fecha_inicio_validez;
    })[0].fecha_inicio_validez;
  }

  tieneSupervisiones() {
    return this.proyecto && this.proyecto.supervisiones && this.proyecto.supervisiones.length > 0;
  }

  tieneOrdenProceder() {
    return this.ordenProcederOriginal ? true : false;
  }

}

export default DesembolsoController;
