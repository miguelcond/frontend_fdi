'use strict';



class comuControllerModal {
  constructor( $uibModalInstance, DataService, Loading, Modal,data) {
    'ngInject';
   
    this.$uibModalInstance = $uibModalInstance;
    this.DataService = DataService;
    this.Loading = Loading;
    this.Modal = Modal;
    this.data=data;

  }

  $onInit(){
    this.opciones = {};
    this.modoVistaConvenio = false;
    //console.log("dataaa",this.data);
    
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
  aceptar() {
    this.$uibModalInstance.close(this.data.datos.filter((comun)=>{

      return comun.seleccionado;
    }));
  }

}

export default comuControllerModal;