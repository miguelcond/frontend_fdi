/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ArchivoAdjunto {
  constructor($scope, Util, Usuarios, Proyecto, Modal, DataService, Message,Storage) {
    'ngInject';
    this.$scope = $scope;
    this.Util = Util;
    this.Usuarios = Usuarios;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.DataService = DataService;
    this.Message = Message;
    this.archivos = null;
    this.Storage = Storage;
  }

  $onInit() {
    this.usuario = this.Storage.getUser();
    console.log(this.usuario,"USUARIO")
    if(this.usuario.rol.id_rol == 15){
      this.tipos_documentos = [{
        codigo: 'Informe_Financiero',
        nombre: 'Informe Financiero'
      }];
    }
    if(this.usuario.rol.id_rol == 14){
      this.tipos_documentos = [{
        codigo: 'Informe_Legal',
        nombre: 'Informe Legal'
      }];
    }

    
    this.uploadArchivoAdjunto = (file) => {
      if (!file.name) {
        this.Message.warning(`Debe escribir el título del archivo.`);
        return;
      }
      let datos = this.proyecto.archivo_adjunto.find((d)=>{return d.datos.titulo==file.name});
      if (!datos.file) {
        this.Message.warning(`No se puede crear subir un documento con titulo '${file.name}'. Puede ser que ya exista uno similar.`);
        return;
      }
      if (datos.datos.id_archivo_adjunto) {
        datos.p = 'change';
      } else {
        datos.p = 'save';
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto`, datos).then(response => {
        if (response && response.finalizado) {
          datos.datos.id_archivo_adjunto = response.datos.id_archivo_adjunto;
          datos.datos.ruta = response.datos.ruta;
          datos.datos.nombre_original = response.datos.nombre_original;
        }
      });
    }

  }

  $onChanges(a) {
    if (!a.proyecto.previousValue && a.proyecto.currentValue.id_proyecto) {
      this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto`).then((resp)=>{
        if (resp.finalizado) {
          // Documentos adjuntos
          let documentos = [];
          for (let i in resp.datos.rows) {
            documentos.push({ datos: resp.datos.rows[i] });
          }
          this.proyecto.archivo_adjunto = documentos;
        }
      });
    }
  }

  /* RESPALDO TECNICO */
  modoEdicionDoc(archivo) {
    return archivo.datos.codigo_estado===this.proyecto.estado_actual.codigo;
  }

  verArchivoAdjunto(archivo) {
    if (archivo.file && archivo.file.base64) {
      this.Modal.showDocumentBase64(archivo.file.base64, 'Informe', 'strBase64');
      return;
    }
    this.DataService.get(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto/${archivo.datos.id_archivo_adjunto}`).then((doc) => {
      if (doc && doc.finalizado) {
        archivo.file = doc.datos.file;
        this.Modal.showDocumentBase64(archivo.file.base64, 'Informe', 'strBase64');
      }
    });
  }

  cambiarArchivoAdjunto(archivo) {
    delete archivo.datos.ruta;
  }

  agregarArchivoAdjunto() {
    console.log('agregar', this.proyecto);
    if (!this.proyecto || !this.proyecto.id_proyecto) return;
    if (!this.proyecto.archivo_adjunto) this.proyecto.archivo_adjunto = [];
    this.proyecto.archivo_adjunto.push({
      datos: {
        titulo: null,
        descripcion: null,
        rol: {
          nombre: this.Usuarios.usuario.rol.nombre,
          descripcion: this.Usuarios.usuario.rol.descripcion,
        },
        codigo_estado: this.proyecto.estado_actual.codigo
      }
    });
  }

  agregarTodosArchivos() {
    if(this.usuario.rol.id_rol == 13){// solo para rol tecnico responsable
      if(this.proyecto.cartera == 3){
        this.tipos_documentos = [{
          codigo: 'carta_solicitud_financiamiento',
          nombre: '01 Carta de Presentacion y Solicitud de financiamiento'
        },{
          codigo: 'lista_beneficiarios',
          nombre: '02 Lista de beneficiarios'
        },{
          codigo: 'Acta_compromiso_cumplimiento',
          nombre: '03 Acta de compromiso de cumplimiento de contrapartes'
        },{
          codigo: 'Acta_verificacion_campo_gam_gaioc',
          nombre: '04 Acta de verificacion de campo del GAM o GAIOC'
        },{
          codigo: 'ddjj_proyecto_no_afecta_tierras_fiscales',
          nombre: '05 DDJJ notariada que establesca que el proyecto no afecta a tierras fiscales'
        },{
          codigo: 'certificacion_sedcam',
          nombre: '06 Certificacion del SEDCAM en la juridiccion del tramo (solo puentes)'
        },{
          codigo: 'Acta_consenso_pueblos',
          nombre: '07 Acta de consenso (pueblos indigenas originarios)'
        },{
          codigo: 'hoja_firmada_responsables_edtp',
          nombre: '08 Hoja firmada por Responsables EDTP'
        },{
          codigo: 'ficha_tecnica',
          nombre: '09 Ficha Tecnica'
        },{
          codigo: 'informe_verificacion_campo',
          nombre: '10 Informe y acta de verificacion de campo.'
        },{
          codigo: 'informe_evaluacion_edtp',
          nombre: '11 Informe de Evaluacion del EDTP (con firmas)'
        },{
          codigo: 'formulario_evaluacion_edtp',
          nombre: '12 Formulario de Evaluacion del EDTP (con firmas)'
        },{
          codigo: 'ficha_tecnica_proyecto',
          nombre: '13 Ficha Tecncia del proyecto (con firmas)'
        },{
          codigo: 'acta_validacion_tecnica',
          nombre: '14 Acta de validacion tecnica'
        }];
      }
      if(this.proyecto.cartera == 4){
        this.tipos_documentos = [{
          codigo: 'informe_evaluacion_edtp',
          nombre: '01 INFORME DE EVALUACION DEL EDTP (CON FIRMAS Y V°B°)'
        },{
          codigo: 'formulario_evaluacion_edtp',
          nombre: '02 FORMULARIO DE EVALUACION DEL EDTP (CON FIRMAS Y  V°B°)'
        },{
          codigo: 'ficha_tecnica',
          nombre: '03 FICHA TECNCIA DEL PROYECTO (CON FIRMAS)'
        },{
          codigo: 'Acta_verificacion_campo_fdi',
          nombre: '04 INFORME Y ACTA DE VERIFICACION DE CAMPO - FDI. (CON FIRMAS Y V°B°)'
        },{
          codigo: 'verificacion_criterios_elegibilidad',
          nombre: '05 VERIFICACION DE CUMPLIMIENTO DE CRITERIOS DE ELEGIBILIDAD'
        },{
          codigo: 'acta_aprobacion_socioeconomica',
          nombre: '06 ACTA DE APROBACION PARTE SOCIOECONOMICA'
        },{
          codigo: 'carta_solicitud_financiamiento',
          nombre: '07 CARTA DE PRESENTACION Y SOLICITUD DE FINANCIAMIENTO'
        },{
          codigo: 'informe_tecnico_condiciones_previas',
          nombre: '08 INFORME TECNICO DE CONDICIONES PREVIAS ITCP'
        },{
          codigo: 'ficha_tecnica_gam',
          nombre: '09 FICHA TECNICA DEL GAM'
        },{
          codigo: 'marco_logico',
          nombre: '10 MARCO LOGICO DEL PROYECTO'
        },{
          codigo: 'cronograma_ejecucion_proyecto',
          nombre: '11 CRONOGRAMA DE EJECUCION DEL PROYECTO'
        },{
          codigo: 'lista_beneficiarios',
          nombre: '12 LISTA DE BENEFICIARIOS POR FAMILIA CON CEDULA DE IDENTIDAD Y FIRMA (PARA PUENTES LISTA DE COMUNIDAD)'
        },{
          codigo: 'Acta_compromiso_cumplimiento',
          nombre: '13 ACTA DE COMPROMISO DE CUMPLIMIENTO DE CONTRAPARTES'
        },{
          codigo: 'Acta_verificacion_campo_gam_gaioc',
          nombre: '14 ACTA DE VERIFICACION DE CAMPO DEL GAM O GAIOC'
        },{
          codigo: 'acta_aprobacion__edtp_gam',
          nombre: '15 ACTA DE APROBACION DEL EDTP ELABORADO POR EL GAM / GAIOC'
        },{
          codigo: 'acta_consenso_proyecto',
          nombre: '16 ACTA DE CONSENSO DE PRIORIZACIÓN DEL PROGRAMA O PROYECTO'
        },{
          codigo: 'ddjj_proyecto_no_afecta_tierras_fiscales',
          nombre: '17 DDJJ NOTARIADA QUE ESTABLEZCA QUE EL PROYECTO NO AFECTA A TIERRAS FISCALES'
        },{
          codigo: 'certificacion_sedcam',
          nombre: '18 CERTIFICACION DEL SEDCAM EN LA JURIDICCION DEL TRAMO (SOLO PUENTES VEHICULARES)'
        },{
          codigo: 'formulacion_aprobado',
          nombre: '19 FORMULARIO SUSCRITO POR LOS PROFESIONALES RESPONSABLES DE LA ELABORACION Y APROBACION DEL EDTP DEL PROYECTO'
        }];
      }
    }
    if (!this.proyecto || !this.proyecto.id_proyecto) return;
    if (!this.proyecto.archivo_adjunto) this.proyecto.archivo_adjunto = []; 
    for (var i = 0; i < this.tipos_documentos.length; i++) {
      this.proyecto.archivo_adjunto.push({
        datos: {
          titulo: this.tipos_documentos[i].nombre,
          descripcion: null,
          rol: {
            nombre: this.Usuarios.usuario.rol.nombre,
            descripcion: this.Usuarios.usuario.rol.descripcion,
          },
          codigo_estado: this.proyecto.estado_actual.codigo
        }
      });
    }

  }

  eliminarArchivoAdjunto(archivo,index) {
    let eliminado = false;
    let i = 0;
    if(!archivo.datos.id_archivo_adjunto){
      // para eliminar elemento sin adjunto
      this.proyecto.archivo_adjunto.splice(index,1);
      return ;
    }
    this.Modal.confirm(`¿Está seguro de eliminar por completo el archivo?`,
      () => {
        while (!eliminado && this.proyecto.archivo_adjunto[i]) {
          if (this.proyecto.archivo_adjunto[i].datos.titulo == archivo.datos.titulo) {
            archivo.datos.p = 'delete';
            this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/archivo_adjunto/${archivo.datos.id_archivo_adjunto}`, archivo.datos).then(doc => {
              if (doc && doc.finalizado) {
                this.proyecto.archivo_adjunto.splice(i,1);
              }
            });
            eliminado = true;
            break;
          }
          i++;
        }
      }, null, 'Eliminar archivo', 'Sí, estoy seguro/a');
  }

}

export default ArchivoAdjunto;
