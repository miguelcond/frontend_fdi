'use strict';

import controller from './archivo_adjunto.controller';
import template from './archivo_adjunto.html';

const ArchivoAdjuntoComponent = {
  bindings:{
    proyecto: '<',
    modoVista: '<'
  },
  controller,
  template
};

export default ArchivoAdjuntoComponent;
