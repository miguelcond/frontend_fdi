'use strict';

import controller from './equipo-tecnico.controller';
import template from './equipo-tecnico.html';

const EquipoTecnicoComponent = {
  bindings: {
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
}

export default EquipoTecnicoComponent;
