'use strict';

import controller from './recepcion-definitiva.controller';
import template from './recepcion-definitiva.html';

const RecepcionDefinitivaComponent = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default RecepcionDefinitivaComponent;
