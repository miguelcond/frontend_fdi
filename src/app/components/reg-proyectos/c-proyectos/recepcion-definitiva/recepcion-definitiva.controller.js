/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class RecepcionDefinitivaController {
  constructor($scope, Proyecto, _) {
    'ngInject';
    this.$scope = $scope;
    this.Proyecto = Proyecto;
    this._ = _;
  }

  $onInit() {
    this.recepcionDefinitivaOriginal = this.proyecto.plazo_recepcion_definitiva;
  }

  $onChanges() {
    // this.$scope.$watch('$ctrl.proyecto.plazo_recepcion_definitiva', () => {

    // });
  }

  validarPlazo () {
    if (this.proyecto && this.proyecto.plazo_recepcion_definitiva && this.proyecto.plazo_recepcion_definitiva >= 1) {
      return;
    } else {
      this.proyecto.plazo_recepcion_definitiva = this.recepcionDefinitivaOriginal;
    }
  }

  // tieneOrdenProceder() {
  //   return this.ordenProcederOriginal ? true : false;
  // }
}

export default RecepcionDefinitivaController;
