/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class RegistroFiscalSupervisorController {
  constructor($uibModalInstance ,DataService, $stateParams, data, _, Message) {
    'ngInject';

    this.DataService = DataService;
    this.$stateParams = $stateParams;
    this.data = data;
    this.$uibModalInstance = $uibModalInstance;
    this._ = _;
    this.Message = Message;
    this.miembro = null;
    this.miembro = this.data.miembro || {};
    this.modoEdicion = true;
  }

  $onInit() {
    if (this.miembro.id_usuario) {
      this.getDatosMiembroEquipo();
    }
  }  

  getDatosMiembroEquipo() {
    this.DataService.get(`proyectos/${this.miembro.fid_proyecto}/equipos_tecnicos/${this.miembro.id_equipo_tecnico}`).then(result => {
      this.miembro = result.datos;
      angular.extend(this.miembro, result.datos.usuario.persona);
      delete this.miembro.usuario;
      this.datosIniciales = angular.copy(this.miembro);
      if (this.miembro.tipo_equipo !== 'TE_FDI' && this.miembro.doc_designacion) {
        let docDesignacion = this.miembro.doc_designacion.substring(1, this.miembro.doc_designacion.indexOf(']'));
        this.archivo = {
          url: `proyectos/${this.miembro.fid_proyecto}/doc/${docDesignacion}`,
          urlDescarga: `proyectos/${this.miembro.fid_proyecto}/doc/${docDesignacion}/descargar`,
        };
      }
    });
  }

  registrar() {
    // let url = `proyectos/${this.$stateParams.codigo}/contactos`;
    // let servicio = 'post';
    // let miembroEquipo = angular.copy(this.miembro);
    // if (this.miembro.id_equipo_tecnico) {
    //   url += `/${this.miembro.id_equipo_tecnico}`;
    //   servicio = 'put';
    // }
    // this.DataService[servicio](url, miembroEquipo).then(result => {
    //   if (result) {
    //     this.Message.success(result.mensaje);
    this.$uibModalInstance.close(this.miembro);
    //   }
    // });
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
}

export default RegistroFiscalSupervisorController;
