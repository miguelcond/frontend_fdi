'use strict';

import controller from './autoridad-beneficiaria.controller';
import template from './autoridad-beneficiaria.html';

const AutoridadBeneficiariaComponent = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default AutoridadBeneficiariaComponent;
