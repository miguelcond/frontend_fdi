'use strict';

import controller from './convenio.controller';
import template from './convenio.html';

const ConvenioComponent = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default ConvenioComponent;
