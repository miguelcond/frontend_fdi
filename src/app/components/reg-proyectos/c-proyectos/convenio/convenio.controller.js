/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class DatosGenerales {
  constructor($scope, Util, Proyecto, Modal, _, DataService, Message, $http,subDomain, baseUrl) {
    'ngInject';
    this.$http = $http;
    this.$scope = $scope;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this._ = _;
    this.archivos = null;
    this.DataService = DataService;
    this.Message = Message;
    this.qrimage = null;
    this.subDomain = subDomain;
    this.baseUrl = baseUrl;
  }

  $onInit() {
    this.tipos_plazo_ampliado = [{
      codigo: 'Convenio modificatorio',
      nombre: 'Convenio modificatorio'
    },{
      codigo: 'Adenda al convenio',
      nombre: 'Adenda al convenio'
    }];
    console.log(this.proyecto)
    this.generar();
    //this.opciones_plazo_administrativo = [{codigo:240,nombre:'240 días (8 meses)'},{codigo:255,nombre:'255 días (8 1/2 meses)'}];
    if (!this.proyecto.convenio) this.proyecto.convenio = {};
    this.proyecto.convenio.plazo_tiempo_administrativo = this.proyecto.financiamiento.plazo_tiempo_administrativo.codigo;
    this.proyecto.convenio.plazo_convenio_ampliado = this.proyecto.convenio.plazo_convenio_ampliado || 0;

    this.$scope.$watch('$ctrl.proyecto.convenio.plazo_tiempo_administrativo', () => {
      if (this.proyecto.convenio.plazo_tiempo_administrativo) {
        this.proyecto.convenio.plazo_convenio = this.proyecto.plazo_ejecucion + this.proyecto.convenio.plazo_tiempo_administrativo;
      }
    });

    this.archivos = {
      urlDocConvenio: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/C` : '',
      urlDocConvenioDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/C/descargar` : '',
      urlDocConvenioExtendido: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT` : '',
      urlDocConvenioExtendidoDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT/descargar` : '',
      urlDocConvenioExtendido2: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT2` : '',
      urlDocConvenioExtendidoDescarga2: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT2/descargar` : '',
      urlDocConvenioExtendido3: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT3` : '',
      urlDocConvenioExtendidoDescarga3: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT3/descargar` : '',
      urlDocConvenioExtendido4: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT4` : '',
      urlDocConvenioExtendidoDescarga4: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT4/descargar` : '',
      urlDocConvenioExtendido5: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT5` : '',
      urlDocConvenioExtendidoDescarga5: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/CXT5/descargar` : '',
      urlDocResolucion: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/RM` : '',
      urlDocResolucionDescarga: this.proyecto ? `proyectos/${this.proyecto.id_proyecto}/doc/RM/descargar` : ''
    };

    if (this.proyecto.orden_proceder) {
      this.diaActual = new Date();
      this.plazo_convenio0 = new Date(this.proyecto.orden_proceder);
      this.plazo_con_ampliaciones = new Date(this.proyecto.orden_proceder);

      this.plazo_amp1 = this.proyecto.convenio.plazo_convenio_ampliado ? parseInt(this.proyecto.convenio.plazo_convenio_ampliado) : 0
      this.plazo_amp2 = this.proyecto.convenio.plazo_convenio_ampliado2 ? parseInt(this.proyecto.convenio.plazo_convenio_ampliado2) : 0
      this.plazo_amp3 = this.proyecto.convenio.plazo_convenio_ampliado3 ? parseInt(this.proyecto.convenio.plazo_convenio_ampliado3) : 0
      this.plazo_amp4 = this.proyecto.convenio.plazo_convenio_ampliado4 ? parseInt(this.proyecto.convenio.plazo_convenio_ampliado4) : 0
      this.plazo_amp5 = this.proyecto.convenio.plazo_convenio_ampliado5 ? parseInt(this.proyecto.convenio.plazo_convenio_ampliado5) : 0
      // console.log(this.plazo_convenio0,'this.plazo_convenio0')
      // console.log(this.proyecto.plazo_ejecucion,'convenio plazoo')
      //  console.log(this.plazo_convenio.get(), 'getDate')
      // console.log(this.plazo_convenio0.getDate()+' + '+ parseInt(this.proyecto.convenio.plazo))
      this.plazo_convenio0.setDate(this.plazo_convenio0.getDate() + parseInt(this.proyecto.plazo_ejecucion))
      this.plazo_con_ampliaciones.setDate( this.plazo_con_ampliaciones.getDate() + parseInt(this.proyecto.plazo_ejecucion)+ this.plazo_amp1 + this.plazo_amp2 + this.plazo_amp3 + this.plazo_amp4 + this.plazo_amp5);
      // console.log('firestoneeeeee ', this.plazo_convenio0)
      // this.fechaAlerta = new Date(this.plazo_convenio0);
      // this.fechaAlerta.setDate(this.fechaAlerta.getDate() - 30)

      // if(this.diaActual < this.fechaAlerta){
       
      //   this.Modal.alert('Plazo Convenio Vigente hasta: ' + new Date(this.plazo_convenio0))
      // }else{
      //   if(this.diaActual < new Date(this.plazo_convenio0)){
      //     this.Modal.alert('Faltan menos de 30 días para el vencimiento del plazo convenio:  '+ new Date(this.plazo_convenio0))
      //   }else{
      //     this.Modal.alert('Se ha vencido el plazo convenio el : ' + new Date(this.plazo_convenio0))
      //   }       
      // }
      console.log(this.plazo_convenio0,"plazo_convenio0")
      console.log(this.plazo_con_ampliaciones," plazo con ampliaciones")
      if(this.plazo_amp1){
        this.diasRestantes = Math.floor((this.plazo_con_ampliaciones - this.diaActual) / (1000*60*60*24));
      }else{
        this.diasRestantes =  Math.floor((this.plazo_convenio0 - this.diaActual) / (1000*60*60*24));
      }
      
      //this.Modal.alert('dias restantes sin ampliacion'+ this.diasRestantes) 
 

      
      if(this.diasRestantes > 0){
        this.Modal.alert('Falta '+ this.diasRestantes + ' días para el vencimiento del plazo convenio ')
      }else{
        if(this.diaActual > this.plazo_con_ampliaciones){
          this.Modal.alert('Se ha vencido el plazo convenio el : ' + new Date(this.plazo_con_ampliaciones))
        }else{
          this.Modal.alert('El plazo de convenio vence hoy: ' + new Date(this.plazo_con_ampliaciones));
        }
      }

      this.plazo_convenio0 = this.plazo_convenio0.toISOString();
    } else {
      this.plazo_convenio0 = null;
    }
    this.mostrar = true;
    this.recalcularPlazoConvenio1();
    this.recalcularPlazoConvenio2();
    this.recalcularPlazoConvenio3();
    this.recalcularPlazoConvenio4();
    this.recalcularPlazoConvenio5();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.proyecto.monto_fdi', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
    this.$scope.$watch('$ctrl.proyecto.monto_contraparte', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
  }

  calcularMontoConvenio() {
    this.proyecto.monto_total_convenio = this.Util.redondear((this.proyecto.monto_fdi ? this.proyecto.monto_fdi : 0) + (this.proyecto.monto_contraparte ? this.proyecto.monto_contraparte : 0));
  }

  recalcularPlazoConvenio1() {
    if (this.plazo_convenio0) {
      this.plazo_convenio1 = new Date(this.plazo_convenio0);
      this.plazo_convenio1.setDate(this.plazo_convenio1.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado))
      this.plazo_convenio1 = this.plazo_convenio1.toISOString();
    }
  }
  recalcularPlazoConvenio2() {
    if (this.plazo_convenio1 && this.proyecto.convenio.plazo_convenio_ampliado2>0) {
      this.plazo_convenio2 = new Date(this.plazo_convenio1);
      this.plazo_convenio2.setDate(this.plazo_convenio2.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado2))
      this.plazo_convenio2 = this.plazo_convenio2.toISOString();
    }
  }
  recalcularPlazoConvenio3() {
    if (this.plazo_convenio2 && this.proyecto.convenio.plazo_convenio_ampliado3>0) {
      this.plazo_convenio3 = new Date(this.plazo_convenio2);
      this.plazo_convenio3.setDate(this.plazo_convenio3.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado3))
      this.plazo_convenio3 = this.plazo_convenio3.toISOString();
    }
  }
  recalcularPlazoConvenio4() {
    if (this.plazo_convenio3 && this.proyecto.convenio.plazo_convenio_ampliado4>0) {
      this.plazo_convenio4 = new Date(this.plazo_convenio3);
      this.plazo_convenio4.setDate(this.plazo_convenio4.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado4))
      this.plazo_convenio4 = this.plazo_convenio4.toISOString();
    }
  }
  recalcularPlazoConvenio5() {
    if (this.plazo_convenio4 && this.proyecto.convenio.plazo_convenio_ampliado5>0) {
      this.plazo_convenio5 = new Date(this.plazo_convenio4);
      this.plazo_convenio5.setDate(this.plazo_convenio5.getDate() + parseInt(this.proyecto.convenio.plazo_convenio_ampliado5))
      this.plazo_convenio5 = this.plazo_convenio5.toISOString();
    }
  }
  generar() {
    console.log(window.location.origin+this.subDomain+`#!/consultasqr/`+this.proyecto.codigo)
    const datos = {
      url: window.location.origin+this.subDomain+`#!/consultasqr/`+this.proyecto.codigo
    }
    console.log("datos",datos);
    this.$http({
      method: 'POST',
      url: this.baseUrl+`proyect/scan`,
      data: datos
    }).then((response)=>{
      this.qrimage = response.data.datos
      console.log(response.data.datos)
    })
    /*this.DataService.post(`proyect/scan`,datos)
      .then(response=>{
        if(response){
          this.Message.success('QR Generado Correctamente...')
        }
      })*/
  }
}

export default DatosGenerales;
