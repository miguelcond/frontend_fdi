'use strict';

import controller from './informe.controller';
import template from './informe.html';

const InformeEvaluacion = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default InformeEvaluacion;
