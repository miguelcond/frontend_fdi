'use strict';

import DatosGenerales from './datos-generales/datos-generales.component';
import DatosGeneralesExtra from './datos-generales-extra/datos-generales.component';
import DatosGeneralesExtraResumen from './datos-generales-extra/datos-generales-resumen.component';
import DatosFinanciamiento from './financiamiento/financiamiento.component';
import Convenio from './convenio/convenio.component';
import AsignacionResponsable from './asignacion-responsable/asignacion-responsable.component';
import Desembolso from './desembolso/desembolso.component';
import AutoridadBeneficiaria from './autoridad-beneficiaria/autoridad-beneficiaria.component';
import Items from './items/items.component';
import EjecutarTransicion from './ejecutar-transicion/ejecutar-transicion.component';
// import Dbc from './dbc/dbc.component';
// import Garantia from './garantia/garantia.component';
// import BoletaGarantia from './boleta-garantia/boleta-garantia.component';
// import Boleta from './boleta-garantia/boleta/boleta.component';
import EquipoTecnico from './equipo-tecnico/equipo-tecnico.component';
import OrdenProceder from './orden-proceder/orden-proceder.component';
import RecepcionDefinitiva from './recepcion-definitiva/recepcion-definitiva.component';
import Evaluacion from './evaluacion/evaluacion.component';
import InformeEval from './informe-eval/informe.component';
import ArchivoAdjunto from './archivo_adjunto/archivo_adjunto.component';

const ComponentsProyectos = angular
    .module('app.rcforms', [])
    .component('regDatosGenerales', DatosGenerales)
    .component('regDatosGeneralesExtra', DatosGeneralesExtra)
    .component('regDatosGeneralesExtraResumen', DatosGeneralesExtraResumen)
    .component('regFinanciamiento', DatosFinanciamiento)
    .component('regConvenio', Convenio)
    .component('regAsignacionResponsable', AsignacionResponsable)
    .component('regDesembolso', Desembolso)
    .component('regAutoridadBeneficiaria', AutoridadBeneficiaria)
    .component('regItems', Items)
    .component('ejecutarTransicion', EjecutarTransicion)
    // .component('dbc', Dbc)
    // .component('garantia', Garantia)
    // .component('boletaGarantia', BoletaGarantia)
    // .component('boleta', Boleta)
    .component('equipoTecnico', EquipoTecnico)
    .component('regOrdenProceder', OrdenProceder)
    .component('regRecepcionDefinitiva', RecepcionDefinitiva)
    .component('regEvaluacion', Evaluacion)
    .component('regInformeEval', InformeEval)
    .component('regArchivoAdjunto', ArchivoAdjunto)
    .name;

export default ComponentsProyectos;
