/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

import modalObservacionController from './observacion.modal.controller.js';
import modalObservacionTemplate from './observacion.modal.html';
import modalRechazarController from './rechazar.modal.controller.js';
import modalRechazarTemplate from './rechazar.modal.html';

class EjecutarTransicion {
  constructor(DataService, Util, Proyecto, $state, Modal, Loading, Message, $scope, _, Storage, $q, $log, Computo) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.$state = $state;
    this.Modal = Modal;
    this.Loading = Loading;
    this.Message = Message;
    this.$scope = $scope;
    this._ = _;
    this.Storage = Storage;
    this.$q = $q;
    this.$log = $log;
    this.Computo = Computo;
  }

  $onInit() {
    setTimeout(() => {
      if ([13].indexOf(this.getRolUsuario()) !== -1 && this.Proyecto.estadoActual && this.Proyecto.estadoActual.codigo === 'REGISTRO_PROYECTO_FDI') {
        console.log('autoguardado activado');
        // Autoguardado cada 5 minutos
        this.interval = setInterval(() => {
          let usuario = this.Storage.getUser();
          // Verificar si puede guardar
          if (!usuario || usuario.id_usuario !== this.usuario.id_usuario || this.$state.$current.name !== 'reg-proyecto') {
            clearInterval(this.interval);
            return;
          }
          const accionAutoguardado = {
            estado: 'REGISTRO_PROYECTO_FDI',
            label: 'Guardar',
            autoguardado: true
          };
          this.ejecutarAccion(accionAutoguardado);
        }, 120000);
      }
    }, 3000);
  }

  $onDestroy() {
    clearInterval(this.interval);
  }

  getRolUsuario() {
    this.usuario = this.Storage.getUser();
    return this.usuario.rol.id_rol;
  }

  puedeAcceder() {
    if (!this.elemento || !this.elemento.estado_actual || !this.elemento.estado_actual.roles) {
      return;
    }
    return (this.elemento.estado_actual.roles.indexOf(this.getRolUsuario()) !== -1)
      || (this.elemento.estado_actual.codigo == 'APROBADO_FDI' && [18,16].indexOf(this.getRolUsuario()) !== -1)
      || (this.elemento.estado_actual.codigo == 'EN_EJECUCION_FDI' && [18,17,16,15,14].indexOf(this.getRolUsuario()) !== -1);
  }

  /**
   * ejecutarAccion - Ejecuta la transición para un elemento, verifica si previa a la ejecución
   * se debe registrar una observación o simplemente realizar un guardado (sin pasar a un siguiente estado)
   * o en todo caso si existe un método previo a ejecutar (obtenido del controlador de donde se llama a este componente)
   * se ejecuta el mismo para luego proceder a ejecutar la transición
   *
   * @param  {object} accion Objeto donde se tiene los detalles de la acción a ejecutar
   * @return {function}        Devuelve el método a ejecutar
   */
  ejecutarAccion(accion) {
    if (accion.rechazar) {
      return this.rechazarProyecto(accion);
    }
    if (accion.observacion) {
      return this.registrarObservacion(accion);
    }
    if (accion.guardar) {
      return this.guardarDatos();
    }
    if (accion.autoguardado) {
      return this.ejecutarAutoguardado();
    }
    if (!this.metodoPrevio || accion.retroceder) {
      return this.ejecutarTransicion(accion);
    } else {
      this.metodoPrevio({accion}).then((result) => {
        this.confirmar = result && result.confirmar && !accion.continuar && !accion.retroceder ? result.confirmar : false;
        return this.ejecutarTransicion(accion);
      }).catch( () => { });
    }
  }

  registrarObservacion(accion) {
    let modalObservacion = this.Modal.show({
      template: modalObservacionTemplate,
      controller: modalObservacionController,
      data: this.elemento,
      size:'md'
    });

    modalObservacion.result.then((elementoActualizado) => {
      this.elemento = elementoActualizado;
      return this.ejecutarTransicion(accion)
    }).catch(() => {});
  }

  rechazarProyecto(accion) {
    let modalRechazo = this.Modal.show({
      template: modalRechazarTemplate,
      controller: modalRechazarController,
      data: this.elemento,
      size:'lg'
    });

    modalRechazo.result.then((elementoActualizado) => {
      this.elemento = elementoActualizado;
      return this.ejecutarTransicion(accion)
    }).catch(() => {});
  }

  ejecutarTransicion(accion) {
    const DATOS = {};
    DATOS[this.atributoEstado] = accion.estado;
    let registrar;
    if (accion.continuar || accion.retroceder) {
      // Cambia de estado sin redirigir a bandeja
      registrar = this.registrarTransicion.bind(this, DATOS, null, null, this.recargarActual.bind(this));
    } else {
      registrar = this.registrarTransicion.bind(this, DATOS, null, null, this.volverABandeja.bind(this));
    }
    if (this.confirmar && !accion.rechazar && !accion.observacion && !accion.continuar && !accion.retroceder) {
      return this.confirmacionPrevia(registrar, accion);
    }
    return registrar();
  }

  confirmacionPrevia(callback, accion) {
    let mensaje = this.mensajeConfirmacion || `¿Está seguro/a de derivar el proceso a la siguiente instancia?`;
    if (this.mensajeButton && this.mensajeButton[accion.label]) {
      mensaje = this.mensajeButton[accion.label];
    }
    this.Modal.confirm( mensaje, () => {
      return callback();
    }, null, 'Confirmar envío', 'Si, estoy seguro/a', 'Cancelar');
  }

  registrarTransicion(data, mensajeLoading, mensajeExito, metodoPosterior) {
    this.Loading.show(mensajeLoading || 'Derivando', true);
    let DATOS = angular.merge(this.servicio.getDatosSegunEstado(this.elemento, this.form), data || {});

    // DATOS.rol = this.usuario.rol.id_rol;
    DATOS.rol = this.getRolUsuario();
    this.DataService.put(`${this.url}`, DATOS)
    .then(response => {
      this.Loading.hide();
      if (response) {
        this.Message.success(mensajeExito || response.mensaje || 'Se envió el proceso a la siguiente instancia correctamente.');
        if (this.metodoPosterior) {
          this.$scope.$emit(this.metodoPosterior);
        } else if (metodoPosterior) {
          return metodoPosterior(response.datos);
        }
      }
    });
  }

  guardarDatos() {
    delete this.elemento[this.atributoEstado];
    return this.registrarTransicion(null, 'Guardando datos', 'Se guardó los datos', this.actualizarDatosElemento.bind(this));
  }

  ejecutarAutoguardado() {
    delete this.elemento[this.atributoEstado];
    let DATOS = angular.merge(this.servicio.getDatosSegunEstado(this.elemento, this.form), {});
    // DATOS.rol = this.usuario.rol.id_rol;
    DATOS.rol = this.getRolUsuario();
    this.DataService.put(`${this.url}`, DATOS)
    .then(response => {
      if (response) {
        console.log('autoguardado ' + new Date());
      }
    });
  }

  /**
   * actualizarDatosElemento - Actualizando datos de un elemento, de acuerdo a los atributos modificados
   *
   * @param  {Object} aElemento Datos del elemento actualizado mediante servicio
   */
  actualizarDatosElemento(aElemento) {
    this._.keys(this.servicio.getDatosSegunEstado(this.elemento, this.form)).map(item => {
      this.elemento[item] = aElemento[item];
    });
    this.servicio.setCopia(this.elemento);
    if (!this.elemento.hasOwnProperty(`${this.atributoEstado}`) || this.elemento[this.atributoEstado] == "") {
      if (aElemento.hasOwnProperty(`${this.atributoEstado}`)) {
        this.elemento[this.atributoEstado] = aElemento[this.atributoEstado];
      }
    }
  }

  cancelar() {
    this.volverABandeja();
  }

  volverABandeja() {
    this.usuario = this.Storage.getUser();
    this.$state.go(this.usuario.pathInicio.replace('/',''));
  }

  recargarActual() {
    this.$state.reload();
  }
}

export default EjecutarTransicion;
