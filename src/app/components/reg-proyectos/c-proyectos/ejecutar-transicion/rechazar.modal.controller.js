'use strict';

class RechazarController {
    constructor($uibModalInstance, data) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
    }
    $onInit() {
      this.data.observacion = null;
      this.data.rechazo_definitivo = null;
    }

    cerrar() {
      this.data.observacion = null;
      this.data.motivo_rechazo = null;
      this.$uibModalInstance.dismiss('cancel');
    }

    rechazar() {
      this.$uibModalInstance.close(this.data);
    }

}

export default RechazarController;
