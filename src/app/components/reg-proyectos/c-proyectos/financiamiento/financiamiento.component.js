'use strict';

import controller from './financiamiento.controller';
import template from './financiamiento.html';

const DatosFinanciamiento = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default DatosFinanciamiento;
