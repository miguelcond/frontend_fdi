'use strict';

import controller from './evaluacion.controller';
import template from './evaluacion.html';

const DatosEvaluacion = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default DatosEvaluacion;
