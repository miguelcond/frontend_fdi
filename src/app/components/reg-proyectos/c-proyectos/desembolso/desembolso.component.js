'use strict';

import controller from './desembolso.controller';
import template from './desembolso.html';

const DesembolsoComponent = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default DesembolsoComponent;
