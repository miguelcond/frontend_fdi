'use strict';

class DesembolsoController {
  constructor($timeout, Proyecto, Modal, Util, Datetime,Message) {
    'ngInject';

    this.$timeout = $timeout;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Util = Util;
    this.Datetime = Datetime;
    this.monto_3 = 0;
    this.porcentaje_3 = 0;
    this.Message = Message;
  }

  $onInit() {
    this.archivo = {
      urlDocCertificacion: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP` : '',
      urlDocCertificacion2: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP2` : '',
      urlDocCertificacion3: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP3` : '',
      urlDocCertificacionDescarga: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP/descargar` : '',
      urlDocCertificacionDescarga2: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP2/descargar` : '',
      urlDocCertificacionDescarga3: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/CIP3/descargar` : ''
    };
    let fechaConvenio = this.Datetime.getFormatDate(this.proyecto.convenio.fecha_suscripcion);
    this.validacion = {
      fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaConvenio), 0),
      mensaje_fecminima: `La fecha debe ser igual o mayor a la fecha del convenio (${this.Datetime.parseDate(fechaConvenio, '/')})`,
      fec_min_inscripcion: this.Datetime.getFormatDate(this.proyecto.convenio.fecha_suscripcion),
      mensaje_fecminima_desembolso1: `La fecha debe ser igual o mayor a la fecha de inscripción presupuestaria.`,
      mensaje_fecminima_desembolso2: `La fecha debe ser igual o mayor a la fecha del desembolso anterior.`,
    };

    if (!this.proyecto.cronograma_desembolsos) {
      if (this.proyecto.beneficiarios.productivo && this.proyecto.beneficiarios.productivo.maquinaria) {
        this.proyecto.cronograma_desembolsos = {
          porcentaje_1: 40, //50,
          porcentaje_2: 40, //30,
          porcentaje_3: 20, //20,
        }
      } else {
        this.proyecto.cronograma_desembolsos = {
          porcentaje_1: 40, //20,
          porcentaje_2: 40, //40,
          porcentaje_3: 20, //40,
        }
      }
    }
    if (this.proyecto.financiamiento.monto_total_proyecto) {
      this.proyecto.cronograma_desembolsos.monto_1 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_1/100]);
      this.proyecto.cronograma_desembolsos.monto_2 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_2/100]);
      this.proyecto.cronograma_desembolsos.monto_3 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_3/100]);
      // this.monto_3 = this.proyecto.cronograma_desembolsos.monto_3;
      // this.porcentaje_3 = this.proyecto.cronograma_desembolsos.porcentaje_3;
    }

    this.monto_privado = this.proyecto.financiamiento.cuadro.totales.privado
                        - (this.proyecto.cronograma_desembolsos.priv1 || 0)
                        - (this.proyecto.cronograma_desembolsos.priv2 || 0)
                        - (this.proyecto.cronograma_desembolsos.priv3 || 0);
    this.monto_publico = this.proyecto.financiamiento.cuadro.totales.publico
                        - (this.proyecto.cronograma_desembolsos.pub1 || 0)
                        - (this.proyecto.cronograma_desembolsos.pub2 || 0)
                        - (this.proyecto.cronograma_desembolsos.pub3 || 0);
    
  }

  recalcularPorcentaje() {
    // para calcular con porcentajes
      // this.proyecto.cronograma_desembolsos.monto_1 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_1/100]);
      // this.proyecto.cronograma_desembolsos.monto_2 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_2/100]);
      // this.proyecto.cronograma_desembolsos.monto_3 = this.Util.multiplicar([this.proyecto.financiamiento.monto_fdi,this.proyecto.cronograma_desembolsos.porcentaje_3/100]);
      // this.proyecto.cronograma_desembolsos.monto_4 = this.proyecto.financiamiento.monto_fdi - this.proyecto.cronograma_desembolsos.monto_1 - this.proyecto.cronograma_desembolsos.monto_2 -this.proyecto.cronograma_desembolsos.monto_3;
      // this.proyecto.cronograma_desembolsos.porcentaje_4 = 100 - this.proyecto.cronograma_desembolsos.porcentaje_3 - this.proyecto.cronograma_desembolsos.porcentaje_2 - this.proyecto.cronograma_desembolsos.porcentaje_1;
      // this.monto_3 = this.proyecto.cronograma_desembolsos.monto_3;
      // this.porcentaje_3 = this.proyecto.cronograma_desembolsos.porcentaje_3;
    //para calcular con montos
    this.proyecto.cronograma_desembolsos.porcentaje_1 = (this.proyecto.cronograma_desembolsos.monto_1 / this.proyecto.financiamiento.monto_fdi) * 100;
    this.proyecto.cronograma_desembolsos.porcentaje_2 = (this.proyecto.cronograma_desembolsos.monto_2 / this.proyecto.financiamiento.monto_fdi) * 100;
    this.proyecto.cronograma_desembolsos.porcentaje_3 = (this.proyecto.cronograma_desembolsos.monto_3 / this.proyecto.financiamiento.monto_fdi) * 100;
    
    this.proyecto.cronograma_desembolsos.monto_4 = this.proyecto.financiamiento.monto_fdi -  this.proyecto.cronograma_desembolsos.monto_1 - this.proyecto.cronograma_desembolsos.monto_2 - this.proyecto.cronograma_desembolsos.monto_3;
    this.proyecto.cronograma_desembolsos.porcentaje_4 = (this.proyecto.cronograma_desembolsos.monto_4 / this.proyecto.financiamiento.monto_fdi) * 100;

    // this.monto_privado = this.proyecto.financiamiento.cuadro.totales.privado - this.proyecto.cronograma_desembolsos.priv1 - this.proyecto.cronograma_desembolsos.priv2 - this.proyecto.cronograma_desembolsos.priv3;
    // this.monto_publico = this.proyecto.financiamiento.cuadro.totales.publico - this.proyecto.cronograma_desembolsos.pub1 - this.proyecto.cronograma_desembolsos.pub2 - this.proyecto.cronograma_desembolsos.pub3;
  if ((this.proyecto.cronograma_desembolsos.monto_1 + this.proyecto.cronograma_desembolsos.monto_2 + this.proyecto.cronograma_desembolsos.monto_3 ) > this.proyecto.financiamiento.monto_fdi){
    this.Modal.alert('El monto asignado supera los : '+ this.proyecto.monto_fdi);
      this.Message.info('El monto asignado para desembolso excede al montoFDI asignado');

    }

     this.monto_publico = this.proyecto.financiamiento.cuadro.totales.publico 
                        - (this.proyecto.cronograma_desembolsos.pub1 || 0) 
                        - (this.proyecto.cronograma_desembolsos.pub2 || 0) 
                        - (this.proyecto.cronograma_desembolsos.pub3 || 0);

    this.monto_privado = this.proyecto.financiamiento.cuadro.totales.privado
                        - (this.proyecto.cronograma_desembolsos.priv1 || 0) 
                        - (this.proyecto.cronograma_desembolsos.priv2 || 0) 
                        - (this.proyecto.cronograma_desembolsos.priv3 || 0);
  }

  sumaMontos(){
    this.proyecto.cronograma_desembolsos.monto_1 = (this.proyecto.cronograma_desembolsos.priv1 || 0) + (this.proyecto.cronograma_desembolsos.pub1 || 0)
    this.proyecto.cronograma_desembolsos.monto_2 = (this.proyecto.cronograma_desembolsos.priv2 || 0) + (this.proyecto.cronograma_desembolsos.pub2 || 0)
    this.proyecto.cronograma_desembolsos.monto_3 = (this.proyecto.cronograma_desembolsos.priv3 || 0) + (this.proyecto.cronograma_desembolsos.pub3 || 0)


    // this.monto_privado = this.proyecto.financiamiento.cuadro.totales.privado - this.proyecto.cronograma_desembolsos.priv1 - this.proyecto.cronograma_desembolsos.priv2 - this.proyecto.cronograma_desembolsos.priv3;
    // this.monto_publico = this.proyecto.financiamiento.cuadro.totales.publico - this.proyecto.cronograma_desembolsos.pub1 - this.proyecto.cronograma_desembolsos.pub2 - this.proyecto.cronograma_desembolsos.pub3;

    if ((this.proyecto.cronograma_desembolsos.monto_1 + this.proyecto.cronograma_desembolsos.monto_2 + this.proyecto.cronograma_desembolsos.monto_3 ) > this.proyecto.financiamiento.monto_fdi){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.monto_fdi);
      this.Message.warning('El monto asignado para desembolso excede al montoFDI asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.monto_1  > this.proyecto.financiamiento.monto_fdi){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.monto_fdi);
      this.Message.warning('El monto asignado para desembolso excede al montoFDI asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.monto_2  > this.proyecto.financiamiento.monto_fdi){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.monto_fdi);
      this.Message.warning('El monto asignado para desembolso excede al montoFDI asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.monto_3  > this.proyecto.financiamiento.monto_fdi){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.monto_fdi);
      this.Message.warning('El monto asignado para desembolso excede al montoFDI asignado');
      return;
    }

// validando montos excedidos de acuerdo a los montos privados y publicos
    if (this.proyecto.cronograma_desembolsos.priv1  > this.proyecto.financiamiento.cuadro.totales.privado){
      // this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.privado);
      this.Modal.alert('El monto asignado supero los : '+this.proyecto.financiamiento.cuadro.totales.privado, () => {
        this.proyecto.cronograma_desembolsos.priv1 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
       });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado*/**');
     
      return;
    }
    if (this.proyecto.cronograma_desembolsos.pub1  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico, () =>{
        this.proyecto.cronograma_desembolsos.pub1 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
      });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.priv2  > this.proyecto.financiamiento.cuadro.totales.privado){
      this.Modal.alert('El monto asignado supero los : '+this.proyecto.financiamiento.cuadro.totales.privado, () => {
        this.proyecto.cronograma_desembolsos.priv2 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
      });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.pub2  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico, () =>{
        this.proyecto.cronograma_desembolsos.pub2 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
      });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.priv3  > this.proyecto.financiamiento.cuadro.totales.privado){
      this.Modal.alert('El monto asignado supero los : '+this.proyecto.financiamiento.cuadro.totales.privado, () => {
        this.proyecto.cronograma_desembolsos.priv3 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
      });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if (this.proyecto.cronograma_desembolsos.pub3  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico, () =>{
        this.proyecto.cronograma_desembolsos.pub3 = 0;
         this.sumaMontos();
         this.recalcularPorcentaje();
      });
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }

    if ((this.proyecto.cronograma_desembolsos.priv1 + this.proyecto.cronograma_desembolsos.priv2 + this.proyecto.cronograma_desembolsos.priv3) > this.proyecto.financiamiento.cuadro.totales.privado){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.privado);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if ((this.proyecto.cronograma_desembolsos.pub1 + this.proyecto.cronograma_desembolsos.pub2 + this.proyecto.cronograma_desembolsos.pub3)  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }

    if ((this.proyecto.cronograma_desembolsos.priv1 + this.proyecto.cronograma_desembolsos.priv2 ) > this.proyecto.financiamiento.cuadro.totales.privado){
      // this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.privado);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if ((this.proyecto.cronograma_desembolsos.pub1 + this.proyecto.cronograma_desembolsos.pub2 )  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }

    if ((this.proyecto.cronograma_desembolsos.priv1 + this.proyecto.cronograma_desembolsos.priv3) > this.proyecto.financiamiento.cuadro.totales.privado){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.privado);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if ((this.proyecto.cronograma_desembolsos.pub1 + this.proyecto.cronograma_desembolsos.pub3)  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }

    if ((this.proyecto.cronograma_desembolsos.priv2 + this.proyecto.cronograma_desembolsos.priv3) > this.proyecto.financiamiento.cuadro.totales.privado){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.privado);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
    if ((this.proyecto.cronograma_desembolsos.pub2 + this.proyecto.cronograma_desembolsos.pub3)  > this.proyecto.financiamiento.cuadro.totales.publico){
      this.Modal.alert('El monto asignado supera los : '+ this.proyecto.financiamiento.cuadro.totales.publico);
      this.Message.warning('El monto asignado para desembolso excede al monto privado asignado');
      return;
    }
  }
  selFechaDesembolso() {
    this.$timeout(() => {
      this.proyecto.fecha_desembolso = this.proyecto.cronograma_desembolsos.fecha_1;
    }, 5);
  }
}

export default DesembolsoController;
