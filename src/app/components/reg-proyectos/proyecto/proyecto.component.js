'use strict';

import controller from './proyecto.controller';
import template from './proyecto.html';

const ProyectoComponent = {
  bindings: {},
  controller,
  template
};

export default ProyectoComponent;
