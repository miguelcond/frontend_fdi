'use strict';

import Admin from './admin/admin.module';
import Proyectos from './proyectos/proyectos.module';
import Empresas from './empresas/empresas.module';
import RegProyectos from './reg-proyectos/proyectos.module';
import RegEmpresas from './reg-empresas/empresas.module';
import Reportes from './reportes/reportes.module';

const Components = angular
    .module('app.components', [
        Admin,
        Proyectos,
        Empresas,
        RegProyectos,
        RegEmpresas,
        Reportes,
    ])
    .name;

export default Components;
