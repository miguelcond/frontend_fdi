'use strict';

class BandejaPendientesController {
  constructor(Storage, DataService, DataTable, $state, _, Modal) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.DataTable = DataTable;
    this.$state = $state;
    this._ = _;
    this.Modal = Modal;
  }

  $onInit() {
    this.usuario = angular.fromJson(this.Storage.getUser());
    this.config = this.getConfiguracion();
    this.configSupervisiones = this.getConfiguracionSupervisiones();
    const QUERY = this.esEmpresa()? {matricula: this.usuario.matricula} : {rol:this.Storage.get('rol').id_rol};
    this.tableParams = this.DataTable.getParams(`proyectos`, QUERY);
    this.verModificacion = this.puedeVerModificacion();
  }

  getConfiguracion() {
    if (!this.usuario.rol) {
      return;
    }
    let configuracion = {
      acciones: [{icon: 'edit', tooltip: 'Procesar proyecto', md: 'primary', metodo: this.procesarProyecto.bind(this)}]
    };
    return configuracion;
  }

  getConfiguracionSupervisiones() {
    var configuracion = {};
    if (!this.usuario.rol) {
      return;
    }
    switch (this.usuario.rol.nombre) {
      default:
        configuracion = {
          acciones: [
            {icon: 'edit', tooltip: 'Procesar planilla', md: 'primary', metodo: this.procesarPlanilla.bind(this)}
          ]
        }
    }
    return configuracion;
  }

  procesarProyecto(proyecto) {
    if (this.esEmpresa() && proyecto.estado_proyecto === 'REGISTRO_EMPRESA') {
      return this.documentarProyecto(proyecto);
    }
    if( (this.usuario.rol.nombre==='SUPERVISOR' && proyecto.estado_proyecto === 'MODIFICADO') ||
        ((this.usuario.rol.nombre==='TECNICO_UPRE' || this.usuario.rol.nombre==='RESP_DEPARTAMENTAL_UPRE') && proyecto.estado_proyecto === 'REVISION_MODIFICACION') ) {
      return this.procesarModificacion(proyecto);
    }
    this.$state.go('proyecto', {codigo: proyecto.id_proyecto});
  }

  documentarProyecto(proyecto) {
    this.$state.go('documentacion-proyecto', {codigo: proyecto.id_proyecto});
  }

  procesarPlanilla(proyecto) {
    this.$state.go('planilla-proyecto', {idSupervision: proyecto.supervisiones[0].id_supervision});
  }

  procesarModificacion(proyecto) {
    this.DataService.get(`proyectos/${proyecto.id_proyecto}/modificaciones/historial`).then((result)=>{
      if(result && result.finalizado) {
        let modificacion = result.datos[0];
        let tipoModificacion = modificacion.tipo_modificacion=='TM_ORDEN_TRABAJO'?1:modificacion.tipo_modificacion=='TM_ORDEN_CAMBIO'?2:modificacion.tipo_modificacion=='TM_CONTRATO_MODIFICATORIO'?3:0;
        if(modificacion && ['MODIFICADO','REVISION_MODIFICACION'].indexOf(modificacion.estado_proyecto)>=0) {
          this.$state.go('modificacion',{
            proyecto: proyecto.id_proyecto,
            version: modificacion.version,
            tipoModificacion,
            edicion: this.usuario.rol.nombre==='SUPERVISOR'
          });
        }
      }
    });
  }

  esEmpresa() {
    return !angular.isUndefined(this.usuario.nit);
  }

  puedeVerSupervision(proyecto) {
    if(proyecto) {
      return ['APROBADO','MODIFICADO'].indexOf(proyecto.estado_proyecto)>=0 && proyecto.supervisiones && proyecto.supervisiones.length > 0;
    }
    return false;
  }

  puedeVerModificacion(proyecto) {
    if(proyecto) {
      // TODO Se deberia obtener desde una configuracion del proyecto Base de DAtos
      return ['SUPERVISOR','TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE','EMPRESA'].indexOf(this.usuario.rol.nombre)>=0 && ['REVISION_MODIFICACION','MODIFICADO'].indexOf(proyecto.estado_actual.codigo)>=0;
    }
    return false;
  }
}

export default BandejaPendientesController;
