'use strict';

class NuevaAdjudicacionModalController {
  constructor($uibModalInstance, Storage, data, apiUrl, $state) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.apiUrl = apiUrl;
    this.$state = $state;
    this.proyecto = data;
  }

  $onInit() {
    this.asignarDatosDescarga();
  }

  asignarDatosDescarga() {
    this.proyecto.supervisiones.map(supervision => {
      supervision.urlPlanillaDescargaAvance = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/SUP_${supervision.nro_supervision}/descargar`;
      supervision.urlPlanillaDescarga = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/PA_${supervision.nro_supervision}/descargar`;
      supervision.nombrePlanillaDescarga = `Planilla de avance nro ${supervision.nro_supervision}`;
      supervision.urlComputosDescarga = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/CMR_${supervision.nro_supervision}/descargar`;
      supervision.nombreComputosDescarga = `Resumen de computos metricos planilla_${supervision.nro_supervision}`;
    });
  }

  irAPlanilla(supervision) {
    this.$state.go('planilla-proyecto', { idSupervision: supervision.id_supervision });
    this.$uibModalInstance.close(false);
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }
}

export default NuevaAdjudicacionModalController;
