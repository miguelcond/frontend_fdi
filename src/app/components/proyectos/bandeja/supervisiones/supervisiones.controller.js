'use strict';

import modalSupervisionesController from './supervisiones.modal.controller';
import modalSupervisionesTemplate from './supervisiones.modal.html';

class ListaSupervisionesController {
  constructor(Storage, DataService, DataTable, $state, _, Modal) {
    'ngInject';
    this.Storage = Storage;
    this.DataService = DataService;
    this.DataTable = DataTable;
    this.$state = $state;
    this._ = _;
    this.Modal = Modal;
  }

  $onInit() {

  }

  verSupervisiones() {
    this.Modal.show({
      template: modalSupervisionesTemplate,
      controller: modalSupervisionesController,
      data: this.proyecto,
      size: 'lg'
    });
  }

}

export default ListaSupervisionesController;
