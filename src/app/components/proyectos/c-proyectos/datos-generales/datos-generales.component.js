'use strict';

import controller from './datos-generales.controller';
import template from './datos-generales.html';

const DatosGenerales = {
  bindings:{
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default DatosGenerales;
