'use strict';

class DesembolsoController {
  constructor(Proyecto, _, Datetime) {
    'ngInject';
    this.Proyecto = Proyecto;
    this._ = _;
    this.Datetime = Datetime;
  }

  $onInit() {
    this.prepararValidacionesFecha();
  }

  prepararValidacionesFecha() {
    let fechaDesde, mensajeError = 'La fecha debe ser mayor a la fecha';
    if (this.tieneBoletas()) {
      fechaDesde = this.fechaMasAntiguaDeBoletas();
      mensajeError = `${mensajeError} de inicio de la boleta de anticipo más antigua`
    } else if (this.proyecto.tipo === 'TP_CIF') {
      fechaDesde = this.proyecto.fecha_desembolso;
      mensajeError = `${mensajeError} del desembolso`
    } else {
      fechaDesde = this.proyecto.fecha_suscripcion_convenio;
      mensajeError = `${mensajeError} del convenio`
    }
    fechaDesde = this.Datetime.getFormatDate(fechaDesde);
    this.validacion = {
      fecha_minima: this.Datetime.addDaysToDate(angular.copy(fechaDesde), 1),
      mensaje_fecminima: `${mensajeError} (${this.Datetime.parseDate(fechaDesde, '/')})`
    };
  }

  tieneBoletas() {
    return this.proyecto.boletas.length > 0;
  }

  fechaMasAntiguaDeBoletas() {
    return this._.sortBy(this.proyecto.boletas, (boleta) => {
      return boleta.fecha_inicio_validez;
    })[0].fecha_inicio_validez;
  }

}

export default DesembolsoController;
