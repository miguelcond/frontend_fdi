'use strict';

import controller from './items.controller';
import template from './items.html';
import './items.scss';

const ItemsComponent = {
  bindings:{
    proyecto: '=',
    modulos: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default ItemsComponent;
