'use strict';

import modalModuloController from './modulo.modal.controller';
import modalModuloTemplate from './modulo.modal.html';
import modalArchivoController from './archivo.modal.controller';
import modalArchivoTemplate from './archivo.modal.html';
import modalValidacionController from './modal/validacion.controller';
import modalValidacionTemplate from './modal/validacion.html';

class ItemsController {
  constructor(DataService, Util,  Modal, NgTableParams, _, Message, Proyecto, Storage) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Modal = Modal;
    this.NgTableParams = NgTableParams;
    this._ = _;
    this.Message = Message;
    this.Proyecto = Proyecto;
    this.user = Storage.getUser();
  }

  $onInit() {
    this.modulos = [];
    this.getListaModulos();
    this.getTipoUnidades();
  }

  getListaModulos(modulo) {
    this.DataService.get(`modulos/proyectos/${this.proyecto.id_proyecto}`).then(response => {
      if (response && response.finalizado) {
        this.modulos = response.datos.rows;
        if (modulo && this.modulos.length) {
          for (let i in this.modulos) {
            if (this.modulos[i].id_modulo==modulo.id_modulo) {
              this.modulos[i].show = true;
            }
          }
        }
      }
    });
  }

  getTipoUnidades() {
    this.DataService.get(`parametricas/UNIDAD`).then(response => {
      if (response) {
        this.unidades = response.datos.rows;
      }
    });
  }

  adicionarModulo() {
    const modulo = {
      fid_proyecto: this.proyecto.id_proyecto
    };
    const modalModulo = this.mostrarModal('Nuevo Módulo', 'fa-plus', modulo, modalModuloTemplate, modalModuloController);
    modalModulo.result.then(result => {
      if (result && result.datos) {
        this.modulos.push(result.datos);
      }
    });
  }

  editarModulo(modulo) {
    this.mostrarModal('Editando Módulo', 'fa-edit', modulo, modalModuloTemplate, modalModuloController);
  }

  eliminarModulo(modulo) {
    let msg = (modulo&&modulo.items&&modulo.items.length)? `¿Está seguro/a de eliminar el módulo ${modulo.nombre} y los ${modulo.items.length} items?`: `¿Está seguro/a de eliminar el módulo ${modulo.nombre}?`;
    this.Modal.confirm(msg, ()=>{
      this.DataService.delete(`modulos/${modulo.id_modulo}`).then(result => {
        if (result) {
          this.modulos = this.eliminarElementoDeLista(this.modulos, modulo, 'id_modulo');
          this.Message.success(`Se eliminó el módulo "${modulo.nombre}" correctamente.`);
          this.refrescarLista();
        }
      });
    }, null, null, 'Si, estoy seguro/a');
  }

  adicionarItem(modulo) {
    if (!modulo.items) {
      modulo.items = [];
    }
    for(let i in this.modulos) {
      for (let j in this.modulos[i].items) {
        if (!this.modulos[i].items[j].id_item || this.modulos[i].items[j].edicion) {
          this.Message.warning(`Debe guardar el item "${this.modulos[i].items[j].nombre||'Sin nombre'}"`);
          return;
        }
      }
    }
    modulo.items.push({edicion: true});
  }

  guardarItem(item, index, indexModulo, modulo) {
    if (!this.sonDatosValidosItem(modulo, index)) {
      return;
    }
    item.fid_modulo = modulo.id_modulo;
    this.DataService.post(`items`, item).then(response => {
      if (response) {
        this.Message.success(`Se registró el item "${item.nombre}" correctamente.`);
        this.modulos[indexModulo].items[index] = response.datos;
        this.refrescarLista(modulo);
      }
    });
  }

  actualizarItem(item, index, modulo) {
    if (!this.sonDatosValidosItem(modulo, index)) {
      return;
    }
    this.DataService.put(`items/${item.id_item}`, item).then(response => {
      if (response) {
        this.Message.success('Se actualizó los datos del item correctamente.');
        item.edicion = false;
      }
    });
  }

  eliminarItem(item, $index, modulo) {
    if (!item.id_item) {
      modulo.items.splice($index, 1);
      return;
    }
    this.DataService.delete(`items/${item.id_item}`).then(result => {
      if (result) {
        this.Message.success(`Se eliminó el item "${item.nombre}" correctamente.`);
        modulo.items = this.eliminarElementoDeLista(modulo.items, item, 'id_item');
        this.refrescarLista(modulo);
      }
    });
  }

  eliminarElementoDeLista(lista, elemento, atributo) {
    return this._.reject(lista, function(item) {
      return item[atributo] === elemento[atributo];
    });
  }

  mostrarModal(titulo, icon, data, template, controller, labelOk, size) {
    return  this.Modal.show({
      template: template,
      controller: controller,
      data: data || {},
      title: titulo,
      icon: icon,
      size: size?size:'md',
      labelOk: labelOk
    });
  }

  guardarDatosModulo(modulo) {
    this.DataService.put(`modulos/${modulo.id_modulo}`, modulo).then(response => {
      if (response) {
        //modulo.items = response.datos.items;
        this.Message.success(`Se guardó correctamente los datos del módulo "${modulo.nombre}".`);
        this.refrescarLista(modulo);
      }
    });
  }

  sonDatosValidosItem(modulo, posicion) {
    const nombreForm = `form_${modulo.id_modulo}`;
    const FORM = angular.element(document.querySelector(`[name=${nombreForm}]`)).scope()[nombreForm];
    let esValido = (
      FORM['item_nombre_'+posicion].$valid &&
      FORM['item_unidad_'+posicion].$valid &&
      FORM['item_cantidad_'+posicion].$valid
    );

    if(!esValido) {
      FORM['item_nombre_'+posicion].$setTouched();
      FORM['item_unidad_'+posicion].$setTouched();
      FORM['item_cantidad_'+posicion].$setTouched();
    }
    if (!esValido) {
      this.Message.warning('Verifique, corrija y/o llene los campos resaltados.');
      return false;
    }
    return true;
  }

  importarItems() {
    const confArchivo = {
      //formatos: 'csv, xlsx, ods', //types
      formatos: 'ods', //types
      tipos: '.csv,.xlsx,.ods', //mime-types
      cargarItems: true,
      mensajeInformacion: 'Puede importar items desde un <strong>archivo en formato .ods</strong>.'
    };
    this.mostrarModal('Importar Items', 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Importar')
    .result.then(documento => {
      const items = {
        id_proyecto: this.proyecto.id_proyecto,
        base64: documento.base64
      };
      this.DataService.post(`items/file`, items).then( result => {
        if (result.finalizado) {
          this.Message.success('Se importó correctamente los items.');
          this.modulos = result.datos;
        }
        if (result.erroresValidacion) {
          const modalModulo = this.mostrarModal('Observaciones en el archivo', 'fa-check', result.erroresValidacion, modalValidacionTemplate, modalValidacionController, null, 'lg');
          modalModulo.result.then(result => { result; });
        }
      });
    });
  }

  cargarEspecificacionesTecnicas() {
    const confArchivo = {
      formatos: 'pdf',
      tipos: '.pdf',
      mensajeInformacion: 'El archivo representará a las especificaciones técnicas de los items en general.'
    };
    this.mostrarModal(`Especificaciones Técnicas`, 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Cargar documento')
    .result.then( docEspecificaciones => {
      if (angular.isUndefined(docEspecificaciones)) {
        return;
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}`, {doc_especificaciones_tecnicas: docEspecificaciones, rol: this.user.rol.id_rol}).then(result => {
        if (result) {
          this.Message.success(`Se registró el documento de especificaciones técnicas correctamente.`);
          this.proyecto.doc_especificaciones_tecnicas = result.datos.doc_especificaciones_tecnicas;
        }
      });
    });
  }

  verEspecificacionesTecnicas() {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/ET`,
    `Especificaciones Técnicas`);
  }

  cargarEspecificacionesItem(item) {
    const confArchivo = {
      formatos: 'pdf',
      tipos: '.pdf'
    };
    this.mostrarModal(`Especificaciones Técnicas - "${item.nombre}"`, 'fa-upload', confArchivo, modalArchivoTemplate, modalArchivoController, 'Cargar documento')
    .result.then( docEspecificaciones => {
      if (angular.isUndefined(docEspecificaciones)) {
        return;
      }
      this.DataService.put(`items/${item.id_item}`, {especificaciones: docEspecificaciones}).then(result => {
        if (result && result.finalizado) {
          this.Message.success(`Se registró el documento de especificaciones técnicas del item "${item.nombre}" correctamente.`);
          item.especificaciones = docEspecificaciones;
        }
      });
    });
  }

  verEspecificacionesItem(item) {
    this.Modal.showDocumentBase64(`proyectos/${this.proyecto.id_proyecto}/doc/ET_${item.id_item}`, `Especificaciones Técnicas - "${item.nombre}"`);
  }

  refrescarLista(modulo) {
    this.getListaModulos(modulo);
  }

}

export default ItemsController;
