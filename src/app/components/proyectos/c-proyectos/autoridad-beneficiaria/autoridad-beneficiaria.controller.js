'use strict';

class AutoridadBeneficiariaController {
  constructor(DataService, Util, Proyecto) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
  }

  $onInit() {
    if (this.Util.isEmpty(this.proyecto.autoridad_beneficiaria)) {
      this.proyecto.autoridad_beneficiaria = {};
    }
  }


}

export default AutoridadBeneficiariaController;
