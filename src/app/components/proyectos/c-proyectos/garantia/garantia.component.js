'use strict';

import controller from './garantia.controller';
import template from './garantia.html';

const GarantiaComponent = {
  bindings: {
    proyecto: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default GarantiaComponent;
