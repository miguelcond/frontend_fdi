'use strict';

class GarantiaController {
  constructor($scope, Proyecto, _) {
    'ngInject';

    this.$scope = $scope;
    this.Proyecto = Proyecto;
    this._ = _;
  }

  $onInit() {
    this.cContrato = {
      porcentajeFijo: true,
      porcentaje: 7,
      monto: 0,
      nombrePorcentaje: 'porcentaje_ccontrato',
      nombreMonto: 'monto_ccontrato',
      tipo: 'TB_CUMPLIMIENTO_CONTRATO'
    };
    this.anticipo = {
      porcentajeFijo: false,
      porcentaje: this.proyecto.porcentaje_anticipo,
      monto: this.proyecto.anticipo,
      nombrePorcentaje: 'porcentaje_anticipo',
      nombreMonto: 'monto_anticipo',
      tipo: 'TB_CORRECTA_INVERSION'
    };

    this.prepararBoletas();
    this.$scope.$on('actualizarBoletas', () => {
      this.actualizarBoletas();
    });
  }

  prepararBoletas() {
    this.boletasCcontrato = this.getBoletasPorTipo('TB_CUMPLIMIENTO_CONTRATO');
    this.boletasAnticipo = this.getBoletasPorTipo('TB_CORRECTA_INVERSION');
  }

  getBoletasPorTipo(tipo) {
    const BOLETAS = angular.copy(this.proyecto.boletas);
    return this._.filter(BOLETAS, boleta => {
      return boleta.tipo_boleta === tipo;
    });
  }

  actualizarBoletas() {
    this.proyecto.boletas = this.boletasAnticipo.concat(this.boletasCcontrato);
  }
}

export default GarantiaController;
