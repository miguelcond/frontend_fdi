'use strict';

class ObservacionController {
    constructor($uibModalInstance, data) {
        'ngInject';
        this.$uibModalInstance = $uibModalInstance;
        this.data = data;
    }
    $onInit() {
      this.data.observacion = null;
    }

    cerrar() {
      this.data.observacion = null;
      this.$uibModalInstance.dismiss('cancel');
    }

    observar() {
      this.$uibModalInstance.close(this.data);
    }

}

export default ObservacionController;
