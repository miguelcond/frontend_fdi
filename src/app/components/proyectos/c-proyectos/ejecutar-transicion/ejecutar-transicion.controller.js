'use strict';

import modalObservacionController from './observacion.modal.controller.js';
import modalObservacionTemplate from './observacion.modal.html';

class EjecutarTransicion {
  constructor(DataService, Util, Proyecto, $state, Modal, Loading, Message, $scope, _, Storage, $q, $log, Computo) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.$state = $state;
    this.Modal = Modal;
    this.Loading = Loading;
    this.Message = Message;
    this.$scope = $scope;
    this._ = _;
    this.Storage = Storage;
    this.$q = $q;
    this.$log = $log;
    this.Computo = Computo;
    this.usuario = this.Storage.getUser();
  }

  $onInit() {
    
  }

  getRolUsuario() {
    return this.usuario.rol.id_rol;
  }

  puedeAcceder() {
    if (!this.elemento || !this.elemento.estado_actual || !this.elemento.estado_actual.roles) {
      return;
    }
    return this.elemento.estado_actual.roles.indexOf(this.getRolUsuario()) !== -1;
  }

  /**
   * ejecutarAccion - Ejecuta la transición para un elemento, verifica si previa a la ejecución
   * se debe registrar una observación o simplemente realizar un guardado (sin pasar a un siguiente estado)
   * o en todo caso si existe un método previo a ejeutar (obtenido del controlador de donde se llama a este componente)
   * se ejecuta el mismo para luego proceder a ejecutar la transición
   *
   * @param  {object} accion Objeto donde se tiene los detalles de la acción a ejecutar
   * @return {function}        Devuelve el método a ejecutar
   */
  ejecutarAccion(accion) {
    if (accion.observacion) {
      return this.registrarObservacion(accion);
    }
    if (accion.guardar) {
      return this.guardarDatos();
    }
    if (!this.metodoPrevio) {
      return this.ejecutarTransicion(accion);
    } else {
      this.metodoPrevio({accion}).then( (result) => {
        this.confirmar = result && result.confirmar ? result.confirmar : false;
        return this.ejecutarTransicion(accion);
      }).catch( () => { });
    }
  }

  registrarObservacion(accion) {
    let modalObservacion = this.Modal.show({
      template: modalObservacionTemplate,
      controller: modalObservacionController,
      data: this.elemento,
      size:'md'
    });

    modalObservacion.result.then((elementoActualizado) => {
      this.elemento = elementoActualizado;
      return this.ejecutarTransicion(accion)
    }).catch(() => {});
  }

  ejecutarTransicion(accion) {
    const DATOS = {};
    DATOS[this.atributoEstado] = accion.estado;
    const registrar = this.registrarTransicion.bind(this, DATOS, null, null, this.volverABandeja.bind(this));
    if (this.confirmar && !accion.observacion) {
      return this.confirmacionPrevia(registrar, accion);
    }
    return registrar();
  }

  confirmacionPrevia(callback, accion) {
    let mensaje = this.mensajeConfirmacion || `¿Está seguro/a de derivar el proceso a la siguiente instancia?`;
    if(this.mensajeButton && this.mensajeButton[accion.label]) {
      mensaje = this.mensajeButton[accion.label];
    }
    this.Modal.confirm( mensaje, () => {
        return callback();
    }, null, 'Confirmar envío', 'Si, estoy seguro/a', 'Cancelar');
  }

  registrarTransicion(data, mensajeLoading, mensajeExito, metodoPosterior) {
    this.Loading.show(mensajeLoading || 'Derivando', true);
    let DATOS = angular.merge(this.servicio.getDatosSegunEstado(this.elemento, this.form), data || {});
    DATOS.rol = this.usuario.rol.id_rol;
    this.DataService.put(`${this.url}`, DATOS)
    .then(response => {
      this.Loading.hide();
      if (response) {
        this.Message.success(mensajeExito || response.mensaje || 'Se envió el proceso a la siguiente instancia correctamente.');
        if (this.metodoPosterior) {
          this.$scope.$emit(this.metodoPosterior);
        } else if (metodoPosterior) {
          return metodoPosterior(response.datos);
        }
      }
    });
  }

  guardarDatos() {
    delete this.elemento[this.atributoEstado];
    return this.registrarTransicion(null, 'Guardando datos', 'Se guardó los datos', this.actualizarDatosElemento.bind(this));
  }

  /**
   * actualizarDatosElemento - Actualizando datos de un elemento, de acuerdo a los atributos modificados
   *
   * @param  {Object} aElemento Datos del elemento actualizado mediante servicio
   */
  actualizarDatosElemento(aElemento) {
    this._.keys(this.servicio.getDatosSegunEstado(this.elemento, this.form)).map(item => {
      this.elemento[item] = aElemento[item];
    });
    this.servicio.setCopia(this.elemento);
  }

  cancelar() {
    this.volverABandeja();
  }

  volverABandeja() {
    this.$state.go(this.usuario.pathInicio.replace('/',''));
  }

}

export default EjecutarTransicion;
