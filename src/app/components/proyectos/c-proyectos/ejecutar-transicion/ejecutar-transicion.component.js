'use strict';

import controller from './ejecutar-transicion.controller';
import template from './ejecutar-transicion.html';

const EjecutarTransicion = {
  bindings:{
    proyecto: '=',
    supervision: '=',
    elemento: '=',
    servicio: '<', // servicio del elemento (Proyecto/Computo)
    form: '<',
    atributoEstado: '@',
    url: '<',
    opcionCancelar: '<?',
    confirmar: '<?',
    mensajeConfirmacion: '@?',
    mensajeButton: '<?', // objeto de mensajes segun label del boton.
    metodoPrevio: '&?',
    metodoPosterior: '@?', // debe ser un string ya que se usará $emit
    validacionParalela: '&?' // metodo que permite realizar una validacion paralela en el componente origen conjuntamente la directiva "validarFormulario"
  },
  controller,
  template
};

export default EjecutarTransicion;
