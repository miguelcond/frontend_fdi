'use strict';
import modalRegistroController from './registro.modal.controller';
import modalRegistroTemplate from './registro.modal.html';

class EquipoTecnicoController {
  constructor(Modal, DataService, _, Usuarios, Message, Storage) {
    'ngInject';
    this.Modal = Modal;
    this.Message = Message;
    this.DataService = DataService;
    this.user = Storage.getUser();
    this._ = _;
    this.Usuarios = Usuarios;
    this.url = '';
    this.esEquipoCompleto = true;
  }

  $onInit() {
    this.url = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos`;
    this.getCargos();
    this.getListaEquipo();
    this.urlReconfirmacion = `proyectos/${this.proyecto.id_proyecto}/equipos_tecnicos/`;
    this.esResponsableProy = this.Usuarios.esResponsableProyecto();
  }

  getListaEquipo() {
    this.DataService.get(`${this.url}?rol=${this.user.rol.id_rol}`).then( result => {
      if (result && result.finalizado) {
        this.listaEquipo = result.datos.rows;
        this.verificarEquipoCompleto();
        this.proyecto.equipo_tecnico = this.listaEquipo;
      }
    });
  }

  getCargos() {
    this.DataService.get(`parametricas/CARGO`).then( response => {
      if (response) {
        this.cargos = response.datos.rows;
        this.verificarEquipoCompleto();
      }
    });
  }

  adicionar(callbackOk, callbackCancel) {
    let adicionMiembro = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {equipo: this.listaEquipo, cargos: this.cargos, registrar: !callbackOk},
      size: 'lg'
    });
    adicionMiembro.result.then( (result) => {
      if (result) {
        if (typeof(callbackOk)==='function') {
          callbackOk(result);
        }
        this.getListaEquipo();
      } else {
        if (typeof(callbackCancel)==='function') {
          callbackCancel();
        }
      }
    });
  }

  verificarEquipoCompleto() {
    var nroMiembros = 0;
    this.cargos.map(cargo => {
      const MIEMBRO = this._.find(this.listaEquipo, (miembro) => {
        return miembro.cargo === cargo.codigo;
      });
      if (MIEMBRO) {
        nroMiembros ++;
      }
    });
    this.esEquipoCompleto = this.cargos.length === nroMiembros;
  }

  esSupervisorOFiscal(usuario) {
    return  usuario.estado=='ACTIVO' && (usuario.cargo === 'CG_SUPERVISOR' || usuario.cargo === 'CG_FISCAL')
  }

  verDatos(miembro) {
    const EDICION = this.Modal.show({
      template: modalRegistroTemplate,
      controller: modalRegistroController,
      data: {equipo: this.listaEquipo, cargos: this.cargos, miembro: miembro, modoVista: this.modoVista},
      size: 'lg'
    });

    EDICION.result.then( (result) => {
      if (result) {
        this.getListaEquipo();
      }
    });
  }

  inactivarUsuario(usuario) {
    this.Modal.confirm(`Esta seguro/a de inhabilitar al ${usuario.cargo_usuario.nombre}?<br><br>Este usuario ya no podra ser habilitado nuevamente con el mismo cargo.<br><br>* Deberá agregar un nuevo usuario.`,
      ()=>{
        usuario.estado = 'INACTIVO';
        this.adicionar((persona)=>{
          this.DataService.put(`${this.url}/${usuario.id_equipo_tecnico}/reemplazar`, persona).then((resp)=>{
            if(resp && resp.finalizado) {
              this.getListaEquipo();
            } else {
              this.Message.warning('No se pudo realizar la inhabilitación.');
            }
          });
        }, ()=>{
          usuario.estado = 'ACTIVO';
        });
      }, null, 'Inhabilitar', 'Si estoy seguro/a', null, 'ban');
  }
}

export default EquipoTecnicoController;
