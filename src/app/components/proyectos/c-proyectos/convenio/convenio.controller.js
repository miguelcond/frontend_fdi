'use strict';

class DatosGenerales {
  constructor($scope, Util, Proyecto, Modal, _) {
    'ngInject';
    this.$scope = $scope;
    this.Util = Util;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this._ = _;
    this.archivos = null;
  }

  $onInit() {
    this.archivos = {
      urlDocConvenio: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/C` : '',
      urlDocConvenioDescarga: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/C/descargar` : '',
      urlDocResolucion: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/RM` : '',
      urlDocResolucionDescarga: this.proyecto? `proyectos/${this.proyecto.id_proyecto}/doc/RM/descargar` : ''
    };
    this.mostrar = true;
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.proyecto.monto_upre', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
    this.$scope.$watch('$ctrl.proyecto.monto_contraparte', () => {
      if (!this.proyecto) {
        return;
      }
      this.calcularMontoConvenio();
    });
  }

  calcularMontoConvenio() {
    this.proyecto.monto_total_convenio = this.Util.redondear((this.proyecto.monto_upre ? this.proyecto.monto_upre : 0) + (this.proyecto.monto_contraparte ? this.proyecto.monto_contraparte : 0));
  }

}

export default DatosGenerales;
