'use strict';

class BoletaGarantiaController {
  constructor($state, $scope, Storage, Proyecto, Modal, Message, DataService, Util, _, Datetime) {
    'ngInject';

    this.$scope = $scope;
    this.$state = $state;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Message = Message;
    this.DataService = DataService;
    this.Util = Util;
    this._ = _;
    this.Datetime = Datetime;
    this.mostrarBoletas = false;
    this.auxBoleta = {};
    this.completado = false;

    this.user = Storage.getUser();
    this.pModificar = false;
  }

  $onInit() {
    if (!this.boletas) {
      this.boletas = [];
    }
    if (!this.boletasEx) {
      this.boletasEx = [];
    }
    if (this.proyecto.tipo === 'TP_CIF') {
      let fechaDesembolso = this.Datetime.getFormatDate(angular.copy(this.proyecto.fecha_desembolso));
      this.validaciones = {
        fec_ini_minima: this.Datetime.addDaysToDate(angular.copy(fechaDesembolso), 1),
        mensaje_fecminima: `La fecha debe ser mayor a la fecha del desembolso (${this.Datetime.parseDate(fechaDesembolso, '/')})`
      };
      this.pModificar = 'EMPRESA'===this.user.rol.nombre && this.modoVista;
    } else {
      let fechaConvenio = this.Datetime.getFormatDate(angular.copy(this.proyecto.fecha_suscripcion_convenio));
      this.validaciones = {
        fec_ini_minima: this.Datetime.addDaysToDate(angular.copy(fechaConvenio), 1),
        mensaje_fecminima: `La fecha debe ser mayor a la fecha del convenio (${this.Datetime.parseDate(fechaConvenio, '/')})`
      };
      this.pModificar = 'FINANCIERO_UPRE'===this.user.rol.nombre && this.modoVista;
    }

    // validaciones para modificar boletas
    this.validacionesEx = {
      fec_ini_minima: this.validaciones.fec_ini_minima,
      mensaje_fecminima: this.validaciones.mensaje_fecminima
    };
    if(this.boletas.length==1) {
      this.validacionesEx.fec_ini_minima = new Date(this.boletas[0].fecha_fin_validez);
      this.validacionesEx.mensaje_fecminima = `La fecha debe ser mayor o igual a la boleta anterior (${this.Datetime.parseDate(this.validacionesEx.fec_ini_minima, '/')})`;
    }
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.porcentaje', () => {
      if (!this.porcentaje) {
        this.monto = null;
      }
      this.verificarValor();
    });
    this.$scope.$watch('$ctrl.boletas', () => {
      this.$scope.$emit('actualizarBoletas');
      this.verificarMontoTotalAnticipo();
    }, true);
    this.$scope.$watch('$ctrl.boletasEx', () => {
      this.verificarMontoTotalAnticipo();
    }, true);
  }

  verificarValor() {
    this.calcularAnticipo();
    if (this.porcentaje === 0) {
      this.boletas = [];
    }
    this.mostrarBoletas = this.porcentaje > 0;
  }

  calcularAnticipo() {
    if (typeof this.porcentaje === 'number' && this.porcentaje <= 20) {
      this.monto = this.Util.getPorcentaje(this.proyecto.monto_total, this.porcentaje);
    }
  }

  adicionarBoleta() {
    this.boletas.push({ tipo_boleta: this.tipo, estado: 'ACTIVO' });
  }

  quitarBoleta(indice) {
    this.boletas.splice(indice, 1);
  }
  invalidarBoleta(indice) {
    this.Modal.confirm(`¿Esta seguro/a de renovar esta boleta?\nDeberá agregar una nueva boleta.`, ()=>{
      let datos = {
        id_boleta: this.boletas[indice].id_boleta,
        estado: 'INACTIVO'
      }
      this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/boletas/${datos.id_boleta}`, datos).then((resp)=>{
        if(resp && resp.finalizado) {
          this.Message.success(`La boleta fue invalidada correctamente.`);
          this.boletas.splice(indice, 1);
        }
      });
    });
  }

  verificarMontoTotalAnticipo() {
    //this.sumaMontoBoletas = this.boletas.length > 0 ? this._.sum(this._.map(this.boletas, 'monto')) : 0;
    this.sumaMontoBoletas = 0;
    for(let i in this.boletas) {
      if(this.boletas[i].estado=='ACTIVO') {
        this.sumaMontoBoletas += parseFloat(this.boletas[i].monto);
      }
    }
    for(let i in this.boletasEx) {
      if(parseFloat(this.boletasEx[i].monto)) {
        this.sumaMontoBoletas += parseFloat(this.boletasEx[i].monto);
      }
    }
    this.completado = this.monto ? this.sumaMontoBoletas >= this.monto : false;
    this.incompleto = this.sumaMontoBoletas < this.monto;
    if (this.monto) {
      this.mensaje = {
        class: this.completado? 'success' : this.incompleto? 'warning' : '',
        mensaje: this.completado? 'Se completó el monto' : this.incompleto? 'Debe completar el monto' : '',
        icono: this.completado? 'check' : this.incompleto? 'warning' : ''
      };
    }
  }

  // Boletas extras
  adicionarBoletaEx() {
    this.boletasEx.push({ tipo_boleta: this.tipo, estado: 'ACTIVO' });
  }
  quitarBoletaEx(indice) {
    this.boletasEx.splice(indice, 1);
  }
  enviarBoletaEx() {
    this.Modal.confirm(`¿Esta seguro/a de agregar las boletas?`, ()=>{
      this.DataService.post(`boletas/proyecto/${this.proyecto.id_proyecto}`, {boletas:this.boletasEx}).then((resp)=>{
        if(resp && resp.finalizado) {
          this.Message.success(`Boleta(s) agregadas correctamente.`);
          this.$state.reload();
        }
      });
    });
  }

}

export default BoletaGarantiaController;
