'use strict';

class BoletaController {
  constructor($scope, Proyecto, Modal, Util) {
    'ngInject';

    this.$scope = $scope;
    this.Proyecto = Proyecto;
    this.Modal = Modal;
    this.Util = Util;
    this.mostrarBoletas = false;
  }

  $onInit() {
    this.tiposDocumento = [
      {codigo: 'BOLETA', nombre: 'Boleta'},
      {codigo: 'POLIZA', nombre: 'Póliza'},
    ];

    this.opcionesFechaInicio = {
      maxDateKey: 'fecha_fin_validez',
    };

    this.opcionesFechaFin = {
      minDateKey: 'fecha_inicio_validez',
    };
  }

}

export default BoletaController;
