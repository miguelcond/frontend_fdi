'use strict';

import controller from './boleta.controller';
import template from './boleta.html';

const BoletaComponent = {
  bindings:{
    model: '=',
    indice: '<',
    validaciones: '<',
    tipoBoleta: '<',
    modoVista: '<',
    bid: '<'
  },
  controller,
  template
};

export default BoletaComponent;
