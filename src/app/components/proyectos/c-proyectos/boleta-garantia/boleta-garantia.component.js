'use strict';

import controller from './boleta-garantia.controller';
import template from './boleta-garantia.html';
import './boleta-garantia.scss';

const BoletaGarantiaComponent = {
  bindings:{
    proyecto: '=',
    datos: '=',
    porcentaje: '=',
    monto: '=',
    tipo: '@',
    boletas: '=',
    modoVista: '<'
  },
  controller,
  template
};

export default BoletaGarantiaComponent;
