'use strict';

import DatosGenerales from './datos-generales/datos-generales.component';
import Convenio from './convenio/convenio.component';
import AsignacionResponsable from './asignacion-responsable/asignacion-responsable.component';
import Desembolso from './desembolso/desembolso.component';
import AutoridadBeneficiaria from './autoridad-beneficiaria/autoridad-beneficiaria.component';
import Items from './items/items.component';
// import EjecutarTransicion from './ejecutar-transicion/ejecutar-transicion.component';
import Dbc from './dbc/dbc.component';
import Garantia from './garantia/garantia.component';
import BoletaGarantia from './boleta-garantia/boleta-garantia.component';
import Boleta from './boleta-garantia/boleta/boleta.component';
// import EquipoTecnico from './equipo-tecnico/equipo-tecnico.component';
import OrdenProceder from './orden-proceder/orden-proceder.component';

const ComponentsProyectos = angular
    .module('app.cforms', [])
    .component('datosGenerales', DatosGenerales)
    .component('convenio', Convenio)
    .component('asignacionResponsable', AsignacionResponsable)
    .component('desembolso', Desembolso)
    .component('autoridadBeneficiaria', AutoridadBeneficiaria)
    .component('items', Items)
    // .component('ejecutarTransicion', EjecutarTransicion)
    .component('dbc', Dbc)
    .component('garantia', Garantia)
    .component('boletaGarantia', BoletaGarantia)
    .component('boleta', Boleta)
    // .component('equipoTecnico', EquipoTecnico)
    .component('ordenProceder', OrdenProceder)
    .name;

export default ComponentsProyectos;
