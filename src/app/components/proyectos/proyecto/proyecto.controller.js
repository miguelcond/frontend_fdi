'use strict';

class ProyectoController {
  constructor($stateParams, Storage, Proyecto, $state, $q, Message, _, Modal) {
    'ngInject';
    this.$stateParams = $stateParams;
    this.Proyecto = Proyecto;
    this.Storage = Storage;
    this.$q = $q;
    this.$state = $state;
    this.Message = Message;
    this._ = _;
    this.Modal = Modal;
  }

  $onInit() {
    this.idProyecto = this.$stateParams.codigo;
    this.Proyecto.setEstadoActual(null);
    this.getProyecto();
    this.url = `proyectos/${this.idProyecto}?rol=${this.Storage.get('rol').id_rol}`;
  }

  getProyecto() {
    this.Proyecto.get(this.idProyecto, this.$stateParams.urlProcesado).then(response => {
      if (response) {
        this.Proyecto.setEstadoActual(response.datos.estado_actual);
        this.proyecto = response.datos;
        this.estadoActual = response.datos.estado_actual;
      } else {
        this.$state.go('pendientes');
      }
    });
  }

  verificarSegunEstado() {
    let defer = this.$q.defer();
    if (this.Proyecto.esEditable('autoridad_beneficiaria')) {
      if (!this.sonDatosCompletosAutoridad()) {
        defer.reject();
      }
    }
    if (this.Proyecto.esEditable('equipo_tecnico')) {
      if (!this.tieneEquipoTecnico()) {
        this.Modal.alert(`Debe registrar al Supervisor y Fiscal del proyecto, en el apartado Equipo Técnico.`, null, 'Información incompleta', null, 'md');
      } else {
        defer.resolve({confirmar: true});
        return defer.promise;
      }
    } else {
      defer.resolve({confirmar: true});
    }
    return defer.promise;
  }

  tieneEquipoTecnico() {
    const TIENE_SUPERVISOR = this._.find(this.proyecto.equipo_tecnico, {cargo: 'CG_SUPERVISOR'});
    const TIENE_FISCAL = this._.find(this.proyecto.equipo_tecnico, {cargo: 'CG_FISCAL'});
    return TIENE_SUPERVISOR && TIENE_FISCAL ? true : false;
  }

  sonDatosCompletosAutoridad() {
    let mensaje = [];
    if (!this.proyecto.sector) {
      mensaje.push({area: 'Datos Generales', mensaje: 'Debe registrar el sector correspondiente al proyecto'});
    }
    if (!this.proyecto.autoridad_beneficiaria.nombres || !this.proyecto.autoridad_beneficiaria.telefono || !this.proyecto.cargo_autoridad_beneficiaria) {
      mensaje.push({area: 'MAE Beneficiarios/as', mensaje: 'Datos de contacto de los/las beneficiarios/as'});
    }
    if (!this.modulos || this.modulos.length < 1) {
      mensaje.push({area: 'Módulos/Items', mensaje: 'Registre al menos un módulo'});
    } else {
      const INCOMPLETOS = this.getModulosIncompletos();
      if (INCOMPLETOS > 0) {
        mensaje.push({area: 'Módulos/Items', mensaje: INCOMPLETOS > 1 ? `Algunos módulos no tienen items registrados, o no fueron guardados.` : `El módulo no tiene items registrados, o no fue guardado.`});
      }
    }
    if (this.Proyecto.esEditable('doc_base_contratacion') && !this.proyecto.doc_base_contratacion) {
      mensaje.push({area: 'DBC', mensaje: 'Registre el Documento Base de Contratación'});
    }
    if (mensaje.length > 0) {
      let texto = '<p>Debe completar la información de los siguientes apartados:</p><ul>';
      mensaje.map(obs => {
        texto += `<li><strong>${obs.area}</strong>: ${obs.mensaje}</li>`;
      });
      texto +='</ul>';
      this.Modal.alert(texto, () => { return false; }, 'Información incompleta', null, 'md');
    } else {
      return true;
    }
  }

  getModulosIncompletos() {
    let incompletos = 0;
    this.modulos.map(modulo => {
      if (!modulo.items || modulo.items.length < 1) {
        incompletos ++;
      }
      for (let i in modulo.items) {
        if (!modulo.items[i].id_item || modulo.items[i].edicion) {
          incompletos ++;
        }
      }
    });
    return incompletos;
  }

  verificarEnParalelo() {
    if (this.Proyecto.esEditable('autoridad_beneficiaria')) {
      return this.sonDatosCompletosAutoridad();
    }
    return false;
  }
}

export default ProyectoController;
