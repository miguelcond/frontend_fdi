'use strict';

import ProyectoComponent from './proyecto.component';

const Proyecto = angular
    .module('app.proyecto', [])
    .component('proyecto', ProyectoComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('proyecto', {
                url: '/proyecto/{codigo: int}',
                component: 'proyecto',
                params: {
                  urlProcesado: ''
                }
            });
    })
    .name;

export default Proyecto;
