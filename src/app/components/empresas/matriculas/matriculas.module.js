'use strict';

import MatriculasComponent from './matriculas.component';

const Matriculas = angular
    .module('app.matriculas', [])
    .component('matriculas', MatriculasComponent)
    .config(($stateProvider) => {
        $stateProvider
            .state('matriculas', {
                url: '/matriculas',
                component: 'matriculas'
            });
    })
    .name;

export default Matriculas;
