'use strict';

class MatriculasController {
  constructor(DataService, Storage, SidenavFactory, $state, Loading) {
    'ngInject';
    this.DataService = DataService;
    this.Storage = Storage;
    this.SidenavFactory = SidenavFactory;
    this.$state = $state;
    this.Loading = Loading;
  }

  $onInit() {
    this.getMatriculas();
  }

  getMatriculas() {
    this.Loading.show('Cargando datos.', true);
    this.DataService.get(`empresas/${this.Storage.getUser().nit}/matriculas`).then( response => {
      this.Loading.hide();
      if (response) {
        if (response.datos.length === 1) {
          this.cargarDatosMatricula(response.datos[0]);
        } else {
          this.matriculas = response.datos;
        }
      }
    });
  }

  cargarDatosMatricula(matricula) {
    const empresa = Object.assign(this.Storage.getUser(), {
      matricula: matricula.IdMatricula,
      username: matricula.RazonSocial,
    });
    this.Storage.setUser(empresa);
    this.SidenavFactory.setUser(empresa);
    this.$state.go('pendientes');
  }

}

export default MatriculasController;
