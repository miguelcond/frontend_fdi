'use strict';

import controller from './matriculas.controller';
import template from './matriculas.html';
import './matriculas.scss';

const MatriculasComponent = {
  bindings: {},
  controller,
  template
};

export default MatriculasComponent;
