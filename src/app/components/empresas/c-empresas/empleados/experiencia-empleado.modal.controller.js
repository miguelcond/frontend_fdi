'use strict';

class FormExperienciaEmpleadoController {
  constructor($uibModalInstance, data) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.data = data;
    this.empleado = data.empleado;
  }

  $onInit() {
    this.camposExperiencia = this.definirColumnasEmpleados();
  }

  definirColumnasEmpleados() {
    return [
      { title: 'Empresa/Entidad', field: 'nombre_empresa', filter: { nombre_empresa: "text" }, sortable: "nombre_empresa" },
      { title: 'Objeto de la obra', field: 'objeto_obra', filter: { objeto_obra: "text" }, sortable: "objeto_obra" },
      { title: 'Monto de la obra en Bs.', field: 'monto_obra', filter: { monto_obra: "text" }, sortable: "monto_obra" },
      { title: 'Cargo', field: 'cargo', filter: { cargo: "text" }, sortable: "cargo" },
      { title: 'Desde', field: 'fecha_inicio', filter: { fecha_inicio: "text" }, sortable: "fecha_inicio" },
      { title: 'Hasta', field: 'fecha_fin', filter: { fecha_fin: "text" }, sortable: "fecha_fin" }
    ];
  }

  aceptar() {
    this.$uibModalInstance.close(null);
  }

  cerrar() {
    this.$uibModalInstance.dismiss('cancel');
  }
}

export default FormExperienciaEmpleadoController;
