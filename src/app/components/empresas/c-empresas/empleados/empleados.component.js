'use strict';

import controller from './empleados.controller';
import template from './empleados.html';

const EmpleadosComponent = {
  bindings: {
    empleados: '='
  },
  controller,
  template
};

export default EmpleadosComponent;
