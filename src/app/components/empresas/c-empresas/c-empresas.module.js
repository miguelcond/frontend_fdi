'use strict';

import ProyectoAdjudicado from './proyecto-adjudicado/proyecto-adjudicado.component';
import CodigoContrato from './codigo-contrato/codigo-contrato.component';
import DatosEmpresa from './datos-empresa/datos-empresa.component';
import DatosAsociacion from './datos-asociacion/datos-asociacion.component';
import DatosIntegrantes from './datos-integrantes/datos-integrantes.component';
import PresupuestoItems from './presupuesto-items/presupuesto-items.component';
import ExperienciaEmpresa from './experiencia/experiencia.component';
import Empleados from './empleados/empleados.component';
import EquipoMinimo from './equipo-minimo/equipo-minimo.component';
import Cronogramas from './cronogramas/cronogramas.component';
import FormulariosGenerados from './formularios-generados/formularios.component';

const CEmpresas = angular
  .module('app.cEmpresas', [])
  .component('proyectoAdjudicado', ProyectoAdjudicado)
  .component('codigoContrato', CodigoContrato)
  .component('datosEmpresa', DatosEmpresa)
  .component('datosAsociacion', DatosAsociacion)
  .component('datosIntegrantes', DatosIntegrantes)
  .component('presupuestoItems', PresupuestoItems)
  .component('experienciaEmpresa', ExperienciaEmpresa)
  .component('empleados', Empleados)
  .component('equipoMinimo', EquipoMinimo)
  .component('cronogramas', Cronogramas)
  .component('formulariosGenerados', FormulariosGenerados)
  .name;

export default CEmpresas;
