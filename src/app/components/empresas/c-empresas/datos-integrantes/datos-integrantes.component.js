'use strict';

import controller from './datos-integrantes.controller';
import template from './datos-integrantes.html';

const DatosIntegrantesComponent = {
  bindings: {
    integrantes: '='
  },
  controller,
  template
};

export default DatosIntegrantesComponent;
