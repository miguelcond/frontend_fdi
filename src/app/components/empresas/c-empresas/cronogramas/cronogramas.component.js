'use strict';

import controller from './cronogramas.controller';
import template from './cronogramas.html';
import './cronogramas.scss';

const cronogramasComponent = {
  bindings: {
    proyecto: '='
  },
  controller,
  template
};

export default cronogramasComponent;
