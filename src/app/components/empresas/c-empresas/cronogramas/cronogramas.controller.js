'use strict';

class CronogramasController {
  constructor(DataService, _, $log, Modal, apiUrl) {
    'ngInject';

    this.DataService = DataService;
    this._ = _;
    this.$log = $log;
    this.Modal = Modal;
    this.apiUrl = apiUrl;
  }

  $onInit() {
    this.documentos = this.getConfiguracion();
    this.verificarArchivosCargados();
  }

  getConfiguracion() {
    return {
      lista: [
        {
          codigo: 'c_ejecucion',
          nombre: 'Cronograma de ejecución',
          nombre_formulario: 'Formulario A8',
          tipo: 'A8',
          urlDescarga: `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/A8/descargar`
        },
        {
          codigo: 'c_desembolsos',
          nombre: 'Cronograma de Desembolsos',
          nombre_formulario: 'Formulario B5',
          tipo: 'B5',
          urlDescarga: `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/B5/descargar`
        },
        {
          codigo: 'analisis_precios_unitarios',
          nombre: 'Análisis de Precios Unitarios',
          nombre_formulario: 'Formulario B2',
          tipo: 'B2',
          urlDescarga: `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/B2/descargar`
        }
      ],
      uploadCallback: (file) => {
        var data = {
          documento: angular.copy(file)
        }
        this.DataService.put(`proyectos/${this.proyecto.id_proyecto}/doc/${this.getTipo(file)}/subir`, data).then(response => {
          this.$log.log('response------', response);
          if (response && response.finalizado) {
            this.asignarDatos(file, response.datos.documento);
          }
        });
      }
    };
  }

  verificarArchivosCargados() {
    this.documentos.lista.map(documento => {
      const FORMULARIO = this.getFormularioRegistrado(documento);
      if (FORMULARIO) {
        documento.registrado = true;
        documento.nombre_archivo = FORMULARIO.nombre_archivo;
      }
    });
  }

  asignarDatos(archivo, docCreado) {
    const index = this._.findIndex(this.documentos.lista, {codigo: archivo.name});
    this.documentos.lista[index].base64 = archivo.base64;
    this.documentos.lista[index].nombre_formulario = docCreado.nombre;
    this.documentos.lista[index].nombre_archivo = docCreado.nombre_archivo;
    this.documentos.lista[index].registrado = true;
    let formulario = this.getFormularioRegistrado(docCreado);
    if (!formulario) {
      if (!this.proyecto.formularios) {
        this.proyecto.formularios = [];
      }
      this.proyecto.formularios.push(docCreado);
    } else {
      formulario = docCreado;
    }
  }

  getFormularioRegistrado(documento) {
    return this._.find(this.proyecto.formularios, {codigo_documento: documento.tipo});
  }

  cambiar(documento) {
    if (documento.registrado) {
      this.Modal.confirm(`¿Está seguro/a de cambiar el ${documento.nombre}?`, () => {
        documento.registrado = false;
      }, null, null, 'Si, estoy seguro', 'Cancelar')
    }
  }

  verDocumento(documento) {
    const URL = `proyectos/${this.proyecto.id_proyecto}/doc/${documento.tipo}`;
    this.Modal.showDocumentBase64(URL, `${documento.nombre}`);
  }

  getTipo(archivo) {
    return this._.find(this.documentos.lista, {codigo: archivo.name}).tipo;
  }
}

export default CronogramasController
