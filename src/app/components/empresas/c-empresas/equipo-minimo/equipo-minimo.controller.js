'use strict';

class EquipoMinimoController {
  constructor(DataTable) {
    'ngInject';

    this.DataTable = DataTable;
  }

  $onInit() {
    this.camposEquipo = this.definirColumnasEquipoTrabajo();
  }

  definirColumnasEquipoTrabajo() {
      return [
        { title: 'Descripción', field: 'descripcion', filter: { descripcion: "text" }, sortable: "descripcion" },
        { title: 'Unidad', field: 'unidad', filter: { unidad: "text" }, sortable: "unidad" },
        { title: 'Cantidad', field: 'cantidad', filter: { cantidad: "text" }, sortable: "cantidad" },
        { title: 'Potencia', field: 'potencia', filter: { potencia: "text" }, sortable: "potencia" },
        { title: 'Capacidad', field: 'capacidad', filter: { capacidad: "text" }, sortable: "capacidad" },
        { title: 'Campo adicional', field: 'campo_adicional', filter: { campo_adicional: "text" }, sortable: "campo_adicional" },
      ];
    }
}

export default EquipoMinimoController;
