'use strict';

import controller from './equipo-minimo.controller';
import template from './equipo-minimo.html';

const EquipoMinimoComponent = {
  bindings: {
    equipo: '='
  },
  controller,
  template
};

export default EquipoMinimoComponent;
