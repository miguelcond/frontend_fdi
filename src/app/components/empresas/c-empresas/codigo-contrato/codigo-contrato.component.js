'use strict';

import controller from './codigo-contrato.controller';
import template from './codigo-contrato.html';

const CodigoContratoComponent = {
  bindings: {
    proyecto: '=',
    modoVista: '<',
    tieneDocumentacion: '=?',
    codigoDocumentacion: '<?'
  },
  controller,
  template
};

export default CodigoContratoComponent;
