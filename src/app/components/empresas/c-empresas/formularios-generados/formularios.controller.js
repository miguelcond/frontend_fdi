'use strict';

class FormulariosGeneradosController {
  constructor(DataService, $stateParams, Message, _, Modal, apiUrl) {
    'ngInject';

    this.DataService = DataService;
    this.$stateParams = $stateParams;
    this.Message = Message;
    this._ = _ ;
    this.Modal = Modal;
    this.apiUrl = apiUrl;
    this.url = '';
  }

  $onInit() {
    this.url = `proyectos/${this.proyecto.id_proyecto}/doc`;
    this.prepararLista();
  }

  prepararLista() {
    if (this.proyecto.formularios) {
      this.asignarPath();
    } else {
      this.DataService.get(this.url).then( result => {
        if (result && result.finalizado) {
          this.proyecto.formularios = result.datos.formularios;
          this.asignarPath();
        }
      });
    }
  }

  asignarPath() {
    this.proyecto.formularios.map(formulario => {
      formulario.path = `${this.apiUrl}proyectos/${this.proyecto.id_proyecto}/doc/${formulario.codigo_documento}/descargar`
    });
  }

  ver(formulario) {
    this.Modal.showDocumentBase64(`${this.url}/${formulario.codigo_documento}`, `${formulario.nombre}`);
  }

}

export default FormulariosGeneradosController;
