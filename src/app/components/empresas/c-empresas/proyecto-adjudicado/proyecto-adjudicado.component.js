'use strict';

import controller from './proyecto-adjudicado.controller';
import template from './proyecto-adjudicado.html';

const ProyectoAdjudicadoComponent = {
  bindings: {
    proyecto: '<'
  },
  controller,
  template
};

export default ProyectoAdjudicadoComponent;
