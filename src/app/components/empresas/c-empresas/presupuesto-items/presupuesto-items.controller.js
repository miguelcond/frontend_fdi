'use strict';

class PresupuestoItemsController {
  constructor(DataService, $stateParams, Message, _, Util, Big, decimales) {
    'ngInject';

    this.DataService = DataService;
    this.$stateParams = $stateParams;
    this.Message = Message;
    this._ = _ ;
    this.Util = Util;
    this.Big = Big;
    this.decimales = decimales;
    this.precioTotal = 0;
  }

  $onInit() {
    // Monto fisico total avanzado hasta la rescision de contrato.
    if (this.proyecto.monto_total_ejecutado) {
      this.proyecto.monto_total_actual = this.proyecto.monto_total; // Monto total con modificaciones hasta la rescision de contrato.
    }
    this.proyecto.monto_total = null;
    this.DataService.get(`items/proyectos/${this.$stateParams.codigo}`).then( response => {
      if (response) {
        this.proyecto.items = response.datos.rows;
        this.copiarItems();
        this.calcularTotal();
      }
    });
  }

  copiarItems() {
    this.cItems = angular.copy(this.proyecto.items);
  }

  registrarPrecioItem(item) {
    if (!this.esPrecioActualizado(item) || angular.isUndefined(item.precio_unitario)) {
      return;
    }
    this.DataService.put(`items/${item.id_item}`, {precio_unitario: item.precio_unitario})
    .then( result => {
      if (result) {
        this.actualizarCopiaItems(result.datos);
        this.calcularTotal();
      }
    });
  }

  esPrecioActualizado(item) {
    const cItem = this.cItems[this._.findIndex(this.cItems, {id_item: item.id_item})];
    return cItem.precio_unitario !== item.precio_unitario;
  }

  actualizarCopiaItems(item) {
    this.cItems[this._.findIndex(this.cItems, {id_item: item.id_item})] = item;
  }

  calcularTotal() {
    this.precioTotal = 0;
    this.proyecto.items.map(item => {
      if (item.cantidad && item.precio_unitario) {
        this.precioTotal = this.Util.sumar([this.precioTotal, this.Util.multiplicar([item.cantidad, item.precio_unitario])])
      }
    });
    this.precioTotal = this.Util.redondear(this.precioTotal);
    if (this.esPresupuestoValido()) {
      this.proyecto.monto_total = this.precioTotal;
    } else {
      this.proyecto.monto_total = null;
      if(this.proyecto.monto_total_rest) {
        this.Message.error('El precio total no debe ser mayor al monto total restante del proyecto.');
      } else {
        this.Message.error('El precio total no debe ser mayor al monto total del convenio del proyecto.');
      }
    }
  }

  esPresupuestoValido() {
    if(this.proyecto.monto_total_rest) {
      return this.precioTotal <= (this.proyecto.monto_total_actual-this.proyecto.monto_total_rest);
    }
    return this.precioTotal <= this.proyecto.monto_total_convenio;
  }

}

export default PresupuestoItemsController;
