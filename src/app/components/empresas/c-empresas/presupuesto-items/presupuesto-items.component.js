'use strict';

import controller from './presupuesto-items.controller';
import template from './presupuesto-items.html';
import './presupuesto-items.scss';

const PresupuestoItemsComponent = {
  bindings: {
    proyecto: '='
  },
  controller,
  template
};

export default PresupuestoItemsComponent;
