'usr strict';

import controller from './resumen.controller';
import template from './resumen.html';

const ResumenComputosComponent = {
  bindings: {
    item: '<',
    resumen: '<'
  },
  controller,
  template
};

export default ResumenComputosComponent;
