'use strict';

class FotosComputosController {
  constructor(DataService, Modal, $scope) {
    'ngInject';

    this.DataService = DataService;
    this.Modal = Modal;
    this.$scope = $scope;
  }

  $onInit() {
    this.initConfiguracion();
    this.$scope.$on('recargarFotos', () => {
      this.initConfiguracion();
    });
  }

  initConfiguracion() {
    this.fotos = {
      backups: {},
      uploadCallback: (file) => {
        var data = angular.copy(file);
        data.fid_supervision = this.supervision.id_supervision;
        data.fid_item = this.item.id_item;
        data.tipo = 'TF_EMPRESA';
        data.path_fotografia = data.name + '_' + data.nombre.replace(/\s/,'_');
        if (this.fotos.backups[file.name]) {
          data.id_foto_supervision = this.fotos.backups[file.name].id_foto_supervision;
          data.id = data.id_foto_supervision;
        }

        this.DataService.save(`fotos`, data).then(response => {
          this.fotos[file.name] = {
            id_foto_supervision: response.datos.id_foto_supervision,
            base64: data.base64,
            nombre: data.nombre.replace(/\s/,'_'),
            show: true,
            showImage: true
          }
          //guardamos backup para recuperar el id_foto_supervision
          this.fotos.backups[file.name] = angular.copy(this.fotos[file.name]);
        })
      }
    };
  }

  showImage(key) {
    var foto = this.fotos[key];
    if (foto.base64) {
      foto.showImage = !foto.showImage;
    } else {
      this.DataService.get(`fotos/${foto.id_foto_supervision}`).then(response => {
        foto.base64 = response.datos;
        // TODO El backend debe enviar el tipo de archivo recuperado
        this.urlDownloadBase64 = foto.base64.indexOf('base64')>0? foto.base64: 'data:application/pdf;base64,'+foto.base64;
        foto.showImage = true;
        foto.show = true;
      })
    }
  }

  changeImage(key) {
    var foto = this.fotos[key];
    if (foto.show) {
      this.Modal.confirm('¿Está seguro/a de cambiar la imagen?', () => {
        foto.show = false;
      }, null, null, 'Sí, estoy seguro/a', 'Cancelar');
    } else {
      foto.show = true;
    }
  }

  vistaPrevia() {
    if (this.fotos['plano'].base64) {
      this.Modal.showDocumentBase64(this.fotos['plano'].base64, this.label, 'strBase64');
    }
  }

  vistaImagen(imgData) {
    if (imgData.base64) {
      this.Modal.showImageBase64(imgData, null);
    }
  }
}

export default FotosComputosController;
