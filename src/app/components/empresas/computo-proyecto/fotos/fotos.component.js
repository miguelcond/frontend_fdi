'usr strict';

import controller from './fotos.controller';
import template from './fotos.html';

const FotosComputosComponent = {
  bindings: {
    item: '<',
    supervision: '<',
    fotos: '=',
    modoVista: '<?',
    archivado: '<'
  },
  controller,
  template
};

export default FotosComputosComponent;
