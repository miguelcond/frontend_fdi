'use strict';

import DocumentacionProyectoComponent from './documentacion-proyecto.component';

const DocumentacionProyecto = angular
    .module('app.documentacionProyecto', [])
    .component('documentacionProyecto', DocumentacionProyectoComponent)
    .config(($stateProvider) => {
      $stateProvider
          .state('documentacion-proyecto', {
            url: '/documentacion-proyecto/{codigo:int}',
            component: 'documentacionProyecto'
          });
    })
    .name;

export default DocumentacionProyecto;
