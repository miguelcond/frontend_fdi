'use strict';

class ObservacionController {
    constructor($uibModalInstance, data) {
        'ngInject';
        
        this.$uibModalInstance = $uibModalInstance;
        this.proyecto = data;
    }
    $onInit() {

    }

    cerrar() {
      this.$uibModalInstance.dismiss('cancel');
    }

    aceptar() {
      this.$uibModalInstance.close(this.proyecto);
    }

}

export default ObservacionController;
