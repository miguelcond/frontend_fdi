'use strict';

import controller from './documentacion-proyecto.controller';
import template from './documentacion-proyecto.html';

const DocumentacionProyectoComponent = {
  bindings:{},
  controller,
  template
};

export default DocumentacionProyectoComponent;
