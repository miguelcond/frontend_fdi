'use strict';

class UbicacionModalController {
  constructor($uibModalInstance, $stateParams, data) {
    'ngInject';

    this.$uibModalInstance = $uibModalInstance;
    this.$stateParams = $stateParams;
    this.foto = data.foto;
    this.fotos = data.fotos;
  }

  $onInit() {
    this.mapa = {
      marcadores: [],
    };
    if (this.foto && this.foto.coordenadas && this.foto.coordenadas.coordinates && this.foto.coordenadas.coordinates[0] && this.foto.coordenadas.coordinates[1]) {
      this.mapa.centro = {
        lat: this.foto.coordenadas.coordinates[1],
        lng: this.foto.coordenadas.coordinates[0],
        zoom: 15,
      };
    } else {
      this.mapa.centro = {
        lat: -16.5286043,
        lng: -68.1798767,
        zoom: 15,
      };
    }

    let icon = {
      iconUrl: 'images/marker-sm-green.png',
      iconSize: [38, 40]
    };
    let icon2 = {
      iconUrl: 'images/marker-sm-red.png',
      iconSize: [38, 40]
    };
    for (let i in this.fotos) {
      if (this.fotos[i].coordenadas && this.fotos[i].coordenadas.coordinates && this.fotos[i].coordenadas.coordinates[0] && this.fotos[i].coordenadas.coordinates[1]) {
        this.mapa.marcadores.push({
          lat: this.fotos[i].coordenadas.coordinates[1],
          lng: this.fotos[i].coordenadas.coordinates[0],
          focus: (this.fotos[i].nro==this.foto.nro),
          draggable: false,
          message: 'Foto '+this.fotos[i].nro,
          icon: (this.fotos[i].nro==this.foto.nro)? icon: icon2,
        });
      }
    }
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default UbicacionModalController;
