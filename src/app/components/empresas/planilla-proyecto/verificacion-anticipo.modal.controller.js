'use strict';

class VerificacionAnticipoModalController {
  constructor($uibModalInstance, $scope, DataService, Message, data, _, Util, Supervision, Big, decimales) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$scope = $scope;
    this.DataService = DataService;
    this.Message = Message;
    this._ = _;
    this.Util = Util;
    this.Supervision = Supervision;
    this.Big = Big;
    this.decimales = decimales;
    this.supervision = data.supervision;
    this.deudaAnticipo = data.deudaAnticipo;
    this.totalDescuentos = data.totalDescuentos;
    this.esPlanillaCierre = data.esPlanillaCierre;
  }

  $onInit() {
    this.tiposDescuento = [
      {codigo: 'TOTAL', nombre: `Por la totalidad del anticipo ${this.supervision.supervisiones_anteriores.length > 0 ? 'pendiente' : ''} (*)`},
      {codigo: 'PROGRESIVO', nombre: 'Progresivo (**)'},
    ];
    this.verificarSiPuedeDescuentoTotal();
    this.verSiPuedeDescuentoProgresivo();
    if (this.esPlanillaCierre) {
      this.calcularDescuento('TOTAL');
    }
  }

  verificarSiPuedeDescuentoTotal() {
    if (this.supervision.monto_total_actual_ejecutado < this.deudaAnticipo) {
      this.puedeDescuentoTotal = false;
      this.supervision.tipo_descuento_anticipo = 'PROGRESIVO';
      this.calcularDescuento('PROGRESIVO');
    } else {
      this.supervision.tipo_descuento_anticipo = null;
      this.puedeDescuentoTotal = true;
    }
  }

  verSiPuedeDescuentoProgresivo() {
    this.puedeDescuentoProgresivo = this.esValidoDescuentoProgresivo();
    if (!this.puedeDescuentoProgresivo) {
      this.supervision.tipo_descuento_anticipo = 'TOTAL';
      this.calcularDescuento('TOTAL');
    }
  }

  esValidoDescuentoProgresivo() {
    this.calcularDescuento('PROGRESIVO');
    if (this.desembolso.descuento > this.deudaAnticipo || this.esPlanillaCierre) {
      return false;
    }
    this.desembolso = null;
    return true;
  }

  calcularDescuento(tipo) {
    this.desembolso = this.Supervision.calcularDescuento(tipo, this.supervision.monto_total_actual_ejecutado, this.deudaAnticipo, this.supervision.proyecto.porcentaje_anticipo);
  }

  getPorcentaje(numero, porcentaje) {
    //return this.Util.redondear(this.Util.redondear(numero * porcentaje)/100);
    return parseFloat(this.Big(numero).mul(porcentaje).div(100).toFixed(this.decimales||2));
  }

  cerrar() {
    this.$uibModalInstance.dismiss('cancel');
  }

  registrar() {
    this.$uibModalInstance.close(this.supervision);
  }

  tieneComputosMetricos() {
    return !this._.isEmpty(JSON.parse(angular.toJson(this.computos))[0]);
  }
}

export default VerificacionAnticipoModalController;
