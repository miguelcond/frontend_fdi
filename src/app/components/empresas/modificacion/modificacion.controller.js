'use strict';

import modalFechaModificacionTemplate from './modal/fechamodificacion.html';
import modalFechaModificacionController from './modal/fechamodificacion.controller.js';

class ModificacionController {
  constructor($scope, $state, $stateParams, $q, $window, DataService, Modal, Storage, Util, Message, Modificacion) {
    'ngInject';
    this.$scope = $scope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$q = $q;
    this.$window = $window;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Storage = Storage;
    this.Util = Util;
    this.Message = Message;
    this.Modificacion = Modificacion;

    this.usuario = this.Storage.getUser();
    if(['SUPERVISOR','TECNICO_UPRE','RESP_DEPARTAMENTAL_UPRE','EMPRESA','ENCAR_REGIONAL_UPRE'].indexOf(this.usuario.rol.nombre)<0) {
      Message.warning('No tiene permisos suficientes para ver la modificación');
      this.$window.history.back();
    }
    this.filtroItems = 'TODOS';

    this.tipoModificacion = $stateParams.tipoModificacion;
    this.tabIndex = 1;
  }

  $onInit() {
    if(this.$stateParams.crear) {
      this.$stateParams.crear = false;
      this.crearModificacion();
    } else {
      this.obtenerModificacion();
    }

    // TODO verificar la URL con el backend
    this.url = `proyectos/${this.$stateParams.proyecto}`;
    this.mensajeConfirmacion = {
      "Finalizar": `¿Está seguro de finalizar y enviar las modificaciones?<br>
                    Se le informa que estas modificaciones, una vez aprobadas serán incorporadas a la planilla de avance.`,
      "Cancelar": `¿Está seguro/a de cancelar la modificación?<br>La cancelación revertirá los cambios realizados en la presente modificación.`,
      "Guardar": `Se guardarán los cambios realizados.`
    };

    this.modificandoPlazo = this.modificandoItems = this.modoVista = !this.$stateParams.edicion;

    this.$scope.$on('transicion_accion_ejecutada', () => {
      this.$state.go('mis-proyectos');
    });

    /* DOCUMENTO */
    this.uploadCallback = (file)=>{
      var data = {
        "doc_respaldo_modificacion": angular.copy(file)
      }
      this.DataService.put(`proyectos/${this.$stateParams.proyecto}`, data).then(response => {
        if (response && response.finalizado) {
          this.informe_tecnico_registrado = true;
          //this.asignarDatos(file, response.datos.documento);
        }
      });
    }

  }

  $onChanges() {
    this.$scope.$watch('$ctrl.filtroItems', () => {
      if (!this.proyecto) {
        return;
      }
      this.mostrarItems();
    });
  }

  cancelar() {
    this.Modal.confirm(`¿Está seguro/a de cancelar la modificación?<br>La cancelación revertirá los cambios realizados en la presente modificación.`,
      ()=>{

      },null,'Cancelación de modificación','Sí, estoy seguro/a');
  }

  finalizar() {
    this.Modal.confirm(`¿Está seguro de finalizar y enviar las modificaciones?<br>
      Se le informa que estas modificaciones de fecha __/__/__ una vez aprobadas serán incorporadas a la planilla de avance.`,
      ()=>{

      },null,'Finalizar Modificación','Sí, estoy seguro/a');
  }

  crearModificacion() {
    let tipo = this.tipoModificacion===3?'TM_CONTRATO_MODIFICATORIO':this.tipoModificacion===2?'TM_ORDEN_CAMBIO':this.tipoModificacion===1?'TM_ORDEN_TRABAJO':'TM';
    this.DataService.put(`proyectos/${this.$stateParams.proyecto}/modificaciones?tipo=${tipo}`).then((result)=>{
      if (result && result.finalizado) {
        //this.proyecto = result.datos;
        //this.agregarCamposModificar(result.datos);
        this.$state.go('modificacion',{
          proyecto: this.$stateParams.proyecto,
          version: result.datos.version,
          tipoModificacion: tipo,
          edicion: true,
          crear: false
        });
      } else {
        //this.$state.go('mis-proyectos',null);
        this.$window.history.back();
      }
    });
  }

  obtenerModificacion() {
    // TODO enviar rol si el supervisor tiene mas de un rol
    let query = {
      rol:  this.Storage.get('rol').id_rol,
    };
    if(this.$stateParams.version) {
      query.version=this.$stateParams.version;
    }
    let url = `proyectos/${this.$stateParams.proyecto}/modificaciones?${this.Util.serialize(query)}`;
    this.DataService.get(url).then((result)=>{
      if (result && result.finalizado) {
        if(!result.datos.modificaciones || !result.datos.modificaciones.nombre) {
          if(this.tipoModificacion!=0) this.$state.go('mis-proyectos',null);
        }
        //this.proyecto = result.datos;
        this.agregarCamposModificar(result.datos);
        this.proyecto.idProyecto = this.$stateParams.proyecto;
        if(this.proyecto.doc_respaldo_modificacion) {
          let a = this.proyecto.doc_respaldo_modificacion.indexOf('[')+1;
          let b = this.proyecto.doc_respaldo_modificacion.indexOf(']');
          let codigo = this.proyecto.doc_respaldo_modificacion.substring(a,b);
          this.DataService.get(`proyectos/${this.$stateParams.proyecto}/doc/${codigo}`).then((doc)=>{
            if(doc && doc.finalizado) {
              this.informe_tecnico = doc.datos;
              this.informe_tecnico_registrado = true;
            }
          });
        }
        if(!this.$stateParams.edicion && this.proyecto.estado_actual) {
          this.$stateParams.edicion = this.proyecto.estado_actual.codigo==='MODIFICADO' && this.usuario.rol.nombre==='SUPERVISOR';
          this.modoVista = !this.$stateParams.edicion;
        }
      } else {
        this.$state.go('mis-proyectos',null);
      }
    });
  }

  agregarCamposModificar(proyecto) {
    if(proyecto) {
      this.tipoModificacion = proyecto.tipo_modificacion=='TM_ORDEN_TRABAJO'?1:proyecto.tipo_modificacion=='TM_ORDEN_CAMBIO'?2:proyecto.tipo_modificacion=='TM_CONTRATO_MODIFICATORIO'?3:0;
      if(this.tipoModificacion==0) {
        //this.Message.warning('No se ha especificado un tipo de modificación válido');
        //this.irAtras();
        //this.$state.go('pendientes',null);
      }
      // Historial
      proyecto.modificaciones.total_variacion = 0;
      proyecto.modificaciones.total_variacion_porcentaje = 0;
      for(let i in proyecto.modificaciones.historial) {
        let version = proyecto.modificaciones.historial[i];
        proyecto.modificaciones.total_variacion = version.variacion+proyecto.modificaciones.total_variacion;
        version.variacion_porcentaje = version.variacion*100/proyecto.version_inicial.monto_total;
        proyecto.modificaciones.total_variacion_porcentaje += version.variacion_porcentaje;
      }
      // Modulos
      this.nuevosmodulos = [];
      this.cabecera = angular.copy(proyecto.modulos[0].items[0]);
      if(!proyecto.version_anterior || !proyecto.version_anterior.tipo_modificacion_proyecto) {
        this.cabecera.version_anterior = null;
      }
      //TODO verificar backend
      if(proyecto.version==2 && !this.cabecera.version_inicial) {
        this.cabecera.version_anterior = {};
      }
      if(proyecto.version==1) {
        proyecto.modificaciones.nombre = "Contrato Original";
      }
      let modulos = [];
      for(let i in proyecto.modulos) {
        if(proyecto.modulos[i].nombre.indexOf(proyecto.modificaciones.nombre)>=0) {
          this.nuevosmodulos.push(proyecto.modulos[i]);
          this.modificandoItems = true;
        } else {
          let modulo = proyecto.modulos[i];
          modulo.isCollapsed = true && !this.modoVista;
          for(let j in modulo.items) {
            if(modulo.items[j].version_anterior && modulo.items[j].cantidad != modulo.items[j].version_anterior.cantidad) {
              this.modificandoItems = true;
              modulo.isCollapsed = false;
            }
            // Verificar avance
            if(!modulo.items[j].avance_actual) {
              modulo.items[j].avance_actual = 0;
            }
            if(modulo.items[j].estado === 'CERRADO') {
              modulo.items[j].avance_actual = modulo.items[j].cantidad;
            }
            // Verificar cabeceras
            if(!modulo.items[j].version_inicial && this.cabecera.version_inicial) {
              modulo.items[j].version_inicial = {};
            }
            modulo.items[j].show = true;
            // Para verificar si se modifico el valor
            modulo.items[j].cantidad_ant = modulo.items[j].cantidad;
          }
          modulos.push(modulo);
        }
      }
      proyecto.modulos = modulos;
      this.proyecto = proyecto;
      this.modificandoPlazo = this.proyecto.plazo_ampliacion && (this.proyecto.plazo_ampliacion.por_volumenes || this.proyecto.plazo_ampliacion.por_compensacion);
      this.recalcularMontos();
      this.recalcularFechas();
      // Valores para verificar se se han cambiado los datos.
      this.proyecto.plazo_ampliacion_ant = {};
    }
  }

  // TODO obtener valores desde el backend
  calcularFechaAmpliada(fecha) {
    if(fecha && this.proyecto && this.proyecto.plazo_ampliacion) {
      fecha = new Date(fecha);
      fecha.setDate(fecha.getDate()+this.proyecto.plazo_ampliacion.por_volumenes+this.proyecto.plazo_ampliacion.por_compensacion);
    }
    return fecha;
  }

  /* MODIFICAR ITEMS */
  modificarCantidadItem(item) {
    if(item && item.cantidad_ant!=item.cantidad) {
      this.DataService.put(`items/${item.id_item}`,{cantidad:item.cantidad}).then((result)=>{
        if(result && result.finalizado) {
          this.Message.success(result.mensaje);
          item.error = false;
        } else {
          item.error = true;
        }
        item.cantidad_ant= item.cantidad;
      });
      this.recalcularMontos();
    }
  }

  recalcularMontos(item) {
    // TODO para optimizar solo sumar o restar el item modificado. (probar con item eliminado)
    item;
    //Recalcular monto total
    this.cantItems = 0;
    this.proyecto.monto_total_variacion_modificado = 0;
    for(let i in this.proyecto.modulos) {
      for(let j in this.proyecto.modulos[i].items) {
        this.cantItems++;
        if(this.proyecto.modulos[i].items[j].version_anterior && this.proyecto.modulos[i].items[j].version_anterior.cantidad!=this.proyecto.modulos[i].items[j].cantidad) {
          this.proyecto.monto_total_variacion_modificado += this.Util.redondear(this.proyecto.modulos[i].items[j].precio_unitario * (this.proyecto.modulos[i].items[j].cantidad-this.proyecto.modulos[i].items[j].version_anterior.cantidad));
        }
      }
    }
    for(let i in this.nuevosmodulos) {
      for(let j in this.nuevosmodulos[i].items) {
        this.nuevosmodulos[i].items[j].nro_item = ++this.cantItems;
        if(this.nuevosmodulos[i].items[j].precio_unitario && this.nuevosmodulos[i].items[j].cantidad) {
          this.proyecto.monto_total_variacion_modificado += this.Util.redondear(this.nuevosmodulos[i].items[j].precio_unitario * this.nuevosmodulos[i].items[j].cantidad);
        }
      }
    }
    this.proyecto.porcentaje_variacion = this.proyecto.monto_total_variacion_modificado*100/this.proyecto.version_inicial.monto_total;
    this.porcentajeLimite = this.tipoModificacion===3?0.1:this.tipoModificacion===2?0.05:0;
    // TODO tambien debe sumarse con las anteriores modificaciones.
    let monto_variacion = this.Util.redondear(this.proyecto.monto_total_variacion_modificado+this.proyecto.modificaciones.total_variacion);
    let monto_limite = this.Util.redondear(this.proyecto.version_inicial.monto_total*this.porcentajeLimite);
    this.porcentajeExcedido = (monto_variacion)>(monto_limite) ||
                              -(monto_variacion)>(monto_limite);
  }

  adicionarModulo() {
    if(!this.nuevosmodulos || !this.nuevosmodulos[0]) {
      let datos = {
        fid_proyecto: this.$stateParams.proyecto,
        nombre: `ITEMS NUEVOS (${this.proyecto.modificaciones.nombre})`
      };
      this.DataService.post(`modulos`, datos).then( (result) => {
        if (result && result.finalizado) {
          if(!this.nuevosmodulos) {
            this.nuevosmodulos = [];
          }
          this.Message.success(result.mensaje);
            if(!result.datos.items) {
              result.datos.items = [];
            }
            if(result.datos.items.length<=0) {
              result.datos.items.push({edicion: true});
            }
          this.nuevosmodulos.push(result.datos);
        }
      });
    }
  }

  modificarItems() {
    this.modificandoItems = !this.modificandoItems;
  }

  /* MODIFICACION DEL PLAZO */
  modificarPlazoAmpliacion(plazo_ampliacion) {
    if(plazo_ampliacion) {
      plazo_ampliacion.por_compensacion = plazo_ampliacion.por_compensacion || 0;
      plazo_ampliacion.por_volumenes = plazo_ampliacion.por_volumenes || 0;
      let datos = {
        plazo_ampliacion: {
          por_compensacion: parseInt(plazo_ampliacion.por_compensacion || 0),
          por_volumenes: parseInt(plazo_ampliacion.por_volumenes || 0)
        }
      }
      if( (this.proyecto.plazo_ampliacion_ant.por_volumenes!=plazo_ampliacion.por_volumenes ||
          this.proyecto.plazo_ampliacion_ant.por_compensacion!=plazo_ampliacion.por_compensacion) ) {
        delete this.proyecto.nueva_fecha_provisional;
        delete this.proyecto.nueva_fecha_definitiva;
        this.DataService.put(`proyectos/${this.$stateParams.proyecto}`, datos).then((result)=>{
          if(result && result.finalizado) {
            this.Message.success(result.mensaje);
            this.proyecto.plazo_ampliacion_ant = angular.copy(plazo_ampliacion);
            this.recalcularFechas();
          }
        });
      }
    }
  }

  recalcularFechas() {
    let plazo_ampliacion = this.proyecto.plazo_ampliacion;
    if(plazo_ampliacion && (parseInt(plazo_ampliacion.por_compensacion) || parseInt(plazo_ampliacion.por_volumenes)) ) {
      this.proyecto.nueva_fecha_provisional = new Date(this.proyecto.orden_proceder);
      this.proyecto.nueva_fecha_provisional.setDate(this.proyecto.nueva_fecha_provisional.getDate() +parseInt(plazo_ampliacion.por_volumenes) +parseInt(plazo_ampliacion.por_compensacion) -1 +this.proyecto.plazo_ejecucion);
      this.proyecto.nueva_fecha_definitiva = new Date(this.proyecto.nueva_fecha_provisional);
      this.proyecto.nueva_fecha_definitiva.setDate(this.proyecto.nueva_fecha_provisional.getDate() +90);
    } else {
      delete this.proyecto.nueva_fecha_provisional;
      delete this.proyecto.nueva_fecha_definitiva;
    }
  }

  fechaSumDias(fecha, dias) {
    let fechaNueva = new Date(fecha);
    if(!isNaN(fechaNueva.getTime()) && !isNaN(parseInt(dias))) {
      fechaNueva.setDate(fechaNueva.getDate() + parseInt(dias));
      return fechaNueva;
    }
    return fecha;
  }

  modificarPlazo() {
    this.modificandoPlazo = !this.modificandoPlazo;
  }

  /* INFOMRE TECNICO*/
  verInformeTecnico() {
    this.Modal.showDocumentBase64(this.informe_tecnico.base64, 'Informe', 'strBase64');
  }

  quitarInformeTecnico() {
    this.informe_tecnico_registrado = false;
    delete this.informe_tecnico;
  }

  /* FUNCIONES UTILES */
  mostrarItems() {
    switch (this.filtroItems) {
      case 'COMPLETADOS':
        this.filtrarItems((item) => {
          return item.avance_actual === item.cantidad;
        });
        break;
      case 'INCOMPLETOS':
        this.filtrarItems((item) => {
          return item.avance_actual !== item.cantidad;
        });
        break;
      default:
        this.filtrarItems();
    }
  }

  getMgColor(item) {
    if (item.avance_actual === item.cantidad || item.estado === 'CERRADO') {
      return  'bl-success';
    }
    if (item.avance_actual === 0) {
      return  'bl-primary';
    }
    if (item.avance_actual > 0) {
      return  'bl-info';
    }
    return  'bl-warning';
  }

  getTitle(item) {
    if (item.avance_actual === item.cantidad || item.estado === 'CERRADO') {
      return  'Completado';
    }
    if (item.avance_actual ===  0) {
      return  'Sin avance';
    }
    if (item.avance_actual > 0) {
      return  'Con avance';
    }
    return  'Con avance';
  }

  filtrarItems(condicion = () => { return true }) {
    this.proyecto.modulos.forEach( modulo => {
      modulo.items.forEach( item => {
        item.show = condicion(item);
      });
      //modulo.show = this._.some(modulo.items, { show: true });
    });
  }

  irAtras() {
    window.history.back();
  }

  verificarInformeTecnico() {
    if(!this.informe_tecnico_registrado) {
      this.Message.warning(`Debe cargar el documento con la aprobación de la modificación.`);
    }
    return this.informe_tecnico_registrado;
  }

  verificarNuevosItems() {
    // Verificar documentos adjuntos
    if(this.nuevosmodulos && this.nuevosmodulos[0]) {
      for(let i in this.nuevosmodulos[0].items) {
        let item = this.nuevosmodulos[0].items[i];
        if(item.id_item && !item.especificaciones) {
          this.Message.warning(`Debe cargar el documento de especificaciones técnicas del item<br>Nro. ${item.nro_item} - <strong>${item.nombre}</strong>`);
          return false;
        }
        if(!this.nuevosmodulos[0].items[i].analisis_precios) {
          this.Message.warning(`Debe cargar el documento de análisis de precios unitarios del item<br>Nro. ${item.nro_item} - <strong>${item.nombre}</strong>`);
          return false;
        }
      }
    }
    return true;
  }

  /* TRANSICION */
  verificarModificacion(accion) {
    let defer = this.$q.defer();
    // TODO Se debe verificar segun tipo modificacion
    // ---
    // Registrar la fecha de modificacion
    if(accion.label==='Cancelar') {
      this.proyecto.fecha_modificacion = new Date();
      defer.resolve({confirmar: true});
    } else {
      // verificar campos requeridos
      if(!this.verificarInformeTecnico()) {
        defer.reject();
        return defer.promise;
      }
      if(!this.verificarNuevosItems()) {
        defer.reject();
        return defer.promise;
      }
      // Modal para registrar fecha
      const modalFechaModificacion =  this.Modal.show({
        template: modalFechaModificacionTemplate,
        controller: modalFechaModificacionController,
        data: this.proyecto,
        size: 'md'
      });
      modalFechaModificacion.result.then( (proyecto) => {
        this.proyecto = proyecto;
        defer.resolve();
      }, () => { });
    }
    //defer.resolve({confirmar: true});
    return defer.promise;
  }

}

export default ModificacionController;
