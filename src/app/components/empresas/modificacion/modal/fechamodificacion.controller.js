'use strict';

class FechaModificacionController {
    constructor($uibModalInstance, Storage, data) {
        'ngInject';

        this.$uibModalInstance = $uibModalInstance;
        this.proyecto = data;
        this.user = Storage.getUser();
    }
    $onInit() {
      let isuper = this.user.rol.nombre==='SUPERVISOR';
      this.option = {
        confirm_txt: isuper? '¿Está seguro/a de finalizar y enviar las modificaciones?': '¿Está seguro/a de aprobar las modificaciones?',
        action: isuper? 'Registrar': 'Revisar',
        disabled: !isuper
      };
      this.validacion = {
        fecha_minima: new Date(this.proyecto.orden_proceder)
      };
      this.validacion.fecha_minima.setDate(this.validacion.fecha_minima.getDate()+1);
    }

    cerrar() {
      this.$uibModalInstance.dismiss('cancel');
    }

    aceptar() {
      this.$uibModalInstance.close(this.proyecto);
    }
}

export default FechaModificacionController;
