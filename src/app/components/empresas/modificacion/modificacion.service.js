'use strict';

class ModificacionService {
  constructor(Util, _) {
    'ngInject';

    this.Util = Util;
    this._ = _;
  }

  getDatosSegunEstado(modificacion) {
    let nSupervision = {};
    const atributos = Array.isArray(modificacion.estado_actual.atributos) ? modificacion.estado_actual.atributos : this._.keys(modificacion.estado_actual.atributos);
    atributos.map(atributo => {
      if (!this.Util.isEmpty(modificacion[atributo])) {
        nSupervision[atributo] = modificacion[atributo];
      }
    });
    return nSupervision;
  }
}

export default ModificacionService;
