'use strict';

class ItemsNoCerradosModalController {
  constructor($uibModalInstance, $scope, DataService, Message, data, _) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$scope = $scope;
    this.DataService = DataService;
    this.Message = Message;
    this.data = data;
    this._ = _;
    this.items = data.items;
  }

  $onInit() {

  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

  aprobar() {
    this.$uibModalInstance.close(true);
  }

}

export default ItemsNoCerradosModalController;
