'use strict';

import planillaProyectoComponent from './planilla-proyecto.component';
import fotosSupervision from './fotos-supervision/fotos-supervision.component';
import empresaAnterior from './empresa-anterior/empresa-anterior.component';

const planillaProyecto = angular
    .module('app.planillaProyecto', [])
    .component('planillaProyecto', planillaProyectoComponent)
    .component('fotosSupervision', fotosSupervision)
    .component('empresaAnterior', empresaAnterior)
    .config(($stateProvider) => {
      $stateProvider
          .state('planilla-proyecto', {
            url: '/planilla-proyecto/:idSupervision',
            component: 'planillaProyecto'
          });
    })
    .name;

export default planillaProyecto;
