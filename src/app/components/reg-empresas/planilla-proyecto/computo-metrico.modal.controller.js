'use strict';

class ComputoMetricosModalController {
  constructor($uibModalInstance, $scope, DataService, Message, data, _) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    this.$scope = $scope;
    this.DataService = DataService;
    this.Message = Message;
    this.data = data;
    this._ = _;
    this.supervision = data.supervision;
    this.item = data.item;
    this.modoVista = data.modoVista;
    this.existeCambios = false;
  }

  $onInit() {

  }

  cerrar() {
    if (this.modificados.length > 0) {
      this.$uibModalInstance.close(this.modificados);
    } else {
      this.$uibModalInstance.dismiss('cancel');
    }
  }

  guardar() {
    if (this.existeCambiosItem) {
      this.$scope.$broadcast('guardarComputosMetricos', () => {
        this.$uibModalInstance.close(this.modificados);
      });
    } else if (this.modificados.length > 0) {
      this.$uibModalInstance.close(this.modificados);
    } else {
      this.$uibModalInstance.close(null);
    }
  }

  tieneComputosMetricos() {
    return !this._.isEmpty(JSON.parse(angular.toJson(this.computos))[0]);
  }
}

export default ComputoMetricosModalController;
