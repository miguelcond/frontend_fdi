'use strict';

import controller from './empresa-anterior.controller';
import template from './empresa-anterior.html';

const EmpresaAnteriorComponent = {
  bindings: {
    supervision: '<',
  },
  controller,
  template
};

export default EmpresaAnteriorComponent;
