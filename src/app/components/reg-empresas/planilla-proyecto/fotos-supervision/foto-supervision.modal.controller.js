'use strict';

class FotoSupervisionController {
  constructor($uibModalInstance ,DataService, $stateParams, data, _) {
    'ngInject';

    this.DataService = DataService;
    this.$stateParams = $stateParams;
    this.data = data;
    this.$uibModalInstance = $uibModalInstance;
    this._ = _;
    this.foto = data.foto;
    this.fotos = data.fotos;
  }

  $onInit() {

  }

  irAPosicion(tipo) {
    const POSICION = tipo === 'anterior' ? this.foto.nro - 1 : tipo === 'siguiente' ? this.foto.nro + 1 : this.foto.nro;
    this.foto = this._.find(this.fotos, {nro: POSICION});
  }

  cerrar() {
    this.$uibModalInstance.close(false);
  }

}

export default FotoSupervisionController;
