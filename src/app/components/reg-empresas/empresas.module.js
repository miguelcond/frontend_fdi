'use strict';

// import Matriculas from './matriculas/matriculas.module';
// import DocumentacionProyecto from './documentacion-proyecto/documentacion-proyecto.module';
import AvanceProyecto from './avance-proyecto/avance-proyecto.module';
import PlanillaProyecto from './planilla-proyecto/planilla-proyecto.module';
import ComputoProyecto from './computo-proyecto/computo-proyecto.module';
import SupervisionService from './supervision.service';
// import CEmpresas from './c-empresas/c-empresas.module';
import Modificacion from './modificacion/modificacion.module';

const Empresas = angular
    .module('app.rempresas', [
        // Matriculas,
        // DocumentacionProyecto,
        AvanceProyecto,
        PlanillaProyecto,
        ComputoProyecto,
        // CEmpresas,
        Modificacion
    ])
    .service('Supervision', SupervisionService)
    .name;

export default Empresas;
