'usr strict';

import controller from './computo-proyecto.controller';
import template from './computo-proyecto.html';
import './computo-proyecto.scss';

const ComputoProyectoComponent = {
  bindings: {
    supervision: '<',
    item: '<',
    computos: '=',
    modoVista: '<?',
    modificados: '=',
    existeCambios: '='
  },
  controller,
  template
};

export default ComputoProyectoComponent;
