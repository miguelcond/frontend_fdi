'usr strict';

import controller from './computos-metricos.controller';
import template from './computos-metricos.html';

const ComputoProyectoComponent = {
  bindings: {
    supervision: '=',
    item: '=',
    computos: '=',
    copiaComputos: '=',
    resumen: '=',
    fotos: '=',
    modoVista: '<?',
    existeCambios: '=',
    modificados: '='
  },
  controller,
  template
};

export default ComputoProyectoComponent;
