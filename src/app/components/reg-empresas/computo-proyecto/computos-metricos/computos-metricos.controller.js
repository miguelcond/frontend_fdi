/*eslint no-console: ["error", { allow: ["log", "warn", "error"] }] */
'use strict';

class ComputosMetricosController {
  constructor($scope, DataService, Modal, Message, Computo, Util, Loading, _, apiUrl) {
    'ngInject';

    this.$scope = $scope;
    this.DataService = DataService;
    this.Modal = Modal;
    this.Message = Message;
    this.Computo = Computo;
    this.Util = Util;
    this.Loading = Loading;
    this._ = _;
    this.apiUrl = apiUrl;
    this.copiaComputos = {};
  }

  $onInit() {
    this.descargaEsp =  {
      url: `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/ET_${this.item.id_item}/descargar`,
      nombre: `Especificaciones item ${this.item.nro_item}`
    };
    // `${this.apiUrl}proyectos/${this.supervision.fid_proyecto}/doc/ET/descargar`;
    this.$scope.$on('recargarDatosComputos', () => {
      this.recargarDatos();
    });
    this.$scope.$on('guardarComputosMetricos', (event, callback) => {
      this.saveAllRow(callback);
    });
  }

  recargarDatos() {
    this.sacarCopiaComputos();
    this.configSegunUnidadMedida();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.computos', () => {
      if (!this.computos) {
        return;
      }
      this.determinarCambiosEnComputos();
    }, true);
  }

  determinarCambiosEnComputos() {
    this.existeCambios = !angular.equals(JSON.parse(angular.toJson(this.computos)), this.copiaComputos);
    if (this.existeCambios) {
      this.actualizarItemsModificados()
    }
  }

  actualizarItemsModificados() {
    if (this.modificados.indexOf(this.item.id_item) === -1) {
      this.modificados.push(this.item.id_item);
    }
  }

  configSegunUnidadMedida() {
    this.conf = this.Computo.getConf(this.item.unidad_medida);
  }

  // esCantidadEntera() {
  //   const unidades_convencionales = ['UN_METRO_CUB', 'UN_METRO_CUAD', 'UN_METRO', 'UN_GLOBAL', 'UN_PIEZA', 'UN_PUNTO'];
  //   return unidades_convencionales.indexOf(this.item.unidad_medida) !== -1;
  // }

  changeComputo(computo) {
    this.Computo.setCantParcial(this.item.unidad_medida, computo);
    this.actualizarResumen();
  }

  uniformarArea(nuevo,computo) {
    if (this.item.unidad_medida === 'UN_METRO_CUAD') {
      if (nuevo === 'alto' && computo.alto) delete computo.ancho;
      if (nuevo === 'ancho' && computo.ancho) delete computo.alto;
    }
  }

  addRow() {
    // Avance de unidades Globales puede tener mas de una fila
    // if (this.item.unidad_medida === 'UN_GLOBAL' && this.computos.length == 1) {
    //   this.Message.warning('Solo puede tener un computo para la unidad de medida GLOBAL');
    //   return;
    // }
    var subtitulo = null;
    for (var i = this.computos.length - 1; i >= 0; i--) {
      if (this.computos[i].isSubtitulo) {
        subtitulo = this.computos[i].descripcion;
        break;
      }
    }
    this.computos.push({ subtitulo: subtitulo });
  }

  saveRow(computo, i) {
    if (!this.sonDatosValidos(i)) {
      return;
    }
    computo.id = computo.id_computo_metrico;
    computo.fid_item = this.item.id_item;
    computo.fid_supervision = this.supervision.id_supervision;
    computo.orden = i;
    computo = this.quitarCamposNulos(computo);
    if (!computo.subtitulo) { computo.subtitulo = null; }
    this.DataService.save(`computos_metricos/`, computo).then(response => {
      if (response && response.finalizado) {
        this.Message.success(response.mensaje);
        angular.extend(computo,  response.datos);
        if (this.item.unidad_medida === 'UN_METRO_CUB') {
          computo.area = response.datos.volumen;
        }
        this.sacarCopiaComputos(computo);
      }
    });
  }

  saveAllRow(callback) {
    if (!this.sonDatosValidos()) {
      return;
    }
    let datos = {
      computos: []
    };
    if (this.computos.length) {
      var fila = this.computos[0];
      var subtitulo = fila.isSubtitulo ? fila.descripcion : null;
      for (var i = 0; i < this.computos.length; i++) {
        if (this.computos[i].isSubtitulo) {
          subtitulo = this.computos[i].descripcion;
        } else {
          this.computos[i].subtitulo = subtitulo;
          this.computos[i].fid_item = this.item.id_item;
          this.computos[i].fid_supervision = this.supervision.id_supervision;
          this.computos[i].orden = i;
          datos.computos.push(this.quitarCamposNulos(this.computos[i]));
        }
      }

      this.DataService.put(`computos_metricos/items/${this.item.id_item}`, datos).then(response => {
        if (response && response.finalizado) {
          this.Message.success(response.mensaje);
          this.computos = this.Computo.getListaOrdenadaComputos(response.datos, this.item);
          this.sacarCopiaComputos();
          if (callback) {
            callback();
          }
        }
      });
    }
  }

  deleteRow(computo, index) {
    if (computo.id_computo_metrico) {
      this.Modal.confirm('¿Está seguro/a de eliminar el cómputo?', () => {
        this.DataService.delete(`computos_metricos/${computo.id_computo_metrico}`, computo).then(response => {
          if (response) {
            this.Message.success(response.mensaje);
            this.quitarComputoDeLista(index);
            this.actualizarResumen();
          }
        })
      }, null, null, 'Sí, estoy seguro/a', 'Cancelar')
    } else {
      this.quitarComputoDeLista(index);
      this.actualizarResumen();
    }
  }

  deleteAllRow() {
    if (!this.tieneDatos()) {
      this.Message.warning(`No tiene cómputos métricos para eliminar.`);
      return;
    }

    this.Modal.confirm('¿Está seguro/a de eliminar todos los cómputos de este item?', () => {
      let i = this.computos.length;
      while (i --) {
        if (!this.computos[i].id_computo_metrico) {
          this.quitarComputoDeLista(i)
        }
      }
      if (this.computos.length === 1 && this._.isEmpty(this.computos[0])) {
        this.actualizarResumen();
        return;
      }
      this.DataService.delete(`computos_metricos/${this.supervision.id_supervision}/todos/${this.item.id_item}`).then(response => {
        if (response) {
          this.quitarComputoDeLista(null, true);
          this.actualizarResumen();
          this.Message.success(response.mensaje);
        }
      });
    }, null, null, 'Sí, estoy seguro/a', 'Cancelar')
  }

  actualizarResumen() {
    this.resumen = this.Computo.getResumen(this.item, this.computos, this.resumen);
  }

  tieneDatos() {
    if (!this.computos || (this.computos.length === 1 && !this.computos[0].cantidad)) {
      return false;
    }
    return true;
  }

  sonDatosValidos(fila) {
    if(this.resumen.cant_faltante < 0) {
      this.Message.error('La cantidad faltante no puede ser negativa, reajuste los valores.');
      return false;
    }
    var form = this.$scope.form_computo;
    let sonCamposValidos;
    if (fila) {
      sonCamposValidos = this.verificarDatosComputo(form, fila);
    } else {
      for (var i = 0; i < this.computos.length; i++) {
        if (!this.computos[i].isSubtitulo) {
          sonCamposValidos = this.verificarDatosComputo(form, i);
        }
      }
    }
    if (!sonCamposValidos) {
      this.Message.warning('Verifique, corrija y/o llene los campos resaltados.');
      return false;
    }
    return true;
  }

  verificarDatosComputo(form, i) {
    let esValido = (
      form['descripcion_'+i].$valid &&
      form['cantidad_'+i].$valid &&
      form['factor_forma_'+i].$valid &&
      form['largo_'+i].$valid &&
      form['ancho_'+i].$valid &&
      form['alto_'+i].$valid &&
      form['area_'+i].$valid &&
      form['cantidad_parcial_'+i].$valid
    );
    if(!esValido) {
      form['descripcion_'+i].$setTouched();
      form['cantidad_'+i].$setTouched();
      form['factor_forma_'+i].$setTouched();
      form['largo_'+i].$setTouched();
      form['ancho_'+i].$setTouched();
      form['alto_'+i].$setTouched();
      form['area_'+i].$setTouched();
      form['cantidad_parcial_'+i].$setTouched();
      form['area_'+i].$setDirty();
      form['cantidad_parcial_'+i].$setDirty();
    }
    return esValido;
  }

  quitarCamposNulos(computo) {
    for (var k in computo) {
      if (computo[k] === null) {
        delete computo[k];
      }
    }
    return computo;
  }

  quitarComputoDeLista(posicion, todos) {
    if (this.computos.length === 1 || todos) {
      this.computos = [{}];
    } else {
      this.computos.splice(posicion, 1);
    }
    this.sacarCopiaComputos();
    this.actualizarItemsModificados();
  }

  addSubtitulo() {
    if (angular.isUndefined(this.index_subtitulo)) {
      this.Message.warning('Debe hacer click en una fila para añadir un subtítulo, y se añadirá sobre la misma.')
    } else {
      this.computos.splice(this.index_subtitulo, 0, {isSubtitulo:true});
    }
  }

  setIndexSubtitulo(index) {
    this.index_subtitulo = index;
  }

  moveSubtitulo(direccion, index) {
    var computo_auxiliar = angular.copy(this.computos[index]);
    if (direccion=='up' && index > 0 && !this.computos[index-1].isSubtitulo) {
      this.computos.splice(index, 1);
      this.computos.splice(index-1, 0, computo_auxiliar);
      this.computos[index].subtitulo = computo_auxiliar.descripcion;
    } else if (direccion=='down' && index+1 < this.computos.length && !this.computos[index+1].isSubtitulo) {
      this.computos.splice(index, 1);
      this.computos.splice(index+1, 0, computo_auxiliar);
      this.computos[index].subtitulo = null;
      for (var i = index; i >= 0 ; i--) {
        if (this.computos[i].isSubtitulo) {
          this.computos[index].subtitulo = this.computos[i].descripcion;
          break;
        }
      }
    }
  }

  saveSubtitulo(subtitulo, index) {
    var index_final = this.computos.length;
    for (var i = index+1; i < this.computos.length; i++) {
      if (this.computos[i].isSubtitulo) {
        index_final = i;
        break;
      } else {
        this.computos[i].subtitulo = subtitulo.descripcion;
      }
    }
    this.saveIniFinRow(index+1, index_final);
  }

   saveIniFinRow(inicio, fin, all) {
    if (!fin) {
      fin = this.computos.length;
    }
    for (var i = inicio; i < fin; i++) {
      if (!this.computos[i].isSubtitulo && (this.computos[i].id_computo_metrico || all)) {
        this.saveRow(this.computos[i], i);
      }
    }
  }

  deleteSubtitulo(subtitulo, index) {
    var descripcion = null;
    for (var i = index-1; i >= 0; i--) {
      if (this.computos[i].isSubtitulo) {
        descripcion = this.computos[i].descripcion;
        break;
      }
    }
    this.saveSubtitulo({ descripcion:descripcion }, index);
    this.computos.splice(index, 1);
  }

  /**
   * sacarCopiaComputos - Copiar los computos en la variable auxiliar "this.copiaComputos", el parámetro computo es opcional,
   * en caso de tener este parámetro se reemplaza o se adiciona a la variable auxiliar. Esto con el fin de determinar
   * si hay modificaciones o no para guardar los cómputos cuando el usuario presione "Aceptar" o las opciones "Anterior/Siguiente"
   * si no ha guardado los datos de algunos o todos sus cómputos métricos.
   *
   * @param  {type} computo Objeto con datos de un cómputo métrico
   */
  sacarCopiaComputos(computo) {
    if (computo) {
      const INDEX = this._.findIndex(this.copiaComputos, {id_computo_metrico: computo.id_computo_metrico});
      if (INDEX === -1) {
        if (this.copiaComputos.length === 1 && this._.isEmpty(this.copiaComputos[0])) {
          this.copiaComputos[0] = computo;
        } else {
          this.copiaComputos.push(computo);
        }
      } else {
        this.copiaComputos[INDEX] = angular.copy(computo);
      }
    } else {
      this.copiaComputos = angular.copy(this.computos);
    }
  }

}

export default ComputosMetricosController;
