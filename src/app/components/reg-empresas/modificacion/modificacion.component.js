'usr strict';

import controller from './modificacion.controller';
import template from './modificacion.html';

const ModificacionComponent = {
  bindings: {},
  controller,
  template
};

export default ModificacionComponent;
