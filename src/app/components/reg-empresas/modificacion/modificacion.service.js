'use strict';

class ModificacionService {
  constructor(Util, _) {
    'ngInject';

    this.Util = Util;
    this._ = _;
  }

  getDatosSegunEstado(supervision) {
    let nSupervision = {};
    const atributos = Array.isArray(supervision.estado_actual.atributos) ? supervision.estado_actual.atributos : this._.keys(supervision.estado_actual.atributos);
    atributos.map(atributo => {
      if (!this.Util.isEmpty(supervision[atributo])) {
        nSupervision[atributo] = supervision[atributo];
      }
    });
    return nSupervision;
  }
}

export default ModificacionService;
