'use strict';

class FechaModificacionController {
  constructor($uibModalInstance, Storage, data, Usuarios) {
    'ngInject';

    this.$uibModalInstance = $uibModalInstance;
    this.proyecto = data;
    this.Usuarios = Usuarios;
    this.user = Storage.getUser();
  }

  $onInit() {
    let esSupervisor = this.Usuarios.esSupervisor() || this.Usuarios.esFiscal();
    this.option = {
      confirm_txt: esSupervisor ? '¿Está seguro/a de finalizar y enviar las modificaciones?' : '¿Está seguro/a de aprobar las modificaciones?',
      action: esSupervisor ? 'Registrar' : 'Revisar',
      disabled: !esSupervisor
    };
    this.validacion = {
      fecha_minima: new Date(this.proyecto.adjudicacion.orden_proceder)
    };
    this.validacion.fecha_minima.setDate(this.validacion.fecha_minima.getDate() + 1);
  }

  cerrar() {
    this.$uibModalInstance.dismiss('cancel');
  }

  aceptar() {
    this.$uibModalInstance.close(this.proyecto);
  }
}

export default FechaModificacionController;
