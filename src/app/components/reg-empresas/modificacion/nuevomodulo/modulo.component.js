'use strict';

import controller from './modulo.controller';
import template from './modulo.html';

const ModuloComponent = {
  bindings:{
    modulo: '=',
    modoVista: '<',
    callback: '&',
    options: '<'
  },
  controller,
  template
};

export default ModuloComponent;
