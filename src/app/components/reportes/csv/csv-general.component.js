'use strict';

import template from './csv-general.html';
import controller from './csv-general.controller';

const ReporteCsvComponent = {
    template,
    controller
};

export default ReporteCsvComponent;
