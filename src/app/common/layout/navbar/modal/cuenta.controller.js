'use strict';

class PasswordModalController {
  constructor($uibModalInstance, data, DataService, apiUrl, Message, Storage, Auth, $location) {
    'ngInject';

    this.items = data;
    this.DataService = DataService;
    this.apiUrl = apiUrl;
    this.Message = Message;
    this.$uibModalInstance = $uibModalInstance;
    this.Storage = Storage;
    this.datos = {};
    if(!Auth.$rootScope.auth) $location.path('/'); $location.replace();
  }

  $onInit() {
    this.usuario = this.Storage.getUser();
  }

  ok() {
    this.enviando = true;
    if(this.datos.actual === this.datos.nueva) {
      this.Message.warning("Las contraseñas son iguales, no se ha realizado ningúna modificación.", null, 0);
      this.$uibModalInstance.dismiss('cancel');
      return;
    }
    if(this.datos.actual && this.datos.nueva && this.datos.confirmado) {
      this.DataService.put('contrasena/'+this.usuario.id_usuario+'/usuario', this.datos)
      .then(response => {
        this.enviando = false;
        if(response === null) return; //throw new Error("No se pudo realizar el cambio.");

        this.$uibModalInstance.close(this.datos);
        this.Message.success(response.mensaje, null, 0);
      })
      .catch(error => {
        this.enviando = false;
        this.Message.error(error, null, 0);
      });
    }
    else{
      this.enviando = false;
      this.Message.warning("Verifique los datos introducidos.", null, 0);
    }
  }

  cancel() {
    this.$uibModalInstance.dismiss('cancel');
  }
}

export default PasswordModalController;
