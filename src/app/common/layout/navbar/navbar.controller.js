'use strict';

import modalCuentaController from './modal/cuenta.controller';
import modalCuentaTemplate from './modal/cuenta.html';

class NavbarController {
  constructor($state, $location, $window, Util, Auth, Storage, SidenavFactory, Modal, $log) {
      "ngInject";

      this.$state = $state;
      this.$window = $window;
      this.$location = $location;
      this.Util = Util;
      this.Auth = Auth;
      this.$log = $log;
      this.Storage = Storage;
      this.SidenavFactory = SidenavFactory;
      this.Modal = Modal;
  }

  $onInit() {
    this.user = this.Storage.getUser();
    window.setTimeout(() => {
      if (this.$window.innerWidth < 990) {
        document.querySelector('#app-sidenav').classList.toggle('sidenav-close');
      }
    }, 750);
  }

  toggleSidenav() {
    document.querySelector('#app-sidenav').classList.toggle('sidenav-close');
  }

  fullscreen() {
    document.querySelector('body').classList.toggle('fullscreen');
    this.Util.fullscreen();
  }

  refresh() {
    this.$state.reload();
  }

  toggled(open) {
    this.$log.log('open', open);
  }

  logout() {
    this.Auth.logout();
  }

  account() {
    //this.Sidenav.path("account");
    this.Modal.show({
      template: modalCuentaTemplate,
      controller: modalCuentaController,
      data: {},
      size: ''
    })
    .result.then(() => {
      this.logout();
    });
  }

  changeRol(rol) {
    this.user.rol = rol;
    // this.refresh();
    // Set rol
    this.Storage.set('menu', rol.menu);
    this.SidenavFactory.setMenu(this.Storage.get('menu'));

    this.SidenavFactory.setRol(rol);
    this.Storage.set('rol', this.SidenavFactory.getRol());
    this.Storage.setUser(this.user);

    let rutaAnt = this.$location.path();
    let ruta = rol.menu[0].submenu[0].url;
    this.$location.path(ruta).replace();
    if (rutaAnt.indexOf(ruta) >= 0 || ruta.indexOf(rutaAnt) >= 0) {
      this.$window.setTimeout(() => { this.refresh(); }, 400);
    }
  }
}

export default NavbarController;
