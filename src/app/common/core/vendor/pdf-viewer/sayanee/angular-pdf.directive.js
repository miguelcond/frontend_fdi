
/**
 * Thanks to:
 * https://github.com/sayanee/angularjs-pdf
 */
import template from './viewer.html';

export const NgPdf = ($window, $document, $http, $log, $sce, $timeout, $compile) => {
  'ngInject';

  const backingScale = canvas => {
    const ctx = canvas.getContext('2d');
    const dpr = $window.devicePixelRatio || 1;
    const bsr = ctx.webkitBackingStorePixelRatio ||
      ctx.mozBackingStorePixelRatio ||
      ctx.msBackingStorePixelRatio ||
      ctx.oBackingStorePixelRatio ||
      ctx.backingStorePixelRatio || 1;

    return dpr / bsr;
  };

  const setCanvasDimensions = (canvas, w, h) => {
    const ratio = backingScale(canvas);
    canvas.width = Math.floor(w * ratio);
    canvas.height = Math.floor(h * ratio);
    canvas.style.width = `${Math.floor(w)}px`;
    canvas.style.height = `${Math.floor(h)}px`;
    canvas.getContext('2d').setTransform(ratio, 0, 0, ratio, 0, 0);
    return canvas;
  };

  const initCanvas = (element, canvas, printCanvas) => {
    let divCanvas = $document[0].querySelector('.ng-pdf-container')? $document[0].querySelector('.ng-pdf-container'): $document[0].createElement('div');
    angular.element(divCanvas).addClass('ng-pdf-container');
    angular.element(canvas).addClass('rotate0');
    angular.element(divCanvas).append(canvas);
    element.append(divCanvas);

    let divCanvasPrint = document.querySelector('#print-ng-pdf-container')? document.querySelector('#print-ng-pdf-container'): document.createElement('div');
    divCanvasPrint.innerHTML = '';
    angular.element(printCanvas).addClass('rotate0');
    angular.element(divCanvasPrint)
      .attr('id','print-ng-pdf-container')
      .addClass('print-ng-pdf-container')
      .append(printCanvas);
    document.body.appendChild(divCanvasPrint);

  };

  return {
    restrict: 'E',
    template: template,
    scope: {
      data: '<',
      data64: '<'
    },
    // templateUrl(element, attr) {
    //   return attr.templateUrl ? attr.templateUrl : 'partials/viewer.html';
    // },
    link(scope, element, attrs) {
      let renderTask = null;
      let pdfLoaderTask = null;
      let debug = false;
      let url = scope.pdfUrl;
      let httpHeaders = scope.httpHeaders;
      let pdfDoc = null;
      let pageToDisplay = isFinite(attrs.page) ? parseInt(attrs.page) : 1;
      let pageFit = attrs.scale === 'page-fit';
      let limitHeight = attrs.limitcanvasheight === '1';
      let scale = attrs.scale > 0 ? attrs.scale : 1;
      let canvas = []; //$document[0].createElement('canvas');
      let printCanvas = [];
      let printCtx = [];
      //let canvas = $document[0].createElement('canvas');
      //initCanvas(element, canvas);
      let creds = attrs.usecredentials;
      debug = attrs.hasOwnProperty('debug') ? attrs.debug : false;

      let ctx = [];
      // let ctx = canvas.getContext('2d');
      let windowEl = angular.element($window);

      scope.download = (attrs.download=='true')?true:false;
      scope.print = (attrs.print=='true')?true:false;
      scope.name = (attrs.name)?attrs.name:'documento.pdf';
      scope.pageLimit = 5; // Limite paginas para modo paginado.

      element.css('display', 'block');

      windowEl.on('scroll', () => {
        scope.$apply(() => {
          scope.scroll = windowEl[0].scrollY;
        });
      });

      PDFJS.disableWorker = true;
      scope.pageNum = pageToDisplay;

      let printRenderPage = num => {
        if (renderTask) {
          renderTask._internalRenderTask.cancel();
        }

        pdfDoc.getPage(num).then(page => {
          let pageWidthScale;
          let printRenderContext;

          //for print pages
          let printViewport = page.getViewport(1);
          printViewport = page.getViewport(2);
          if (scope.pageLimit>=scope.pageCount) {
            setCanvasDimensions(printCanvas[num-1], printViewport.width, printViewport.height);
            printRenderContext = {
              canvasContext: printCtx[num-1],
              viewport: printViewport
            };
          } else {
            setCanvasDimensions(printCanvas[0], printViewport.width, printViewport.height);
            printRenderContext = {
              canvasContext: printCtx[0],
              viewport: printViewport
            };
          }
          renderTask = page.render(printRenderContext);
          renderTask.promise.then(() => {
            if (angular.isFunction(scope.onPageRender)) {
              scope.onPageRender();
            }
          }).catch(reason => {
            $log.log(reason);
          });
        });
      };

      scope.renderPage = num => {
        if (renderTask) {
          renderTask._internalRenderTask.cancel();
        }

        pdfDoc.getPage(num).then(page => {
          let viewport;
          let pageWidthScale;
          let renderContext;

          if (pageFit) {
            viewport = page.getViewport(1);
            const clientRect = element[0].getBoundingClientRect();
            pageWidthScale = (clientRect.width / viewport.width) - 0.1;
            if (limitHeight) {
              pageWidthScale = Math.min(pageWidthScale, clientRect.height / viewport.height);
            }
            scale = pageWidthScale;
          }
          viewport = page.getViewport(scale);

          if (scope.pageLimit>=scope.pageCount) {
            setCanvasDimensions(canvas[num-1], viewport.width, viewport.height);
            renderContext = {
              canvasContext: ctx[num-1],
              viewport
            };
          } else {
            setCanvasDimensions(canvas[0], viewport.width, viewport.height);
            renderContext = {
              canvasContext: ctx[0],
              viewport
            };
          }

          renderTask = page.render(renderContext);
          renderTask.promise.then(() => {
            if (angular.isFunction(scope.onPageRender)) {
              scope.onPageRender();
            }
          }).catch(reason => {
            $log.log(reason);
          });

        });
      };

      scope.goPrevious = () => {
        if (scope.pageToDisplay <= 1) {
          return;
        }
        scope.pageToDisplay = parseInt(scope.pageToDisplay) - 1;
        scope.pageNum = scope.pageToDisplay;
      };

      scope.goNext = () => {
        if (scope.pageToDisplay >= parseInt(pdfDoc.numPages)) {
          return;
        }
        scope.pageToDisplay = parseInt(scope.pageToDisplay) + 1;
        scope.pageNum = scope.pageToDisplay;
      };

      scope.zoomIn = () => {
        pageFit = false;
        if(parseFloat(scale) + 0.2 > 2.2) return scale;
        scale = parseFloat(scale) + 0.2;
        // scope.renderPage(scope.pageToDisplay);
        let aux;
        for(let i=0;i<canvas.length;i++) {
          scope.renderPage(i+1);
          if (canvas[i].getAttribute('class') === 'rotate90') {
            aux = parseInt(canvas[i].style.width.replace('px','')) - parseInt(canvas[i].style.height.replace('px',''));
            canvas[i].style.marginBottom = 48 +aux+'px';
          } else {
            canvas[i].style.marginBottom = 8 +'px';
          }
        }
        return scale;
      };

      scope.zoomOut = () => {
        pageFit = false;
        if(parseFloat(scale) - 0.2 < 0.2) return scale;
        scale = parseFloat(scale) - 0.2;
        // scope.renderPage(scope.pageToDisplay);
        let aux;
        for(let i=0;i<canvas.length;i++) {
          scope.renderPage(i+1);
          if (canvas[i].getAttribute('class') === 'rotate90') {
            aux = parseInt(canvas[i].style.width.replace('px','')) - parseInt(canvas[i].style.height.replace('px',''));
            canvas[i].style.marginBottom = aux+'px';
          } else {
            canvas[i].style.marginBottom = 16 +'px';
          }
        }
        return scale;
      };

      scope.fit = () => {
        pageFit = true;
        // scope.renderPage(scope.pageToDisplay);
        for(let i=0;i<canvas.length;i++) {
          scope.renderPage(i+1);
        }
      }

      scope.changePage = () => {
        scope.renderPage(scope.pageToDisplay);
      };

      scope.rotate = () => {
        let aux;
        for(let i=0; i<canvas.length; i++) {
          aux = parseInt(canvas[i].style.width.replace('px','')) - parseInt(canvas[i].style.height.replace('px',''));
          if (canvas[i].getAttribute('class') === 'rotate0') {
            canvas[i].setAttribute('class', 'rotate90');
            canvas[i].style.marginBottom = 16+aux+'px';
          } else if (canvas[i].getAttribute('class') === 'rotate90') {
            canvas[i].setAttribute('class', 'rotate180');
            canvas[i].style.marginBottom = 16 +'px';
          } else if (canvas[i].getAttribute('class') === 'rotate180') {
            canvas[i].setAttribute('class', 'rotate270');
            canvas[i].style.marginBottom = 16+aux+'px';
          } else {
            canvas[i].setAttribute('class', 'rotate0');
            canvas[i].style.marginBottom = 16 +'px';
          }
        }
      };

      scope.downloadFile = () => {
        if(!scope.descargando) {
          scope.descargando = true;
          let a = document.createElement('a');
          a.innerHTML = '.';
          element.append(a);
          $compile(a)(scope);
          // Si se utiliza una dirección URL del archivo PDF
          if (scope.data && scope.data.url) {
            // a = document.getElementById('download_'+scope.name);
            // Si es InternetExplorer PDF
            if (window.navigator.msSaveBlob && scope.data.urlDownload) { // // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
              $http.post(scope.data.urlDownload, {}, { responseType: 'arraybuffer' }).then(resp => {
                if (resp.data) {
                  let file = new window.Blob([resp.data], { type: 'application/pdf' });
                  window.navigator.msSaveOrOpenBlob(file, scope.name||'documento.pdf');
                  scope.descargando = false;
                }
              }).catch(error => { $log.log(error); });
              return;
            }
            if (scope.data.method!='POST') { // // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
              $http.get(scope.data.url, { responseType: 'arraybuffer' }).then(resp => {
                if (resp) {
                  let file = new window.Blob([ (new Uint8Array(resp.data)) ], { type: 'application/pdf' });
                  if (window.navigator.msSaveBlob) {
                    window.navigator.msSaveOrOpenBlob(file, scope.name||'documento.pdf');
                    scope.descargando = false;
                  } else {
                    // a.href = window.URL.createObjectURL(file);
                    let base64String = btoa(String.fromCharCode.apply(null, new Uint8Array(resp.data)));
                    a.href = 'data:application/pdf;base64,' + base64String;
                    a.download = scope.name||'documento.pdf';
                    a.target = '_blank';
                    $timeout(()=>{
                      a.click();
                      scope.descargando = false;
                      angular.element(a).remove();
                    },250);
                  }
                }
              }).catch(error => { $log.log(error); });
              return;
            }
            a.href = scope.data.url;
            a.download = scope.name||'documento.pdf';
            a.target = '_blank';
            $timeout(()=>{
              a.click();
              scope.descargando = false;
              angular.element(a).remove();
            },250);
            return;
          }
          // Para archivos en base 64.
          let blob = b64toBlob(scope.data64.replace('data:application/pdf;base64,',''),'application/pdf');
          if (window.navigator.msSaveBlob) { // // IE hack; see http://msdn.microsoft.com/en-us/library/ie/hh779016.aspx
            window.navigator.msSaveOrOpenBlob(blob, 'documento.pdf');
            scope.descargando = false;
          } else {
            a.href = window.URL.createObjectURL(blob);
            // a.href = 'data:application/pdf;base64,' + scope.data64;
            a.download = scope.name||'documento.pdf';
            a.target = '_blank';
            $timeout(()=>{
              a.click();
              scope.descargando = false;
            },250);
          }
        }
      };

      scope.printFile = () => {
        window.print();
      };

      scope.printFileUrl = () => {
        let documento;
        // let blob = new Blob([scope.data.data], { type: 'application/pdf' });
        // let objectURL = window.URL.createObjectURL(blob, { type: "application/pdf" });
        if (scope.data && scope.data.url) {
          documento = window.open( scope.data.url );
          documento.onload = function() {
            documento.print();
          };
        } else {
          let blob = b64toBlob(scope.data64.replace('data:application/pdf;base64,',''),'application/pdf');
          documento = $window.open( window.URL.createObjectURL(blob) );
          documento.onload = function() {
            documento.print();
          };
        }
        // var documento = $window.open( $sce.trustAsResourceUrl(objectURL) );
      };

      scope.detectIE = () => {
        var ua = window.navigator.userAgent;
        // Test values; Uncomment to check result …
        // IE 10
        // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
        // IE 11
        // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
        // Edge 12 (Spartan)
        // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
        // Edge 13
        // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';
        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
          // IE 10 or older => return version number
          return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }
        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
          // IE 11 => return version number
          var rv = ua.indexOf('rv:');
          return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
          // Edge (IE 12+) => return version number
          return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }
        // other browser
        return false;
      }
      /**
       * Determine the mobile operating system.
       * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
       *
       * @returns {String}
       */
      scope.getMobileOS = () => {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
          return 'Windows Phone';
        }
        if (/android/i.test(userAgent)) {
          return 'Android';
        }
        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
          return 'iOS';
        }
        return false;
      }

      /**
       * b64toBlob - Convert base64 string to a Blob object
       *
       * @param  {string}  b64Data     Base64 string
       * @param  {strig}   contentType Type of base64 file
       * @param  {integer} sliceSize   Slice for performance
       * @return {Blob}                Blob object
       * @description https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
       */
      function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
          var slice = byteCharacters.slice(offset, offset + sliceSize);

          var byteNumbers = new Array(slice.length);
          for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
          }

          var byteArray = new Uint8Array(byteNumbers);

          byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
      }

      function clearCanvas() {
        for(let i=0; i<ctx.length; i++) {
          ctx[i].clearRect(0, 0, canvas[i].width, canvas[i].height);
        }
      }

      function renderPDF() {
        clearCanvas();

        let params;
        // let params = {
        //   'url': url,
        //   'withCredentials': creds
        // };

        if (httpHeaders) {
          params.httpHeaders = httpHeaders;
        }

        if (scope.data) {
          params = scope.data;
        }

        if (scope.data64) {
          params = {
            data: atob(scope.data64.replace('data:application/pdf;base64,',''))
          }
        }

        //if (url && url.length) {
        if (params) {
          $timeout(()=>{
            pdfLoaderTask = PDFJS.getDocument(params);
            pdfLoaderTask.onProgress = scope.onProgress;
            pdfLoaderTask.onPassword = scope.onPassword;
            pdfLoaderTask.then(
              _pdfDoc => {
                if (angular.isFunction(scope.onLoad)) {
                  scope.onLoad();
                }

                pdfDoc = _pdfDoc;
                // scope.renderPage(scope.pageToDisplay);
                //
                let num, cant;
                if (scope.pageLimit>=pdfDoc.numPages) {
                  num = 1;
                  cant = pdfDoc.numPages;
                } else {
                  num = scope.pageToDisplay;
                  cant = parseInt(scope.pageToDisplay) || pdfDoc.numPages;
                }
                //for(var num = 1; num <= pdfDoc.numPages; num++) {
                for(; num <= cant; num++) {
                  canvas.push($document[0].createElement('canvas'));
                  ctx[num-1] = canvas[num-1].getContext('2d');

                  printCanvas.push(document.createElement('canvas'));
                  printCtx[num-1] = printCanvas[num-1].getContext('2d');
                  initCanvas(element, canvas[num-1], printCanvas);
                  scope.renderPage(num);
                  printRenderPage(num);
                }

                scope.$apply(() => {
                  scope.pageCount = _pdfDoc.numPages;
                });
              }, error => {
                if (error) {
                  if (angular.isFunction(scope.onError)) {
                    scope.onError(error);
                  }
                }
              }
            );
          }, 500);
        }
      }

      scope.$watch('pageNum', newVal => {
        if (pdfDoc && newVal > parseInt(pdfDoc.numPages)) {
          newVal = pdfDoc.numPages;
          scope.pageNum = pdfDoc.numPages;
        }
        if(newVal < 1) {
          newVal = 1;
          scope.pageNum = 1;
        }
        scope.pageToDisplay = parseInt(newVal);
        if (pdfDoc !== null) {
          scope.renderPage(scope.pageToDisplay);
          printRenderPage(scope.pageToDisplay);
        }
      });

      scope.$watch('data', newVal => {
        if (newVal && newVal !== '') {
          if (debug) {
            $log.log('data change detected: ', scope.data);
          }
          url = newVal;
          scope.pageNum = scope.pageToDisplay = pageToDisplay;
          if (pdfLoaderTask) {
            pdfLoaderTask.destroy().then(() => {
              renderPDF();
            });
          } else {
            renderPDF();
          }
        }
      });

      scope.$watch('data64', newVal => {
        if (newVal && newVal !== '') {
          if (debug) {
            $log.log('data64 change detected: ', scope.data64);
          }
          url = newVal;
          scope.pageNum = scope.pageToDisplay = pageToDisplay;
          if (pdfLoaderTask) {
            pdfLoaderTask.destroy().then(() => {
              renderPDF();
            });
          } else {
            renderPDF();
          }
        }
      });

      scope.$on('downloadFile', function() {
        scope.downloadFile();
      });

      scope.$on('printFile', function() {
        scope.printFile();
      });
    }
  }
}
