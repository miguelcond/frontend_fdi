'use strict';

class PdfViewerController {

  constructor($q, $scope, $http, Storage) {
    'ngInject';

    this.defer = $q.defer();
    this.$scope = $scope;
    this.$http = $http;
    this.Storage = Storage;
  }

  $onInit () {

    // atob() is used to convert base64 encoded PDF to binary-like data.
    // (See also https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/
    // Base64_encoding_and_decoding.)}


    this.base64Data =
      'JVBERi0xLjcKCjEgMCBvYmogICUgZW50cnkgcG9pbnQKPDwKICAvVHlwZSAvQ2F0YWxvZwog' +
      'IC9QYWdlcyAyIDAgUgo+PgplbmRvYmoKCjIgMCBvYmoKPDwKICAvVHlwZSAvUGFnZXMKICAv' +
      'TWVkaWFCb3ggWyAwIDAgMjAwIDIwMCBdCiAgL0NvdW50IDEKICAvS2lkcyBbIDMgMCBSIF0K' +
      'Pj4KZW5kb2JqCgozIDAgb2JqCjw8CiAgL1R5cGUgL1BhZ2UKICAvUGFyZW50IDIgMCBSCiAg' +
      'L1Jlc291cmNlcyA8PAogICAgL0ZvbnQgPDwKICAgICAgL0YxIDQgMCBSIAogICAgPj4KICA+' +
      'PgogIC9Db250ZW50cyA1IDAgUgo+PgplbmRvYmoKCjQgMCBvYmoKPDwKICAvVHlwZSAvRm9u' +
      'dAogIC9TdWJ0eXBlIC9UeXBlMQogIC9CYXNlRm9udCAvVGltZXMtUm9tYW4KPj4KZW5kb2Jq' +
      'Cgo1IDAgb2JqICAlIHBhZ2UgY29udGVudAo8PAogIC9MZW5ndGggNDQKPj4Kc3RyZWFtCkJU' +
      'CjcwIDUwIFRECi9GMSAxMiBUZgooSGVsbG8sIHdvcmxkISkgVGoKRVQKZW5kc3RyZWFtCmVu' +
      'ZG9iagoKeHJlZgowIDYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDAwMDEwIDAwMDAwIG4g' +
      'CjAwMDAwMDAwNzkgMDAwMDAgbiAKMDAwMDAwMDE3MyAwMDAwMCBuIAowMDAwMDAwMzAxIDAw' +
      'MDAwIG4gCjAwMDAwMDAzODAgMDAwMDAgbiAKdHJhaWxlcgo8PAogIC9TaXplIDYKICAvUm9v' +
      'dCAxIDAgUgo+PgpzdGFydHhyZWYKNDkyCiUlRU9G';

    // For Base64 files
    // this.pdfUrl = {
    //   data: atob(this.base64Data)
    // };

    // TODO Falta implementar print y download
    // Url files
    // this.pdfUrl = '//cdn.mozilla.net/pdfjs/tracemonkey.pdf';

    // TODO Opciones todavia no probadas. Falta implementar print y download
    // withCredentials
    // this.pdfUrl = {
    //   'url': '//cdn.mozilla.net/pdfjs/tracemonkey.pdf',
    //   'method': 'POST',
    //   'withCredentials': false,
    //   'httpHeaders': { 'Authorization': 'Bearer some-aleatory-token' }
    // };
    // this.base64 = this.base64Data;
    // if(this.pdfUrl) {
    //   this.pdfUrl = '//cdn.mozilla.net/pdfjs/tracemonkey.pdf';
    // }
    if(this.base64) {
      this.pdfBase64 = this.base64;
    }
  }

  $onChanges() {
    // if(this.base64) {
    //   this.pdfBase64 = this.base64;
    // }
    if(this.data) {
      if(this.data.method == 'POST') {
        let config = {
          responseType: 'arraybuffer'
        }
        if(this.Storage.get('token')) {
          config.headers = {
            'Authorization': this.Storage.get('token')
          }
        }
        this.data.urlDownload = this.data.url;
        this.$http.post(this.data.urlDownload, {}, config).then(resp => {
          if (resp && resp.data) {
            let file = new window.Blob([resp.data], { type: 'application/pdf' });
            this.data.url = window.URL.createObjectURL(file);
            this.pdfUrl = this.data;
          }
        }).catch(error => { });
      } else {
        this.pdfUrl = this.data;
      }
    }
    this.$scope.$watch(() => {
      this.heightPdfContainer = parseInt(this.height, 10) - 32 + 'px';
      angular.element(document.querySelectorAll('.ng-pdf-container')).css('max-height', this.heightPdfContainer);
    });
  }

  $onDestroy() {
    let ngPrintFile = document.getElementById('print-ng-pdf-container');
    ngPrintFile.parentNode.removeChild(ngPrintFile);
  }

}

export default PdfViewerController;
