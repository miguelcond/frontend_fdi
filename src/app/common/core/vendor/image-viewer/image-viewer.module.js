'use strict';

import {ImageViewer} from './image-viewer.directive';

const ImageViewerModule = angular
    .module('app.image-viewer', [])
    .directive('imageViewer', ImageViewer)
    .name;

export default ImageViewerModule;
