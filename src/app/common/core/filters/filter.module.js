'use strict';

import Datetime from './datetime.filter';
import Text from './text.filter';
import Number from './number.filter';
import NombreUnido from './nombre-unido.filter';
import Typeof from './typeof.filter';

const Filter = angular
    .module('app.filters', [])
    .filter('datetime', Datetime)
    .filter('characters', Text.characters)
    .filter('splitcharacters', Text.splitcharacters)
    .filter('words', Text.words)
    .filter('numberToLiteral', Number.numberToLiteral)
    .filter('formatDecimal', Number.formatDecimal)
    .filter('nombreUnido', NombreUnido)
    .filter('typeof', Typeof)
    .name;

export default Filter;
