'use strict';

const TypeofFilter = () => {
    'ngInject';

    return (object) => {
        return typeof object;
    };
};

export default TypeofFilter;
