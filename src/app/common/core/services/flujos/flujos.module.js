'use strict';

import FlujosService from './flujos.service';

const Flujos = angular
    .module('app.service.flujos',[])
    .service('Flujos', FlujosService)
    .name;

export default Flujos;
