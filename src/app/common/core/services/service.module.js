/**
 * No se puede definir factories con las clases de EcmaScript 6, por lo cual se deben
 * utilizar services para definir estos factories(factory), si se quieren factories
 * se deben codificar de la manera tradicional con la estructura:
 * function () { return {} };
 */

'use strict';

import Filter from './filter/filter.module';
import Data from './data/data.module';
import Flujos from './flujos/flujos.module';

import Storage from './storage/storage.service';
import Util from './util/util.service';
import numToWord from './util/numeroaLetras.service';
import urm2lat from './util/urm2lat.service';
import ArrayUtil from './array/array.service';
import Datetime from './datetime/datetime.service';
import Message from './message/message.module';
import Modal from './modal/modal.module';
import Formly from './formly/formly.service';
import DataTable from './data-table/data-table.service';
import DownloadFile from './file/download-file.directive';

const Core = angular
    .module('app.services',[
        Filter,
        Data,
        Message,
        Modal,
        Flujos
    ])
    .service('numToWord', numToWord)
    .service('urm2lat', urm2lat)
    .service('Util', Util)
    .service('Storage', Storage)
    .service('ArrayUtil', ArrayUtil)
    .service('Datetime', Datetime)
    .service('Formly', Formly)
    .service('DataTable', DataTable)
    .directive('downloadFile', () => new DownloadFile())
    .name;

export default Core;
