'use strict';

class UtilService {

  constructor($injector, $location, $anchorScroll, $log, Message, decimales, Storage, _, Big, Modal) {
    "ngInject";

    this.$injector = $injector;
    this.$location = $location;
    this.$anchorScroll = $anchorScroll;
    this.$log = $log;
    this.Message = Message;
    this.decimales = decimales;
    this.Storage = Storage;
    this._ = _;
    this.Big = Big;
    this.Modal = Modal;
    this.camposInvalidos = {};
  }

  $onInit() {
    this.tmpl_print = `
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8"/>
    <title>Document</title>
    <style>{css}</style>
    </head>
    <body>{body}</body>
    </html>`;
  }

  toType (obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  }

  isJson (text) {
    return /^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''));
  }

  stripHTML(texto) {
    return texto.replace(/(<([^>]+)>)/ig,"");
  }

  nano (template, data) {
    return template.replace(/\{([\w\.]*)\}/g, function (str, key) {
      var keys = key.split("."), v = data[keys.shift()];
      for (var i = 0, l = keys.length;i < l;i++)
      v = v[keys[i]];
      return (typeof v !== "undefined" && v !== null) ? v : "";
    });
  }

  print (html, css) {
    if (typeof css == 'string') {
      angular.element.get(css, (response) => {
        var popup = window.open('', 'print');
        popup.document.write(this.nano(this.tmpl_print, {body : html, css : response}));
        popup.document.close();
        popup.focus();
        popup.print();
        popup.close();
      });
    } else {
      var popup = window.open('', 'print');
      popup.document.write(this.nano(this.tmpl_print, {body : html, css : css}));
      popup.document.close();
      popup.focus();
      popup.print();
      popup.close();
    }

    return true;
  }

  /**
  * Función para crear una pausa para ejecutar una instrucción:
  *  Ejemplo:
  *
  *  let funcionConDelay = this.Util.debounce(function() {
  *      // ... lo que se quiere ejecutar con el retraso
  *  }, 1000);
  *
  *  funcionConDelay(); // Ejecutando el retraso
  */
  debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }

  popup(url) {
    window.open(url, 'print');
  }

  fullscreen () {
    if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
        if (document.documentElement.requestFullscreen) {
          document.documentElement.requestFullscreen();
        } else if (document.documentElement.msRequestFullscreen) {
          document.documentElement.msRequestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
          document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
          document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.msExitFullscreen) {
          document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        }
      }
    }

    size(obj) {
      return Object.keys(obj).length;
    }

    serialize(json) {
      var string = [];
      for (var i in json) {
        string.push(i + '=' + json[i]);
      }
      return string.join('&');
    }

    getMenuOption(menu, url) {
      for (var i in menu) {
        if (typeof menu[i].submenu != 'undefined') {
          var pages = menu[i].submenu;
          for (var j in pages) {
            if (pages[j].url == url) {
              return [menu[i].label, pages[j].label];
            }
          }
        }
      }
      for (var k in menu) {
        if (menu[k].url == url) {
          return [menu[k].label, false];
        }
      }
      return [false,false];
    }

    getKeys(data) {
      var types = {};

      data.map((el) => {
        types[el.key] = el;
      });

      return types;
    }


    /**
    * Public: funcion que retrasa la ejecucion de una funcion, y que al volverse a activar reinicia el timeout
    * @param {Object} obj  objeto que guardara la referencia del intervalo
    * @param {Integer} ms  tiempo en miliesgundos
    * @param {Function} func funcion callback
    * *
    * Ejemplo
    this.timer = {};
    this.Util.funcDelay(this.timer, 1000, ()=> {
    this.personalSave(form, per);
  })
  */
  funcDelay (obj, ms, func) {
    if ( obj.timer_delay )
    clearTimeout (obj.timer_delay);

    obj.timer_delay = setTimeout(() => {
      obj.timer_delay = null;
      func();
    }, ms);
  }

  setTouchedForm (form, val, ignored, focusear) {
    var focus, funcRecursive;
    // this.$log.log(form);
    // this.$log.log(val, ignored);
    if (ignored) {
      focus = ignored.split(', ');
      ignored = {};
      focus.forEach( it => {
        ignored[it] = true;
      })
    } else {
      ignored = {};
    }
    if (angular.isUndefined(val)) val = true;
    focus = val && angular.isDefined(focusear);
    // var visits = {};
    // this.$log.log(visits);
    funcRecursive = (form, val, ignored) => {
      form.$$controls.forEach( field => {
        // if (!visits[field.$name])
        // visits[field.$name] = 0;
        if (field.$name!='' && !ignored[field.$name]) {
          // visits[field.$name]++;
          if (field.$$controls) {
            funcRecursive(field, val, ignored);
          } else {
            if (focus && field.$invalid) {
              // this.$log.log(field);
              var node = field.$$element[0];
              var x = node.contentEditable;
              node.contentEditable =  true;
              if (node.disabled) {
                node.disabled = false;
                node.focus();
                node.disabled = true;
              }else {
                node.focus();
              }
              node.contentEditable = x;
              // this.$log.log(x, node);
              node = field.$$element.closest(focusear);
              if (node) {
                // this.gotoAnchor(null, node);
                node.classList.add('blink-container');
                setTimeout(() => {
                  node.classList.remove('blink-container');
                }, 5000);
              }

              // field.$$element.closest('div.form-group').focus();
              focus = !focus;
            }
            if (val) {
              field.$setTouched();
            } else {
              field.$setUntouched();
            }
          }
        }
      })
    };
    funcRecursive(form, val, ignored);
  }

  isValidForm (form, ignored) {
    var resp, funcRecursive;
    // this.$log.log(form);
    // this.$log.log(val, ignored);
    if (ignored) {
      resp = ignored.split(', ');
      ignored = {};
      resp.forEach( it => {
        ignored[it] = true;
      })
    } else {
      ignored = {};
    }
    resp = true;
    //var visits = {};
    // this.$log.log(visits);
    funcRecursive = (form, ignored) => {
      form.$$controls.forEach( field => {
        // if (!visits[field.$name])
        // visits[field.$name] = 0;
        if (field.$name!='' && !ignored[field.$name]) {
          // visits[field.$name]++;
          if (field.$$controls) {
            funcRecursive(field, ignored);
          } else {
            if (field.$invalid){
              resp = false;
              //console.log(field.$name+" NO VALIDO");
              let elemento = angular.element(document.querySelector('[name=' + field.$name + ']'));
              let label = this.obtenerLabel(field.$name, elemento);
              if (angular.isUndefined(this.camposInvalidos[field.$name])) {
                if (label) {
                  this.camposInvalidos[label] = label;
                }else{
                  this.camposInvalidos[field.$name] = field.$name;
                }
              }
            }
          }
        }
      });
    };
    funcRecursive(form, ignored);
    return resp;
  }

  obtenerLabel(nombreCampo, elemento) {
   let elementoPadre = elemento.parent();
   let i = 0;
   while (i < 10) {
     let elemPadre = elementoPadre.find('label');
     if (elemPadre.length > 0) {
       for (let j = 0; j < elemPadre.length; j++) {
         if (elemPadre[j].htmlFor === nombreCampo) {
           let etiqueta = elemPadre[j].innerText;
           if (elementoPadre[j].hasOwnProperty('title') && elemPadre[j].title !== '') {
             etiqueta = elemPadre[j].title;
           }
           return etiqueta;
         }
       }
     }
     elementoPadre = elementoPadre.parent();
     i++;
   }
   return null;
  }

  id(length = 5) {
    let zero = '';
    for (let i = 0, l = length; i < l; i++) {
      zero += '0';
    }
    return (zero + (Math.random() * Math.pow(36, length) << 0).toString(36)).slice(-1 * length);
  }

  gotoAnchor(id) {
    if (this.$location.hash() !== id) {
      this.$location.hash(id);
    } else {
      this.$anchorScroll();
    }
    let el = document.querySelector(`#${id}`);
    if (el) {
      el.classList.add('blink-container');
      setTimeout(() => {
        el.classList.remove('blink-container');
      }, 8000);
    }
  }
  isEmpty(data) {
    let hasOwnProperty = Object.prototype.hasOwnProperty;
    if (data == null) return true;
    if (data.length > 0) return false;
    if (data.length === 0) return true;
    if (typeof data === "number") return false;
    if (typeof data !== "object") return true;
    for (let key in data) {
      if (hasOwnProperty.call(data, key)) return false;
    }
    return true;
  }

  isIEoEdge() {
    if (window.navigator.msSaveBlob) {
      return true;
    }
    return false;
  }

  isMobile() {
    const device = this.isAndroid() || this.isBlackBerry() || this.isiOS() || this.isOpera() || this.isWindows();
    return !this.isEmpty(device)? true : false;
  }

  isAndroid() {
    return navigator.userAgent.match(/Android/i);
  }

  isBlackBerry() {
      return navigator.userAgent.match(/BlackBerry/i);
  }

  isiOS() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  }

  isOpera() {
      return navigator.userAgent.match(/Opera Mini/i);
  }

  isWindows() {
      return navigator.userAgent.match(/IEMobile/i);
  }


  getFormatoRadioOptions(datos) {
    return datos.map(item => ({
      text: item.nombre,
      field: item.codigo
    }));
  }

  limpiarEspaciosDobles(texto) {
    return texto.replace(/\s+/gi, ' ');
  }

  findArray(array, item) {
    for (let i = 0; i < array.length; i++) {
      if (array[i] === item) {
        return true;
      }
    }
    return false;
  }

  isValidFormFieldsAlerta(form, mensaje) {
    this.camposInvalidos = {};
    if (!this.isValidForm(form)) {
      this.setTouchedForm(form);
      this.Message.warning(mensaje || "Verifique, corrija y/o llene los campos resaltados.");
      return false;
    }
    return true;
  }

  isValidFormFields(form, mensaje) {
    this.camposInvalidos = {};
    if (!this.isValidForm(form)) {
      this.setTouchedForm(form);
      this.Message.warning(mensaje || "Verifique, corrija y/o llene los campos resaltados.");
      if(Object.keys(this.camposInvalidos).length > 0){
        let texto = '<p>Verifique, corrija y/o llene los siguientes campos:</p>';
        texto += '<ul>';
        angular.forEach(this.camposInvalidos, (error) => {
          texto += '<li>' + error + '</li>';
        });
        texto += '</ul>' ;
        //this.Message.warning(mensaje || "Verifique, corrija y/o llene los campos resaltados.");
        this.Modal.alert(texto, () => { return false; }, 'Información incompleta', null, 'md');
      }
      return false;
    }
    return true;
  }

  getNombreUnido(datos) {
    let nombre = '';
    nombre += datos.primer_apellido? `${datos.primer_apellido} ` : '';
    nombre += datos.segundo_apellido? `${datos.segundo_apellido} ` : '';
    nombre += datos.nombres;
    return nombre;
  }

  // Redondear utilizando la libreria Big.js
  redondear(numero, decimales) {
    numero = parseFloat(numero);
    if ( isNaN(numero) ) {
      return;
    }
    decimales = parseInt(decimales || this.decimales || 2);
    return parseFloat(this.Big(numero).toFixed(decimales));
  }

  // TODO redondeo Funcion utilizada para redondear. reemplazado por existir algunos casos
  redondear2(numero, decimales) {
    if (typeof numero !== 'number') {
      return;
    }
    decimales = decimales || this.decimales || 2;
    const ES_NEGATIVO = numero < 0;
    numero = ES_NEGATIVO ? numero * -1 : numero;
    const FACTOR_CONVERSION = Math.pow(10, decimales);
    numero = parseFloat((numero * FACTOR_CONVERSION).toFixed(11));
    numero = (Math.round(numero) / FACTOR_CONVERSION);
    numero = ES_NEGATIVO ? (numero * -1) : numero;
    return numero;
  }

  _operar(parametros, conRedondeo = true, decimales, opcion) {
    let resultado = opcion === 'add' || opcion === 'minus' ? 0 : 1;
    for (let i = 0; i < parametros.length; i++) {
      parametros[i] = typeof parametros[i] === 'number' ? parametros[i] : opcion === 'add' || opcion === 'minus' ? 0 : 1;
      let valor = this.Big(parametros[i]);
      resultado = valor[opcion](resultado);
    }
    resultado = parseFloat(resultado.valueOf());
    return conRedondeo ? this.redondear(resultado, decimales) : resultado;
  }

  multiplicar(parametros, redondear, decimales) {
    return this._operar(parametros, redondear, decimales, 'mul');
  }

  sumar(parametros, redondear, decimales) {
    return this._operar(parametros, redondear, decimales, 'add');
  }

  restar(parametro1, parametro2, redondear, decimales) {
    return this._operar([parametro2, parametro1], redondear, decimales, 'minus');
  }

  dividir(parametro1, parametro2, redondear, decimales) {
    if (parametro2 === 0) {
      return;
    }
    return this._operar([parametro2, parametro1], redondear, decimales, 'div');
  }

  valorAbsoluto(valor) {
    valor = typeof valor === 'number' && !isNaN(valor) ? valor : 0;
    valor = this.Big(valor);
    return parseFloat(valor.abs().valueOf());
  }

  esEmpresa(usuario) {
    usuario = usuario ? usuario : this.Storage.getUser();
    return usuario.rol && usuario.rol.nombre === 'EMPRESA';
  }

  getNumeroOrdinal(i) {
    if (i === 1 || i === 3) {
      return i + 'er';
    }
    if (i === 2) {
      return i + 'do';
    }
    if (i === 4 || i === 5 || i === 6) {
      return i + 'to';
    }
    if (i === 7 || i === 10) {
      return i + 'mo';
    }
    if (i === 8) {
      return i + 'vo';
    }
    if (i === 9) {
      return i + 'no';
    }
    return i + 'avo';
  }

  getPorcentaje(numero, porcentaje) {
    // return this.redondear(this.redondear(numero * porcentaje)/100);
    return parseFloat(this.Big(numero).mul(porcentaje).div(100).toFixed(this.decimales||2));
  }

  arrayToObject(array) {
    return this._.zipObject(array, Array.from(new Array(array.length), () => true));
  }
}

export default UtilService;
