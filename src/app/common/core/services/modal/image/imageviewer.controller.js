'use strict';

class ImageviewerModalController {
    constructor($uibModalInstance, $timeout, $window, data) {
        'ngInject';

        this.$uibModalInstance = $uibModalInstance;
        this.$timeout = $timeout;
        this.$window = $window;

        this.title = (data.title)? data.title: '';
        if (data.image) {
          this.image = data.image;
          if (data.image.nombre) {
            this.title += (this.title==='')? data.image.nombre: ' - ' + data.image.nombre;
          }
        }
    }

    $onInit() {
      this.$ivScope = {}; // TODO Buscar otra forma de obtener el $scope de la directiva image-viewer
      this.$timeout(()=>{
        if(this.$window.innerWidth>768) {
          document.querySelector('.modal-dialog').style.maxWidth = '90%';
        } else {
          document.querySelector('.modal-dialog').style.maxWidth = '100%';
        }
        document.querySelector('.modal-dialog').style.maxHeight = this.$window.innerHeight + 'px';
      },100);
    }

    save() {
      this.$uibModalInstance.close(this.informe);
    }

    cancel() {
      this.$uibModalInstance.dismiss('cancel');
    }

    getBase64Image() {
      if(this.informe.base64.indexOf('base64,')>0) {
        return this.informe.base64;
      }
      return 'data:' + this.informe.tipo + ';base64,' + this.informe.base64;
    }

    getBlobImage() {
      if(!this.descargando) {
        this.descargando = true;
        let file,a = document.createElement('a');
        a.innerHTML = '.';
        document.body.appendChild(a);
        file = this.$$b64toBlob(this.informe.base64.replace('data:' + this.informe.tipo + ';base64,' + this.informe.base64,''), this.informe.tipo);
        if (window.navigator.msSaveBlob) {
          window.navigator.msSaveOrOpenBlob(file, this.informe.nombre||'documento.pdf');
          this.descargando = false;
        } else {
          a.href = window.URL.createObjectURL(file);
          // let base64String = btoa(String.fromCharCode.apply(null, new window.Uint8Array(resp.data)));
          // a.href = 'data:application/pdf;base64,' + base64String;
          a.download = this.informe.nombre||'documento.pdf';
          a.target = '_blank';
          this.$timeout(()=>{
            a.click();
            this.descargando = false;
            angular.element(a).remove();
          },250);

        }
      }
    }

    descargarImg() {
      if(this.$ivScope.imageDownload) this.$ivScope.imageDownload();
    }

    imprimirImg() {
      if(this.$ivScope.imagePrint) this.$ivScope.imagePrint();
    }

    /**
     * b64toBlob - Convert base64 string to a Blob object
     *
     * @param  {string}  b64Data     Base64 string
     * @param  {strig}   contentType Type of base64 file
     * @param  {integer} sliceSize   Slice for performance
     * @return {Blob}                Blob object
     * @description https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     */
    $$b64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      let byteCharacters;
      if (b64Data.split(',')[0].indexOf('base64') >= 0) {
        byteCharacters = atob(b64Data.split(',')[1]);
      } else {
        byteCharacters = atob(b64Data);
      }
      let byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new window.Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
    }
}

export default ImageviewerModalController;
