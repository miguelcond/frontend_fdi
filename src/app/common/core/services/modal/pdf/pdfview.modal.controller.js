'use strict';
class PdfViewModalController {
  constructor($uibModalInstance, data, $sce, DataService, $log) {
    'ngInject';

    this.item = data;
    this.$uibModalInstance = $uibModalInstance;
    this.$sce = $sce;
    this.DataService = DataService;
    this.$log = $log;
  }  

  $onInit() {
    if (this.item.type === 'base64') {
      this.getFileBase64();
    } else if (this.item.type === 'strBase64') { // TODO solo se asume archivos PDFs
      this.documento = {
          base64: this.item.urlPdf.indexOf('base64')>0? this.item.urlPdf: 'data:application/pdf;base64,' + this.item.urlPdf,
          tipo: 'application/pdf'
      }
    } else {
      this.getFile();
    }
  }

  getFile() {
    this.DataService.pdf(this.item.urlPdf, {}).then((response) => {
      this.$log.log('Documento PDF = ', response);
      if (response) {
        this.urlPdf = response;
      }
    });
  }

  reloadFile(){
    if (this.item.type === 'base64') {
      this.getFileBase64Reloaded();
    } else if (this.item.type === 'strBase64') { // TODO solo se asume archivos PDFs
      this.documento = {
          base64: this.item.urlPdf.indexOf('base64')>0? this.item.urlPdf: 'data:application/pdf;base64,' + this.item.urlPdf,
          tipo: 'application/pdf'
      }
    } else {
      this.getFile();
    }
  }

  getFileBase64Reloaded() {
    this.DataService.get(this.item.urlPdf+"&reload=1", {}).then((response) => {
      if (response) {
        this.documento = {
            base64: 'data:' + response.datos.tipo + ';base64,' + response.datos.base64,
            tipo: response.datos.tipo
        }
      }
    });
  }

  getFileBase64() {
    this.DataService.get(this.item.urlPdf, {}).then((response) => {
      if (response) {
        this.documento = {
            base64: 'data:' + response.datos.tipo + ';base64,' + response.datos.base64,
            tipo: response.datos.tipo
        }
      }
    });
  }

  fix(){
    this.reloadFile();
  }

  ok() {
    this.$uibModalInstance.close(/*Algun valor*/);
  }

  cancel() {
    this.$uibModalInstance.dismiss('cancel');
  }
}

export default PdfViewModalController;
