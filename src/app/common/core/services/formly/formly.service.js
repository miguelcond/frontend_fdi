'use strict';

class Formly {
  constructor(){
    'ngInject';

  }

  $onInit(){
    this.cargarPlantillas();
  }
  nuevaPlantilla(tipo){
    return this.parse(this.templates[tipo].template);
  }

  init (formlyConfig) {
    formlyConfig.setType({
      name: 'texto',
      templateUrl: 'app/common/util/formly/texto/texto.html'
    });
    formlyConfig.setType({
      name: 'lista',
      templateUrl: 'app/common/util/formly/listas/lista.html'
    });
  }

  obtenerConfiguracionItem(item){
    if(item.type === 'layout') return this.parse(item);
    var obj = {
      type: item.type,
      key: item.key,
      className: this.parse(item.className),
      templateOptions: this.parse(item.templateOptions),
      validation: this.parse(item.validation)
    };
    return obj;
  }

  parse(data){
    return angular.fromJson(angular.toJson(data));
  }

  listaAFormulario(listaPlantillas, configForm){
    var
    nro_columnas = configForm.columnas || 1,
    clase = 'flex-gt-sm-' + parseInt(100/nro_columnas),
    nro_filas = parseInt(listaPlantillas/nro_columnas),
    formularios = [];

    for (var i = 0; i < nro_filas; i++) {
      formularios.push({
        elementAttributes: {
          'layout-gt-sm': 'row',
          'layout': 'column',
          'layout-align-gt-sm': 'start-end'
        },
        fieldGroup: []
      });
    }

    var
    nro_form = 0,
    contador = 0;

    while (nro_form < listaPlantillas.length) {
      // var nro_col = 0;
      while (nro_form < listaPlantillas.length && nro_form < nro_columnas) {
        listaPlantillas[nro_form].className = clase;
        if(listaPlantillas[nro_form].templateOptions.label === '' || listaPlantillas[nro_form].templateOptions.label === null) {
          listaPlantillas[nro_form].key = listaPlantillas[nro_form].templateOptions.placeholder;
        } else {
          listaPlantillas[nro_form] = listaPlantillas[nro_form].templateOptions.label;
        }

        if(listaPlantillas[nro_form].type === 'radio') {
          listaPlantillas[nro_form].templateOptions.className =  'layout  layout-wrap';
        }

        if (configForm.deshabilitar || false) {
          listaPlantillas[nro_form].templateOptions.disabled = true;
        } else {
          listaPlantillas[nro_form].templateOptions.disabled = false;
        }

        if (configForm.todoMayuscula || false) {
          if (listaPlantillas[nro_form].type === 'input'){
            var onKeyUp = 'model["' + listaPlantillas[nro_form].key + '"]= model["'+listaPlantillas[nro_form].key+'"].toUpperCase()';
            listaPlantillas[nro_form].templateOptions.onKeyUp = onKeyUp;
          }
        }

        if(configForm.validacion || false) {
          if (listaPlantillas[nro_form].type === 'input' || listaPlantillas[nro_form].type === 'datepicker') {
            listaPlantillas[nro_form].validation = {
              messages: { required: '"Debe llenar este campo."'}
            };
          }
        }

        formularios[contador].fieldGroup.push(this.obtenerConfiguracionItem(listaPlantillas[nro_form]));
        nro_form++;
        // nro_col++;

      }
      contador++;

    }
    return formularios;

  }

  filtrarDatos(listaPlantillas, listaValores){
    for (var i = 0; i < listaPlantillas.length; i++) {
      if( listaPlantillas[i].type === "datepicker"){
        var nombre = listaPlantillas[i].templateOptions.label;
        if(listaValores.hasOwnProperty(nombre)){
          listaValores[nombre] = new Date(listaValores[nombre]);
        }
      }
    }
  }

  cargarPlantillas(){
    this.templates = {
      input:{
        template: {
          type: 'input',
          key: 'inputText',
          templateOptions: {
            type: 'text',
            label: 'Campo de texto',
            disabled: false
          }
        }
      },
      radio: {
        template: {
          type: 'radio',
          key: 'inputRadio',
          templateOptions: {
            label: 'Selección simple',
            labelProp: 'value',
            valueProp: 'id',
            vertical:false,
            options:[
              {value: 'Opción 1', id:'op1'},
              {value: 'Opción 2', id:'op2'}
            ]
          }
        }
      },
      select: {
        template: {
          type: 'select',
          key: 'inputSelect',
          templateOptions: {
            label: 'Lista de selección',
            multiple: false,
            labelProp: 'value',
            valueProp: 'id',
            options: [
              {value: 'Opción 1', id:'op1'},
              {value: 'Opción 2', id:'op2'}
            ]
          }
        }
      },
      select_multiple: {
        template: {
          type: 'select',
          key: 'inputSelect',
          templateOptions: {
            label: 'Lista de selección multiple',
            multiple: true,
            labelProp: 'value',
            valueProp: 'id',
            options: [
              {value: 'Opción 1', id:'op1'},
              {value: 'Opción 2', id:'op2'}
            ]
          }
        }
      },
      textarea: {
        template: {
          type: 'textarea',
          key: 'inputTextArea',
          templateOptions: {
            label: 'Área de texto',
            rows: 2,
            grow: false
          }
        }
      },
      checkbox: {
        template: {
          type: 'checkbox',
          key: 'inputCheckBox',
          templateOptions: {
            label: 'Casilla de verificación',
            disabled: false
          }
        }
      },
      datepicker: {
        template: {
          type: 'datepicker',
          key: 'inputDatePicker',
          templateOptions:{
            label: 'Calendario'
          }
        }
      },
      titulo: {
        template: {
          type: 'texto',
          key: 'texto',
          templateOptions: {
            label: 'Título',
            tipo: 'h2',
            className: 'ap-text-left',
            mTop:true,
            mBot: true
          }
        }
      },
      subtitulo: {
        template: {
          type: 'texto',
          key: 'texto',
          templateOptions: {
            label: 'Subtítulo',
            tipo: 'h2',
            className: 'ap-text-left',
            mTop:true,
            mBot: true
          }
        }
      },
      parrafo: {
        template: {
          type: 'texto',
          key: 'parrafo',
          templateOptions: {
            label: `Lorem ipsum dolor sit amet, consectetur adipisicing elit,
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
            nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.`,
            tipo: 'p',
            className: 'ap-text-justify',
            mTop: true,
            mBot: true
          }
        }
      },
      ccArchivo: {
        template: {
          type: 'ccArchivo',
          key: 'ccArchivo',
          templateOptions: {
            labelw: 10,
            label: 'CC/Archivo:'
          }
        }
      },
      cite: {
        template: {
          type: 'cite',
          templateOptions: {
            labelCite: 'CITE',
            labelFecha: 'FECHA',
            tipo: 'general',
            numeracionPagina: 'false',
            tipoHoja: 'Letter'
          }
        }
      }
    };
  }

  datosAVista(datos, modelo){
    var
    elem,
    form = [],
    llaves = [];

    for (var i = 0; i < datos.length; i++) {
      if(datos[i].type == "layout") {
        var fg = this.parse(datos[i]);
        var clase = " flex-" + parseInt(100/fg.elementAttributes['num-cols']);
        delete fg.type;
        delete fg.elementAttributes['num-cols'];
        fg.fieldGroup = [];
        for (var j = 0; j < datos[j].fieldGroup.length; j++) {
          elem = this.obtenerConfiguracionItem(datos[i].fieldGroup[j]);
          elem.className += clase;
          this.cambiarElemento(elem, llaves, modelo);
          fg.fieldGroup.push(elem);
        }
        form.push(fg);
      } else {
        elem = this.obtenerConfiguracionItem(datos[i]);
        this.cambiarElemento(elem, llaves, modelo);
        form.push(elem);
      }
    }
  }

  tamanoObjeto(objeto){
    return Object.keys(objeto).length;
  }

  mostrarErrorRecursivo(errores){
    for (var error in errores){
      for (var i = 0; i < errores[error].length; i++){
        if(angular.isDefined(errores[error][i].$submitted)){
          errores[error][i].$submitted = true;
          this.mostrarErrorRecursivo(errores[error][i].$error);
        } else {
          if(errores[error][i].$setTouched) errores[error][i].$setTouched(true);
        }


      }
    }
  }

  desactivarFormulario(datos, habilitar){
    angular.forEach(datos, (campo) => {
      if(angular.isDefined(campo.fieldGroup)){
        for (var i = 0; i < campo.fieldGroup.length; i++) {
          if(campo.fieldGroup[i].templateOptions){
            campo.fieldGroup[i].templateOptions.disabled = habilitar;
          } else {
            if(campo.templateOptions){
              campo.templateOptions.disabled = habilitar;
            }
          }
        }
      }
    });
  }

  limpiarModelo(datos, modelo){
    var
    elem,
    nuevoModelo = {},
    llaves = {};

    for (var i = 0; i < datos.length; i++) {
      if(datos[i].type == "layout"){
        for (var j=0; j<datos[i].fieldGroup.length; j++) {
          elem = this.obtenerConfiguracionItem(datos[i].fieldGroup[j]);
          this.limpiarElemento(elem, llaves, modelo, nuevoModelo);
        }
      } else {
        elem = this.obtenerConfiguracionItem(datos[i]);
        this.limpiarElemento(elem, llaves, modelo, nuevoModelo);
      }
    }
  }

  limpiarElemento(elemento, llaves, modelo, nuevoModelo){
    if(angular.isUndefined(llaves[elemento.type])){
      llaves[elemento.type] = 1;
      elemento.key = elemento.type + "-0";
    } else {
      elemento.key = elemento.type + "-" + llaves[elemento.type];
      llaves[elemento.type]++;
    }
    if(angular.isDefined(modelo[elemento.key])) {
      nuevoModelo[elemento.key] = angular.copy(modelo[elemento.key]);
    }
  }

  cambiarElemento(elemento, llaves, modelos){
    if(angular.isUndefined(llaves[elemento.type])){
      llaves[elemento.type] = 1;
      elemento.key = elemento.type + "-0";
    } else {
      elemento.key = elemento.type +"-"+ llaves[elemento.type];
      llaves[elemento.type]++;
    }
    if(elemento.type === "radio"){
      elemento.templateOptions.className = "layout layout-wrap";
    }
    if(elemento.type === "datepicker" && angular.isDefined(modelos) && angular.isDefined(modelos[elemento.key])){
      modelos[elemento.key] = new Date(modelos[elemento.key]);
    }
    if(elemento.type === "textarea"){
      elemento.templateOptions.grow = true;
    }

    if(elemento.templateOptions){
      var validation = {messages:{}};
      if(elemento.templateOptions.required){
        validation.messages.required = '"Debe llenar este campo."';
      }
      if( elemento.type==="input"){
        var idx = {
          '([a-zA-Zñáéíóú]+ ?)+': '"El campo debe contener solo palabras separadas por un espacio."',
          '\\d+': '"El campo debe contener solo números."',
          '([a-zA-Z0-9ñáéíóú]+ ?)+': '"El campo debe contener palabras y números separadas por un espacio."',
          '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$': '"El campo debe ser un email válido."',
          '^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$':'"El campo debe ser una hora en formato hh:mm (ej. 08:47)"'
        };
        if(elemento.templateOptions.pattern) validation.messages.pattern = idx[elemento.templateOptions.pattern];
        //le da el efecto de slider al mensaje de error
        validation.show = true;
        elemento.validation = validation;
      }
    }
  }
}

export default Formly;
