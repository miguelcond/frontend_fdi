'use strict';

class DataTableService {
  constructor(NgTableParams, DataService, Util) {
    'ngInject';

    this.NgTableParams = NgTableParams;
    this.DataService = DataService;
    this.Util = Util;
  }

  getParams(url, queryExtra = {}) {
    return new this.NgTableParams({}, {
      getData: (params) => {
        let query = this.getQuery(params.url());
        return this.DataService.list(url, angular.merge(query, queryExtra)).then( response => {
          if (response && response.datos) {
            params.total(response.datos.count);
              this.showEmptyTable(response.datos);
            this.renderLabels(this.getLabels(query, response.datos));
            return response.datos.rows;
          }
        });
      }
    });
  }

  getParamsList(data) {
    if (this.Util.toType(data) !== 'array') {
      return;
    }
    return new this.NgTableParams({}, {
      dataset: data
    });
  }

  getLabels(query, data) {
    return {
      ini: (query.page - 1) * query.limit + 1,
      end: query.page * query.limit < data.count ? query.page * query.limit : data.count,
      total: data.count
    };
  }

  renderLabels(params) {
    setTimeout(() => {
      if (document.querySelector('.ng-table-counts')) {
        document.querySelector('.ng-table-counts').setAttribute('data-content', 'Filas: ');
      }
      if (document.querySelector('.ng-table-pagination')) {
        document.querySelector('.ng-table-pagination').setAttribute('data-content', `${params.ini} al ${params.end} de ${params.total} Registros.`);
      }
    }, 100);
  }

  showEmptyTable(data) {
    const areaPagination = angular.element(document.querySelector('div[ng-table-pagination]'));
    const sinRegistros = `<div class="alert alert-warning empty-table"><i class="fa fa-warning"></i> No existen registros.</div>`;
    if (data.count === 0) {
      setTimeout(() => {
        if (!angular.isUndefined(angular.element(document.querySelector(".empty-table"))[0])) {
          angular.element(document.querySelector('.empty-table')).remove();
        }
        areaPagination.append(sinRegistros);
      }, 100);
    } else {
      angular.element(document.querySelector('.empty-table')).remove();
    }
  }

  getQuery(data) {
    let query = {
      limit: data.count,
      page: data.page,
    };
    let sort = this.getSorting(data);
    if (sort) {
      query.order = sort;
    }
    let filters = this.getFilters(data);
    if (filters.length) {
      if (filters[0].crudtable_search_term) {
        query.filter = filters[0].crudtable_search_term;
      } else {
        for (let i in filters) {
          let key = Object.keys(filters[i])[0];
          query[key] = filters[i][key];
        }
      }
    }
    return query;
  }

  getSorting(params) {
    for (let key in params) {
      if (key.indexOf('sorting') != -1) {
        return (params[key] == 'desc' ? "-" : "") + this.removeCorchete(key, 'sorting');
      }
    }
    return null;
  }

  getFilters(params) {
    let filters = [];
    for (let key in params) {
      if (key.indexOf('filter') != -1) {
        filters.push({[this.removeCorchete(key, 'filter')]: params[key]});
      }
    }
    return filters;
  }

  removeCorchete(string, search) {
    return string.replace(`${search}[`, '').replace(']', '');
  }

}

export default DataTableService;
