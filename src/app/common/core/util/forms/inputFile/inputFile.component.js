'use strict';

import controller from './inputFile.controller';
import template from './inputFile.html';

const InputFileComponent = {
    bindings: {
        onCompleteItem: '=',
        onCompleteAll: '=',
        onAfterAddingFile: '=',
        onBeforeUploadItem: '=',
        label: '@',
        labelSubir: '@',
        textUpload: '@',
        multiple: '@',
        maxfiles: '@',
        types: '@',
        mimeTypes: '@',
        name: '@',
        url: '=',
        uploadCallback: '<',
        form: '<',
        data: '=',
        maxsize: '@',
        fileBase64: '=',
        fileSelect: '='
    },
    controller,
    template
};

export default InputFileComponent;
