'use strict';

class SelectParametroController {

  constructor(DataService, $log, $scope) {
    'ngInject';
    this.DataService = DataService;
    this.$log = $log;
    this.$scope = $scope;
  }

  $onInit() {
    var url = `parametricas/${this.parametro}`;
    if (this.query)
      url += this.query;
    this.DataService.get(url)
    .then((response) => {
      this.items = response.datos.rows;

      //si es opcional aniadimos un valor al combo que sera la bandera para no validar demas eventos
      var isTrueSet = (this.opcional === 'true');
      if (isTrueSet) {
        this.$log.log(' OPCIONAL***: ');
        this.items.unshift({
          codigo: 'NN',
          nombre: 'Ninguno',
          descripcion: 'Sin valor'
        });
      }

      if (this.parametro === 'UNIDAD') {
        this.$log.log(' PERMITE "OTRO"***: ');
        this.items.push({
          codigo: 'OTRO',
          nombre: 'Otro',
          descripcion: 'otro'
        });
      }
    })

    this.$scope.$watch('$ctrl.ngModel', (newValue, oldValue) => {
      if (angular.toJson(newValue) != angular.toJson(oldValue)) {
        this.ngChange({item:newValue});
      }
    })

  }
}


export default SelectParametroController;
