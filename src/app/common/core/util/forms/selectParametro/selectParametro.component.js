'use strict';

import controller from './selectParametro.controller';
import template from './selectParametro.html';
import './selectParametro.scss';

const selectParametroComponent = {
    bindings: {
        ngModel: '=',
        name: '@',
        ngRequired: '<',
        ngDisabled: '<',
        ngChange: '&',
        parametro: '@',
        query: '@',
        selectModel: '=',
        label: '@',
        opcional: '@'
    },
    controller,
    template
};

export default selectParametroComponent;
