'use strict';

class DocumentoIdentidadController {

    constructor($scope, DataService, Datetime, Message, $log) {
        'ngInject';
        this.$scope = $scope;
        this.DataService = DataService;
        this.Datetime = Datetime;
        this.Message = Message;
        this.$log = $log;
    }

    $onInit () {
      this.tiposPersona = [{ nombre: 'Nacional', valor: '1' }, { nombre: 'Extranjero', valor: '2', }];
      this.tipoDocumento = 'Contrastación';
      this.ngModel.tipo_persona = '1';
      // envio de parametro para no verificar con segig
      setTimeout( ()=>{
      // console.log('seguimiento controller', this.proyecto)
      this.ngModel.noVerificar = true;
    },1000)
      this.establecerRangoValido();
      this.initDefault('tipo_documento', 'TD_PASS');
    }

    $onChanges() {
      this.$scope.$watch('$ctrl.ngModel', () => {
        if (this.ngModel) {
          this.cargando_valores = true;
          if (!this.ngModel.tipo_persona) {
            this.ngModel.tipo_persona = '1';
          }
          return;
        }
        if (!this.ngModel) {
          this.ngModel = {};
        }
        this.initDefault('tipo_documento', 'TD_PASS');
      }, true);
    }

    initDefault (key, val) {
      if (!this.ngModel[key]) {
        this.ngModel[key] = val;
      }
    }

    establecerRangoValido() {
      if (this.edadMinima) {
        this.maxDate = this.Datetime.lessYearsToDate(this.edadMinima);
      }
      if (this.edadMaxima) {
        this.minDate = this.Datetime.lessYearsToDate(this.edadMaxima);
      }
    }

    isCI () {
      if (angular.isUndefined(this.ngModel)) {
        return;
      }
      if (angular.isUndefined(this.ngModel.tipo_documento)) {
        return;
      }
      return this.ngModel.tipo_documento === 'TD_CI';
    }

    isFill () {
      if (angular.isUndefined(this.ngModel)) {
        return;
      }
      return this.ngModel.estado=='ACTIVO';
      // return this.ngModel.nombres && this.ngModel.primer_apellido && this.ngModel.tipo_documento==='TD_CI';
    }

    changeType() {
      if (!this.cargando_valores) {
        this.nombreDel();
      }
      this.cargando_valores = false;
    }
    nombreDel() {
      delete this.ngModel.nombres;
      delete this.ngModel.primer_apellido;
      delete this.ngModel.segundo_apellido;
      delete this.ngModel.id_persona;
      delete this.ngModel.estado;
      delete this.ngModel.complemento;
      delete this.ngModel.complemento_visible;
      delete this.ngModel.telefono;
      delete this.ngModel.correo;
      delete this.ngModel.fecha_nacimiento;
    }

    isDisabled() {
      if (angular.isUndefined(this.ngModel)) {
        return;
      }
      // revisar esta parte porque se ejecuta varias veces esta funcion
      return angular.isUndefined(this.ngModel.numero_documento) ;
    }

    search() {
      let ci, datos;
      if (this.ngModel.numero_documento.indexOf('-') !== -1) {
        datos = this.ngModel.numero_documento.split('-');
        ci = datos[0];
      } else {
        ci = this.ngModel.numero_documento;
      }

      let url = `personas/${ci}/${this.ngModel.fecha_nacimiento}`;
      if (this.ngModel.complemento) {
        url += `?complemento=${this.ngModel.complemento}`
      }
      this.DataService.get(url)
      .then( response => {
        this.$log.log(response);
        this.nombreDel();
        if (response) {
          this.ngModel.primer_apellido = response.datos.primer_apellido;
          this.ngModel.segundo_apellido = response.datos.segundo_apellido;
          this.ngModel.nombres = response.datos.nombres;
          this.ngModel.id_persona = response.datos.id_persona;
          delete this.ngModel.telefono;if(response.datos.telefono){this.ngModel.telefono=response.datos.telefono;}
          this.ngModel.correo = response.datos.correo;

          if (typeof(this.callbackSearch)=='function') {
            this.callbackSearch({data:this.ngModel});
          }
          if(response.finalizado) {
            this.ngModel.estado = 'ACTIVO';
          }
        }
      });
    }
    buscarPersona(){
      this.DataService.get(`personas/buscarLocal/?ci=${this.ngModel.numero_documento}`)
      // this.$log.log("fecha nacimiento : ", this.ngModel.fecha_nacimiento)
      .then( response => {
        if(response && response.datos){
          this.ngModel.primer_apellido = response.datos.primer_apellido;
          this.ngModel.segundo_apellido = response.datos.segundo_apellido;
          this.ngModel.fecha_nacimiento = response.datos.fecha_nacimiento; //tipo string
          this.ngModel.nombres = response.datos.nombres;
          delete this.ngModel.telefono;if(response.datos.telefono){this.ngModel.telefono=response.datos.telefono;}
          this.ngModel.correo = response.datos.correo;
        }else{
          delete this.ngModel.primer_apellido;
          delete this.ngModel.segundo_apellido;
          delete this.ngModel.fecha_nacimiento; // no entiendo porque no se borra el valor fechanacimiento
          delete this.ngModel.nombres;
          delete this.ngModel.telefono;
          delete this.ngModel.correo;
          // this.nombreDel();
        }
      });
    }

    contrastar() {
      let campo = {
        ci: this.ngModel.numero_documento,
        complemento: this.ngModel.complemento,
        nombres: this.ngModel.nombres,
        primer_apellido: this.ngModel.primer_apellido,
        segundo_apellido: this.ngModel.segundo_apellido,
        fecha: this.ngModel.fecha_nacimiento,
      }

      // this.DataService.get(`personas/${ci}/${this.ngModel.fecha_nacimiento}?tipo=${1}&campo=${JSON.stringify(campo)}`)
      // Enviar datos al backend
      this.DataService.get(`personas/contrastacion/?tipo=${this.ngModel.tipo_persona}&campo=${JSON.stringify(campo)}`)
      .then( response => {
        this.$log.log(response);
        // this.nombreDel();
        if (response) {
          this.ngModel.primer_apellido = response.datos.primer_apellido;
          this.ngModel.segundo_apellido = response.datos.segundo_apellido;
          this.ngModel.nombres = response.datos.nombres;
          this.ngModel.id_persona = response.datos.id_persona;
          delete this.ngModel.telefono;if(response.datos.telefono){this.ngModel.telefono=response.datos.telefono;}
          this.ngModel.correo = response.datos.correo;

          if (typeof(this.callbackSearch)=='function') {
            this.callbackSearch({data:this.ngModel});
          }
          if(response.finalizado) {
            this.ngModel.estado = 'ACTIVO';
          }
        }
      });
    }
}

export default DocumentoIdentidadController;
