'use strict';

class AllowClearClass {
  constructor() {
    'ngInject';
    this.restrict = 'A';
  }

  link($scope, $elem, $attr) {
    if ($attr.allowClear) {
      let find = angular.element($elem[0].querySelector('.glyphicon-remove'));
      find.removeClass('glyphicon glyphicon-remove');
      find.addClass('fa fa-remove');
    }
  }
}

export default AllowClearClass;
