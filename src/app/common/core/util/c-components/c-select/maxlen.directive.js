'use strict';

class MaxLen {
  constructor() {
    this.restrict = 'A';
  }

  link($scope, $elem, $attr) {
    const uiSelectSearch = angular.element($elem[0].querySelector('.ui-select-search'));
      uiSelectSearch.attr("maxlength", $attr.maxlen);
  }
}

export default MaxLen;
