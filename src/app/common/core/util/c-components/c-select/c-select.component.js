'use strict';

import controller from './c-select.controller';
//import './ui-select.scss';
const CSelectComponent = {
  bindings: {
    name: '@',
    label: '@',
    title: '@',
    placeholder: '@',
    ngModel: '=',
    ngChange: '&',
    onSelect: '&',
    ngDisabled: '=',
    ngRequired: '=',
    options: '<',
    field: '@',
    keyValue: '@'
  },
  require: {
    form: '^form',
  },
  template: `
      <div class="md-form-group md-select-group" md-event-label>
        <ui-select ng-if="!$ctrl.Util.isMobile.any() && !$ctrl.keyValue" name="{{$ctrl.name}}" on-select="$ctrl.onSelect({item:$item})" ng-change="$ctrl.ngChange()" ng-model="$ctrl.ngModel" theme="bootstrap"
        class="md-select" ng-required="$ctrl.ngRequired" ng-disabled="$ctrl.ngDisabled" maxlen="20">
          <ui-select-match placeholder="{{$ctrl.placeholder}}" allow-clear="!$ctrl.ngRequired" allow-clear-class>
            {{$select.selected[$ctrl.field]}}
          </ui-select-match>
          <ui-select-choices repeat="item in $ctrl.options | filter: $select.search">
            <span ng-bind-html="item[$ctrl.field] | highlight: $select.search"></span>
          </ui-select-choices>
          <ui-select-no-choice>
            <small> No se encontraron coincidencias </small>
          </ui-select-no-choice>
        </ui-select>

        <ui-select ng-if="!$ctrl.Util.isMobile.any() && $ctrl.keyValue" name="{{$ctrl.name}}" on-select="$ctrl.onSelect({item:$item})" ng-change="$ctrl.ngChange()" ng-model="$ctrl.ngModel" theme="bootstrap"
        class="md-select" ng-required="$ctrl.ngRequired" ng-disabled="$ctrl.ngDisabled" maxlen="20">
          <ui-select-match placeholder="{{$ctrl.placeholder}}" allow-clear="!$ctrl.ngRequired" allow-clear-class>
            {{$select.selected[$ctrl.field]}}
          </ui-select-match>
          <ui-select-choices repeat="item[$ctrl.keyValue] as item in $ctrl.options | filter: $select.search">
            <span ng-bind-html="item[$ctrl.field] | highlight: $select.search"></span>
          </ui-select-choices>
          <ui-select-no-choice>
            <small> No se encontraron coincidencias </small>
          </ui-select-no-choice>
        </ui-select>

        <select class="select-mobile" ng-if="!!$ctrl.Util.isMobile.any()" name="{{$ctrl.name}}" ng-model="$ctrl.ngModel" ng-options="item[$ctrl.field] for item in $ctrl.options"
        ng-change="$ctrl.ngChange()" ng-required="$ctrl.ngRequired" ng-disabled="$ctrl.ngDisabled"></select>

        <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="{{!!$ctrl.Util.isMobile.any()? 'label-mobile' : 'md-control-label'}}" ng-class="{'requerido':$ctrl.ngRequired, 'md-control-label': $ctrl.ngModel}">{{$ctrl.label}}</label>

        <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
        <div ng-messages="$ctrl.form[$ctrl.name].$error" ap-error ng-if="$ctrl.form[$ctrl.name].$touched" role="alert">
          <div ng-message="required"> El campo es requerido. </div>
        </div>
      </div>
    `,
  controller
};

export default CSelectComponent;
