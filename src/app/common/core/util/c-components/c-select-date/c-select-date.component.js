'use strict';

import controller from './c-select-date.controller';
import template from './c-select-date.html';

const CSelectDateComponent = {
    template,
    controller,
    bindings: {
        model: '=',
        name: '@',
        label: '@',
        title: '@?',
        options: '=',
        field: '@',
        isDisabled: '=?',
        ngRequired: '=?',
        onChange: '&',
        minDate: '=?',
        maxDate: '=?',
        abreviacion: '@'
    },
    require: {
        form: '^form'
    }
};

export default CSelectDateComponent;
