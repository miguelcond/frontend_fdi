'use strict';

class CSelectDateController {
    constructor($scope, $timeout, Util) {
      'ngInject';
      if (angular.isUndefined(this.isDisabled)) {
        this.isDisabled = false;
      }
      if (angular.isUndefined(this.ngRequired)) this.ngRequired = false;

      this.date = new Date();
      this.listMonths = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
      this.numDays = { '1': 31, '2': 28, '3': 31, '4': 30, '5': 31, '6': 30, '7': 31, '8': 31, '9': 30, '10': 31, '11': 30, '12': 31 };
      this.$scope = $scope;
      this.$timeout = $timeout;
      this.Util = Util;
    }

    $onInit() {
      this.$scope.$on('resetForm', () => {
        this.day = '';
        this.month = '';
        this.year = '';
      });
      if (angular.isUndefined(this.abreviacion)) this.abreviacion= this.label;
      this.years = this.initYears();
      this.months = this.initMonths();
      this.days = this.initDays();
      if(this.maxDate) {
        this.$scope.$watch(()=>{ return this.maxDate; }, ()=>{
          if(this.model) {
            this.years = this.initYears();
            if(this.year && this.maxDate && this.year.value>this.maxDate.getFullYear()) {
              this.year = this.years[0];
            }
          }
        });
      }
      if(this.minDate) {
        this.$scope.$watch(()=>{ return this.minDate; }, ()=>{
          if(this.model) {
            this.years = this.initYears();
            if(this.year && this.minDate && this.year.value<this.minDate.getFullYear()) {
              this.year = this.years[this.years.length-1];
            }
          }
        });
      }
    }

    _verifMinMonth() {
      if(!this.minDate) {
        this.minDate = new Date();
        this.minDate.setFullYear(this.minDate.getFullYear()-100);
      }
      return this.minDate && this.year && this.minDate.getFullYear()==this.year.value && this.month && this.minDate.getMonth()+1==this.month.value;
    }
    _verifMaxMonth() {
      if(!this.maxDate) {
        this.maxDate = new Date();
      }
      return this.maxDate && this.year && this.maxDate.getFullYear()==this.year.value && this.month && this.maxDate.getMonth()+1==this.month.value;
    }
    initDays(lastDay) {
      var days = [];
      var last = lastDay || 31;
      for (var i = 1; i <= last; i++) {
        days.push({
          disabled: (this._verifMinMonth() && i<this.maxDate.getDate()) || (this._verifMaxMonth() && i>this.maxDate.getDate()),
          text: i,
          value: i
        });
      }
      if(this.day) {
        this.day = days.find((d)=>{ return d.value==this.day.value; });
      }
      return days;
    }

    _verifMinYear() {
      if(!this.minDate) {
        this.minDate = new Date();
        this.minDate.setFullYear(this.minDate.getFullYear()-100);
      }
      return this.minDate && this.year && this.minDate.getFullYear()==this.year.value;
    }
    _verifMaxYear() {
      if(!this.maxDate) {
        this.maxDate = new Date();
      }
      return this.maxDate && this.year && this.maxDate.getFullYear()==this.year.value;
    }
    initMonths() {
      var months = [];
      for (var i = 0; i < 12; i++) {
        months.push({
          disabled: (this._verifMinYear() && i<this.minDate.getMonth()) || (this._verifMaxYear() && i>this.maxDate.getMonth()),
          text: this.listMonths[i],
          value: i + 1
        });
      }
      if(this.month) {
        this.month = months.find((m)=>{ return m.value==this.month.value; });
      }
      return months;
    }

    initYears() {
      var maxYear = this.date.getFullYear(),
      minYear = this.date.getFullYear() - 100,
      years = [];
      if(this.maxDate && this.maxDate.getFullYear()) {
        maxYear = this.maxDate.getFullYear();
      }
      if(this.minDate && this.minDate.getFullYear()) {
        minYear = this.minDate.getFullYear();
      }

      for (var i = maxYear; i >= minYear; i--) {
        years.push({
          text: i,
          value: i
        });
      }
      return years;
    }

    setDate() {
      this.$timeout(()=>{
        if(this.onChange && angular.isFunction(this.onChange)) {
          this.onChange();
        }

        var day = this.verificarValor(this.day),
        month = this.verificarValor(this.month),
        year = this.verificarValor(this.year);

        if(this._verifMaxYear() && month>this.maxDate.getMonth()+1) {
          this.month = this.months[this.maxDate.getMonth()];
          month = this.verificarValor(this.month);
        }
        if(this._verifMinYear() && month<this.minDate.getMonth()+1) {
          this.month = this.months[this.minDate.getMonth()];
          month = this.verificarValor(this.month);
        }

        // Verificar año biciesto
        var nDays;
        nDays = this.numDays[month];

        if (nDays == 28 && (year % 4) == 0) {
          ++nDays;
        }
        if(day>nDays) {
          this.day = this.days[nDays-1];
          day = this.verificarValor(this.day);
        }
        if(this._verifMaxMonth() && day>this.maxDate.getDate()) {
          this.day = this.days[this.maxDate.getDate()-1];
          day = this.verificarValor(this.day);
        }
        if(this._verifMinMonth() && day<this.minDate.getDate()) {
          this.day = this.days[this.minDate.getDate()-1];
          day = this.verificarValor(this.day);
        }
        if (day !== '00' && month !== '00' && year !== '00') {
          this.model = new Date(month + '/' + day + '/' + year);
        } else {
          this.model = null;
        }

        this.months = this.initMonths();
        this.days = this.initDays(nDays);
      }, 100);
    }

    verificarValor(item) {
      if (!angular.isUndefined(item) && item !== null) {
        return item.value;
      } else {
        return '00';
      }
    }

    inputDate(data, lista, model) {
      if (!data) {
        return;
      }
      for (var i = 0; i < lista.length; i++) {
        if (String(lista[i].text).toLowerCase() === data.toLowerCase()) {
          this[model] = lista[i];
          this.setDate();
          return;
        }
      }
    }

}

export default CSelectDateController;
