'use strict';

import controller from './c-date.controller';

const CDateComponent = {
  bindings:{
    name: '@',
    label: '@',
    ngModel: '=',
    ngDisabled: '=',
    ngRequired: '=',
    minDate: '=?',
    mensajeMinDate: '@?',
    maxDate: '=?',
    mensajeMaxDate: '@?',
    ngChange: '&',
    initDate: '<',
    minDateKey: '@',
    maxDateKey: '@'
  },
  require:{
    form: '^form'
  },

  template:`
      <div class="md-form-group">
        <label class="md-control-label" for="{{$ctrl.name}}" ng-class="{'requerido':$ctrl.ngRequired}" style="top:-1rem;font-size:11px;">{{$ctrl.label}}</label>
        <div class="input-group">
          <input type="text"
                 id="{{$ctrl.name}}"
                 name="{{$ctrl.name}}"
                 class=""
                 ui-mask="99/99/9999"
                 maxlength="10"
                 ng-disabled="$ctrl.ngDisabled"
                 ng-required="$ctrl.ngRequired"
                 model-view-value="true"
                 ng-model="$ctrl.date"
                 init-date="$ctrl.initDate"
                 ng-model-options="{ getterSetter: true }"
                 uib-datepicker-popup="dd/MM/yyyy"
                 is-open="$ctrl.opened"
                 datepicker-options="$ctrl.options"
                 ng-click="$ctrl.open($event)"
                 ng-change="$ctrl.actualizarValor(); $ctrl.ngChange({data:$ctrl.date})"
                 ui-validate="{minDate: '$ctrl.verifyMinDate($value)', maxDate: '$ctrl.verifyMaxDate($value)'}"
                 min-date="$ctrl.minDate"
                 max-date="$ctrl.maxDate"
                 >
          <button type="button" class="btn btn-link p-0 m-0" ng-click="$ctrl.open($event)"><i class="fa fa-calendar"></i></button>
        </div>
        <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
        <div ng-messages="$ctrl.form[$ctrl.name].$error" role="alert" ap-error ng-if="$ctrl.form[$ctrl.name].$touched">
          <div ng-message="required"> El campo es requerido. </div>
          <div ng-message="date"> Introduzca una fecha válida (dd/mm/aaaa). </div>
          <div ng-message="minDate" ng-if="!$ctrl.mensajeMinDate"> La fecha debe ser mayor o igual a {{$ctrl.options.minDate | date:'dd/MM/yyyy' }}. </div>
          <div ng-message="minDate" ng-if="$ctrl.mensajeMinDate"> {{$ctrl.mensajeMinDate}} </div>
          <div ng-message="maxDate" ng-if="!$ctrl.mensajeMaxDate"> La fecha debe ser menor o igual a {{$ctrl.options.maxDate | date:'dd/MM/yyyy'}}. </div>
          <div ng-message="maxDate" ng-if="$ctrl.mensajeMaxDate"> {{$ctrl.mensajeMaxDate}} </div>
        </div>
      </div>`,
  controller
};

export default CDateComponent;
