'use strict';

class CDateController {
  constructor(Datetime, $scope, _){
    'ngInject';
    this.options= {};
    this.Datetime = Datetime;
    this._ = _;
    this.$scope = $scope;
  }
  $onInit(){
    this.formatoValor = 'YYYY-MM-dd';
    this.options = {
      maxDate: this.maxDate,
      minDate: this.minDate,
      formatYear: 'yyyy',
      showWeeks: false
    };
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.ngModel', () => {
      if (this._.isEmpty(this.ngModel)) {
        return;
      }
      this.inicializarModel();
    });
  }

  inicializarModel() {
    this.verificarFormatoModel();
    this.date = this.Datetime.toDate(angular.copy(this.ngModel));
    this.actualizarValor();
  }

  verificarFormatoModel() {
    if (this.ngModel.length === 10) {
      let valor = this.ngModel.split('-');
      if(this.formatoValor=='dd-MM-YYYY') {
        if (valor.length === 3 && valor[0].length === 2 && valor[1].length === 2 && valor[2].length === 4) {
          this.ngModel = `${valor[2]}-${valor[1]}-${valor[0]}T04:00:00.000Z`;
        }
      }
      if(this.formatoValor=='YYYY-MM-dd') {
        if (valor.length === 3 && valor[0].length === 4 && valor[1].length === 2 && valor[2].length === 2) {
          this.ngModel = `${valor[0]}-${valor[1]}-${valor[2]}T04:00:00.000Z`;
        }
      }
      if(this.formatoValor=='dd/MM/YYYY') {
        valor = this.ngModel.split('/');
        if (valor.length === 3 && valor[0].length === 2 && valor[1].length === 2 && valor[2].length === 4) {
          this.ngModel = `${valor[2]}-${valor[1]}-${valor[0]}T04:00:00.000Z`;
        }
      }
    }
  }

  actualizarValor() {
    this.ngModel = this.formatearFecha(angular.copy(this.date));
  }

  formatearFecha() {
    if (this.ngModel) {
      if (this.ngModel.indexOf('-')>=1) {
        if (this.ngModel.split('-')[0].length==2) this.formatoValor = 'dd-MM-YYYY';
        if (this.ngModel.split('-')[0].length==4) this.formatoValor = 'YYYY-MM-dd';
      }
      if (this.ngModel.indexOf('/')>=1) {
        if (this.ngModel.split('/')[0].length==2) this.formatoValor = 'dd/MM/YYYY';
        if (this.ngModel.split('/')[0].length==4) this.formatoValor = 'YYYY/MM/dd';
      }
    }
    let f = this.Datetime.standar(this.date, this.formatoValor).toISOString().substr(0,10);
    return f
  }

  open (){
    this.opened = !this.opened;
    if(this.options) {
      if(this.maxDateKey) {
        const MAX_DATE = angular.element(document.getElementById(this.maxDateKey)).val();
        if (MAX_DATE) {
          this.options.maxDate = this.Datetime.convert(MAX_DATE);
        }
      }
      if(this.minDateKey) {
        const MIN_DATE = angular.element(document.getElementById(this.minDateKey)).val();
        if (MIN_DATE) {
          this.options.minDate = this.Datetime.convert(MIN_DATE);
        }
      }
    }
  }

  verifyMaxDate(value) {
    if (!angular.isUndefined(this.options.maxDate)) {
      this.options.maxDate = new Date(this.options.maxDate);
      var dateSelected = new Date(value),
      maxDate = new Date(this.options.maxDate.getFullYear(), this.options.maxDate.getMonth(), this.options.maxDate.getDate() + 1);
      return dateSelected < maxDate;
    }
    return true;
  }

  verifyMinDate(value) {
    if (!angular.isUndefined(this.options.minDate)) {
      this.options.minDate = new Date(this.options.minDate);
      var dateSelected = new Date(value),
      minDate = new Date(this.options.minDate.getFullYear(), this.options.minDate.getMonth(), this.options.minDate.getDate() - 1);
      return dateSelected > minDate;
    }
    return true;
  }
}

export default CDateController;
