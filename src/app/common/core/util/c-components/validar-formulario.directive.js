'use strict';

class ValidarFormulario {
  constructor() {
    'ngInject';
    this.restrict = 'A';
    this.controller = ValidarFormularioController;
    this.scope = {
      form: '=validarFormulario',
      nombreForm: '@nombreFormulario',
      method: '&ngClick',
      methodValidation: '&validacionParalela'
    }
  }

  link($scope, $elem, $attr, $ctrl) {
    $elem.bind('click', (event) => {
      if (!$scope.form && $scope.nombreForm) {
        $scope.form = $scope.nombreForm;
      }
      if ($ctrl.Util.isEmpty($scope.form)) {
        return;
      }
      event.stopImmediatePropagation();
      if (typeof $scope.form === 'object') {
        this.validarFormulario($scope, $ctrl, $scope.form, event);
      } else if (typeof $scope.form === 'string') {
        const FORM = this.buscarFormulario($scope);
        if (!angular.isUndefined(FORM)) {
          this.validarFormulario($scope, $ctrl, FORM[$scope.form]);
        }
      }
      $scope.$apply();
    });
  }

  validarFormulario($scope, $ctrl, form) {    
    if ($ctrl.Util.isValidFormFields(form)) {
      $scope.method();
    } else {
      $scope.methodValidation();
    }
  }

  buscarFormulario($scope) {
    return angular.element(document.querySelector('[name=' + $scope.form + ']')).scope();
  }

}

class ValidarFormularioController {
  constructor(Util) {
    'ngInject';
    this.Util = Util;
  }
}

export default ValidarFormulario;
