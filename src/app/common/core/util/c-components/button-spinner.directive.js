'use strict';

class ButtonSpinner {
  constructor() {
    this.restrict = 'A';
    this.transclude = true;
    this.scope = {
        spinning: '=buttonSpinner',
        spinningIcon: '@?',
        buttonPrepend: '@?',
        buttonAppend: '@?'
    };
    this.template = `<span ng-if="!!buttonPrepend" ng-hide="spinning"><i class="{{ buttonPrepend }}"></i>&nbsp;</span>
                     <span ng-if="!!buttonPrepend" ng-show="spinning"><i class="{{ !!spinningIcon ? spinningIcon : 'fa fa-spinner fa-spin' }}"></i>&nbsp;</span>
                     <ng-transclude></ng-transclude>
                     <span ng-if="!!buttonAppend" ng-hide="spinning">&nbsp;<i class="{{ buttonAppend }}"></i></span>
                     <span ng-if="!buttonPrepend" ng-show="spinning">&nbsp;<i class="{{ !!spinningIcon ? spinningIcon : 'fa fa-spinner fa-spin' }}"></i></span>`;
  }

}

export default ButtonSpinner;
