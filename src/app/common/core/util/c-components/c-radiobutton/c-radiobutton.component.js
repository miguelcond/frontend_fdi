'use strict';

import controller from './c-radiobutton.controller';

const CRadioButtonComponent = {
  bindings: {
    name: '@',
    label: '@',
    ngModel: '=',
    ngDisabled: '<',
    ngRequired: '<',
    options: '<',
    horizontal: '<',
    ngChange:'&?',
    field: '@',
    value: '@?'
  },
  require: {
          form: '^form',
  },
  template: `
  <div class="md-form-group">
    <div ng-class="{'row':$ctrl.horizontal}">
      <div ng-repeat="option in $ctrl.options" class="ml-3">
        <label class="custom-control custom-radio">
          <input type="radio" name="{{$ctrl.name}}" ng-model="$ctrl.ngModel"
                 value="{{option[$ctrl.value] || option.codigo}}" class="custom-control-input"
                 ng-change="$ctrl.controlCambioValor(option[$ctrl.value] || option.codigo)"
                 ng-required="$ctrl.ngRequired" ng-disabled="$ctrl.ngDisabled">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">{{option[$ctrl.field]}}</span>
        </label>
      </div>
    </div>
    <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="md-label-radio" ng-class="{'requerido':$ctrl.ngRequired, '{{$ctrl.classs}}' : $ctrl.classs}">
    {{$ctrl.label}}
    </label>
    <div ng-messages="$ctrl.form[$ctrl.name].$error" role="alert" ap-error ng-if="$ctrl.form[$ctrl.name].$touched">
      <div ng-message="required"> El campo es requerido. </div>
    </div>
  </div>
  `,
  controller
};

export default CRadioButtonComponent;
