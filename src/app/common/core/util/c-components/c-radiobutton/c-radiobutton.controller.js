'use strict';

class CRadioButtonController{
  constructor(){
    'ngInject';
  }
  $onInit(){

  }
  controlCambioValor(valor) {
    if(this.ngChange && typeof this.ngChange === 'function'){
      this.ngChange({data: valor});
    }
  }
}
export default CRadioButtonController;
