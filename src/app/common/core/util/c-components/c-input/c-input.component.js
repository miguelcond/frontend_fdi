'use strict';

import controller from './c-input.controller';
const CInputComponent = {
  bindings: {
    name: '@',
    label: '@',
    title: '@?',
    type: '@?',
    ngModel: '=',
    ngDisabled: '=',
    ngRequired: '=',
    ngValidation: '@',
    cUiValidate: '@?',
    cUiValidateWatch: '@?',
    ngMinlength: '@?',
    ngMaxlength: '@?',
    maxlength: '@?',
    class: '@?',
    ngDirectives: '@?',
    ngBlur: '&?'
  },
  require: {
    form: '^form',
  },
  template: `
     <div class="md-form-group" md-event-label>
        <input name="{{$ctrl.name}}" id="{{$ctrl.name}}"
               ng-required="$ctrl.ngRequired"
               ng-disabled="$ctrl.ngDisabled"
               ng-readonly="$ctrl.ngReadonly"
               ng-model="$ctrl.ngModel"
               ng-pattern="$ctrl.validation"
               ng-minlength="$ctrl.ngMinlength"
               ng-maxlength="$ctrl.ngMaxlength"
               maxlength="{{$ctrl.maxlength}}"
               ui-validate ="{{$ctrl.cUiValidate}}"
               ui-validate-watch ="{{$ctrl.cUiValidateWatch}}"
               type="{{ $ctrl.type || 'text' }}"
               class="{{$ctrl.class}}"
               ng-blur="$ctrl.ngBlur()"
               autocapitalize="off">
        <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="md-control-label" ng-class="{'requerido':$ctrl.ngRequired}">{{ $ctrl.label }}</label>
        <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
        <div ng-messages="$ctrl.form[$ctrl.name].$error" role="alert" ap-error ng-if="$ctrl.form[$ctrl.name].$touched">
            <div ng-message="required"> El campo es requerido. </div>
            <div ng-message="pattern"> {{$ctrl.messageErrorPattern}} </div>
            <div ng-message="minlength"> El contenido es muy corto. </div>
            <div ng-message="maxlength"> El contenido es muy largo. </div>
            <div ng-message="parse"> {{$ctrl.messageErrorParse}} </div>
            <div ng-message="contrasena"> Las contraseñas son distintas. </div>
        </div>
    </div>`,
  controller
};

export default CInputComponent;
