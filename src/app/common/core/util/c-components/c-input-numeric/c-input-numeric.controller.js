'use strict';

class CInputNumericController {
    constructor(Util, $scope, $compile, $element) {
      'ngInject';
      this.Util = Util;
      this.$scope = $scope;
      this.$compile = $compile;
      this.$element = $element;
    }

    $onInit() {
      this.verificarAtributosAdicionales();
      if (this.Util.isEmpty(this.ngModel)) {
        this.ngModel = undefined;
      }
      
      this.$scope.$watch('$ctrl.ngModel', () => {
        if (this.ngDecimals == 0 && this.ngModel && this.ngModel % 1 !== 0) {
          if (this.ngModel % 1 >= 0.5) this.ngModel = Math.floor(this.ngModel) + 1;
          else this.ngModel = Math.floor(this.ngModel);
        }
      });
    }

    verificarAtributosAdicionales() {
      if (this.ngDirectives) {
        const directivas = this.Util.limpiarEspaciosDobles(this.ngDirectives).split(' ');
        for (let i = 0; i < directivas.length; i++) {
          this.$compile(this.$element.find('input').attr(directivas[i], ''))(this.$scope);
        }
      }
    }
}
export default CInputNumericController;
