'use strict';

import controller from './c-input-numeric.controller';
const CInputNumericComponent = {
  bindings: {
      name: '@',
      label: '@',
      ngModel: '=',
      ngDisabled: '=?',
      ngRequired: '=?',
      ngBlur: '&?',
      placeholder: '@?',
      ngMinvalue: '@?',
      ngMaxvalue: '@?',
      ngDecimals: '@?',
      ngFormatting: '<?',
      class: '@?',
      ngDirectives: '@?',
      ngDecimalsDinamic: '<',
      ngMaxDecimals: '@?',
      ngNotNumber: '<?',
      ngNumberDefault: '<?'
  },
  require: {
    form: '^form',
  },
  template: `
     <div class="md-form-group" md-event-label>
      <input name="{{$ctrl.name}}"
             id="{{$ctrl.name}}"
             type="text"
             ng-model="$ctrl.ngModel"
             ng-required="$ctrl.ngRequired"
             ng-disabled="$ctrl.ngDisabled"
             ng-readonly="$ctrl.ngReadonly"
             ng-maxlength="$ctrl.ngMaxlength"
             ng-placeholder="$ctrl.placeholder"
             ng-blur="$ctrl.ngBlur()"
             numerico
             min="$ctrl.ngMinvalue"
             max="$ctrl.ngMaxvalue"
             decimals="$ctrl.ngDecimals"
             decimals-dinamic="$ctrl.ngDecimalsDinamic"
             maxdecimals="$ctrl.ngMaxDecimals"
             formatting="$ctrl.formatting"
             class="{{$ctrl.class}}"
             not-number="$ctrl.ngNotNumber"
             number-default="$ctrl.ngNumberDefault"
        >
        <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="md-control-label" ng-class="{'requerido':$ctrl.ngRequired}">{{ $ctrl.label }}</label>
        <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
      <div ng-messages="$ctrl.form[$ctrl.name].$error" role="alert" ap-error ng-if="$ctrl.form[$ctrl.name].$touched">
        <div ng-message="required"> El campo es requerido. </div>
      </div>
    </div>`,
  controller

};

export default CInputNumericComponent;
