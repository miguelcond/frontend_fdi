'use strict';

class ValidacionFormsService {
  constructor(Modal) {
    'ngInject';
    this.Modal = Modal;
  }

  $onInit() {

  }

  esFormularioValido(form) {
    return form.$valid;
  }

  validarFormulario(form, mostrarModal) {
    this.camposInvalidos = {};
    if (this.esFormularioValido(form)) {
      return;
    } else {
      angular.forEach(form.$error, (campos) => {
        angular.forEach(campos, (campo) => {
          if (campo.$name) {
            let elemento = angular.element(document.querySelector('[name=' + campo.$name + ']'));
            let label = this.obtenerLabel(campo.$name, elemento);
            if (angular.isUndefined(this.camposInvalidos[campo.$name])) {
              if (label) {
                this.camposInvalidos[campo.$name] = label;
              }
            }
          }
          campo.$touched = true;
        })
      });
    }
    if (mostrarModal) {
      this.mostrarCamposInvalidos(this.camposInvalidos);
    } else {
      return this.camposInvalidos;
    }
  }

  obtenerLabel(nombreCampo, elemento) {
   let elementoPadre = elemento.parent();
   let i = 0;
   while (i < 10) {
     let elemPadre = elementoPadre.find('label');
     if (elemPadre.length > 0) {
       for (let j = 0; j < elemPadre.length; j++) {
         if (elemPadre[j].htmlFor === nombreCampo) {
           if (elemPadre[j].title !== '') {
             return elemPadre[j].title;
           }
           return elemPadre[j].innerText;
         }
       }
     }
     elementoPadre = elementoPadre.parent();
     i++;
   }
   return null;
  }

  mostrarCamposInvalidos(camposInvalidos, titulo) {
    this.Modal.alert(this.getMensajeCamposIncompletos(camposInvalidos), null, titulo || 'Campos incompletos', null, 'md');
  }

  getMensajeCamposIncompletos(camposInvalidos) {
    let mensaje = '<p>Verifique, corrija y/o llene los siguientes campos:</p>';
    mensaje += '<ul>';
    angular.forEach(camposInvalidos || this.camposInvalidos, (error) => {
      mensaje += '<li>' + error + '</li>';
    });
    return mensaje += '</ul>' ;
  }

}

export default ValidacionFormsService;
