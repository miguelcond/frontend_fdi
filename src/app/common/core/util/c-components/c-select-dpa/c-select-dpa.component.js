'use strict';

import controller from './c-select-dpa.controller';
import template from './c-select-dpa.html';

const CSelectDPAComponent = {
  bindings: {
          ngModel: '=',
          ngRequired: '<?',
          ngDisabled: '<?',
          departamentos: '=?',
          provincias: '=?',
          municipios: '=?',
          selectMunicipio: '&?',
          disableProvincia: '<?',
          horizontal: '=?'
  },
  require: {
    form: '^form',
  },
  template,
  controller
};

export default CSelectDPAComponent;
