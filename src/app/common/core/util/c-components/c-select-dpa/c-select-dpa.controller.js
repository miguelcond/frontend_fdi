'use strict';

class CSelectDPAController {
  constructor(DataService, Util) {
    'ngInject';
    this.DataService = DataService;
    this.Util = Util;
    this.departamentos = [];
    this.provincias = [];
    this.municipios = [];
  }

  $onInit() {
    this.prepararListasDeVisualizacion();
    this.getDatosDpa();
  }

  getDatosDpa() {
    if (!this.ngDisabled) {
      this.getDepartamentos();
      if (!this.Util.isEmpty(this.ngModel)) {
        this.getProvincias(this.ngModel.departamento, false);
        this.getMunicipios(this.ngModel.provincia, false);
      }
    }
  }

  /**
  * prepararListasDeVisualizacion - En caso de que se los datos solo serán visualizados,
  * no se obtiene datos de provincias y municipios, entonces para los mobiles se prepara
  * las listas que contienen los datos del modelo
  */
  prepararListasDeVisualizacion() {
    if (!this.Util.isEmpty(this.ngModel) &&this.ngDisabled) {
      this.provincias.push(this.ngModel.provincia);
      this.municipios.push(this.ngModel.municipio);
    }
  }

  $doCheck() {
    if(this.ngModel) {
      this.ngModel.departamento = this.getElementoDeLista(this.ngModel.departamento, this.departamentos);
      this.ngModel.provincia = this.getElementoDeLista(this.ngModel.provincia, this.provincias);
      this.ngModel.municipio = this.getElementoDeLista(this.ngModel.municipio, this.municipios);
    }
  }

  getElementoDeLista(elemento, lista) {
    if (!elemento) {
      return null;
    }
    for (var i = 0; i < lista.length; i++) {
      if (lista[i].codigo === elemento.codigo) {
        return lista[i];
      }
    }
    return elemento;
  }

  /**
  * getDepartamentos - Obtiene departamentos, si ngModel no es undefined significa que existe un valor ya asignado,
  * entonces se envía en query el código del departamento ya cargado. Setea los departamentos como habilitados.
  */
  getDepartamentos() {
    let query = !this.Util.isEmpty(this.ngModel) && this.ngDisabled? '?codigo=' + this.ngModel.departamento.codigo : '';
    this.DataService.get(`departamentos/${query}`).then((response) => {
      this.departamentos = response;
      if (this.Util.isEmpty(this.ngModel)) {
        this.ngModel = {};
        this.establecerDatosPorDefecto(response, 'departamento');
      }
    });
  }

  getProvincias(departamento, reset) {
    if (reset) {
      this.resetearValores();
    }
    this.DataService.get(`provincias/${departamento.codigo}`).then(response => {
      this.provincias = response;
      this.establecerDatosPorDefecto(response, 'provincia');
    });
    this.getMunicipios(departamento, reset);
  }

  getMunicipios(provincia, reset) {
    if (reset) {
      this.ngModel.municipio = null;
    }
    this.DataService.get(`municipios/${provincia.codigo}`).then(response => {
      this.municipios = response;
      this.establecerDatosPorDefecto(response, 'municipio');
    });
  }

  resetearValores() {
    this.ngModel.provincia = null;
    this.ngModel.municipio = null;
    this.municipios = [];
  }

  setMunicipio(municipio) {
    if (typeof(selectMunicipio)=='function') {
      this.selectMunicipio({item: municipio});
    }
    if (this.disableProvincia) {
      this.ngModel.provincia = this.provincias.find((prov)=>{ return prov.codigo==municipio.codigo.substring(0,4); });
    } else {
      if (!this.ngModel.provincia) {
        this.ngModel.provincia = this.provincias.find((prov)=>{ return prov.codigo==municipio.codigo.substring(0,4); });
        this.municipios = this.municipios.filter((muni)=>{ return muni.codigo.substring(0,4)==municipio.codigo.substring(0,4); });
      }
    }
  }

  /**
  * establecerDatosPorDefecto - Si en la lista de opciones solo hay un elemento,éste es asignado
  * a la variable que corresponde como dato por defecto. Además se asigna sólo si ngModel es indefinido.
  *
  * @param  {array} lista    Lista de opciones
  * @param  {string} elemento elemento a asignar el valor
  * @return {function}          Método que se ejecutará de acuerdo al elemento seleccionado
  */
  establecerDatosPorDefecto(lista, elemento) {
    if ( lista.length !== 1 ) {
      return;
    }
    this.ngModel[elemento] = lista[0];
    switch (elemento) {
      case 'departamento':
      return this.getProvincias(lista[0]);
      case 'provincia':
      return this.getMunicipios(lista[0]);
    }
    return;
  }

}
export default CSelectDPAController;
