'use strict';

import controller from './c-input-money.controller';
const CInputMoneyComponent = {
  bindings: {
      name: '@',
      label: '@',
      ngModel: '=',
      ngDisabled: '=',
      ngRequired: '=',
      placeholder: '@?',
      ngValidation: '@',
      ngMinvalue: '@',
      ngMaxvalue: '@',
      ngBlur: '&?',
      ngDecimals: '@',
      icon: '<?',
  },
  require: {
    form: '^form',
  },
  template: `
     <div class="md-form-group" md-event-label>
        <input ng-required="$ctrl.ngRequired"
               ng-disabled="$ctrl.ngDisabled"
               ng-readonly="$ctrl.ngReadonly"
               ng-model="$ctrl.ngModel"
               ng-maxlength="$ctrl.ngMaxlength"
               ng-placeholder="$ctrl.placeholder"
               ng-blur="$ctrl.ngBlur()"
               name="{{$ctrl.name}}"
               type="text"
               numerico
               min="$ctrl.ngMinvalue"
               max="$ctrl.ngMaxvalue"
               decimals="$ctrl.ngDecimals"
               class="{{$ctrl.class}} {{$ctrl.icon===false?'text-right':''}}" id="{{$ctrl.name}}">
      <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="md-control-label" ng-class="{'requerido':$ctrl.ngRequired}" style="padding-right:22px">{{ $ctrl.label }}</label>
      <button ng-if="!($ctrl.icon===false)" type="button" class="btn btn-link p-0 m-0" style="position:absolute;top:5px;right:0"><i class="fa fa-money" aria-hidden="true"></i></button>
      <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
      <div ng-messages="$ctrl.form[$ctrl.name].$error" role="alert" ap-error ng-if="$ctrl.form[$ctrl.name].$touched">
        <div ng-message="required"> El campo es requerido. </div>
      </div>
    </div>`,
  controller

};

export default CInputMoneyComponent;
