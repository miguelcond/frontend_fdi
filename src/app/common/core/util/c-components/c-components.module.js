'use strict';

import CInputComponent from './c-input/c-input.component';
import CInputMoneyComponent from './c-input-money/c-input-money.component';
import CSelectComponent from './c-select/c-select.component';
import MaxLen from './c-select/maxlen.directive';
import AllowClearClass from './c-select/allow-clear-class.directive';
import CTextAreaComponent from './c-textarea/c-textarea.component';
import CCheckboxComponent from './c-checkbox/c-checkbox.component';
import CRadioButton from './c-radiobutton/c-radiobutton.component';
import CDateComponent from './c-date/c-date.component';
import CInputChipsComponent from './c-input-chips/c-input-chips.component';
import CSelectDateComponent from './c-select-date/c-select-date.component';
import CSelectDPAComponent from './c-select-dpa/c-select-dpa.component';
import ValidacionFormsService from './validacion.service';
import ButtonSpinner from './button-spinner.directive';
import ValidarFormulario from './validar-formulario.directive';
import ValidarFormularioAlerta from './validar-formulario-alerta.directive';
import CInputNumericComponent from './c-input-numeric/c-input-numeric.component';
import CTableComponent from './c-table/c-table.component';
import './c-components.scss';

// libreria mascara para el date
import uiMask from 'angular-ui-mask';

const UiComponentsModule = angular
    .module('app.ccomponents', [
      uiMask
      // CDataTable
    ])
    .component('cInput', CInputComponent)
    .component('cInputMoney',CInputMoneyComponent)
    .component('cSelect',CSelectComponent)
    .directive('maxlen', () => new MaxLen())
    .directive('allowClearClass', () => new AllowClearClass())
    .component('cCheckbox',CCheckboxComponent)
    .component('cRadiobutton',CRadioButton)
    .component('cTextarea',CTextAreaComponent)
    .component('cDate',CDateComponent)
    .component('cInputChips',CInputChipsComponent)
    .component('cSelectDate', CSelectDateComponent)
    .component('cSelectDpa', CSelectDPAComponent)
    .component('cTable', CTableComponent)
    .component('cInputNumeric', CInputNumericComponent)
    .service('ValidacionForms', ValidacionFormsService)
    .directive('buttonSpinner', () => new ButtonSpinner())
    .directive('validarFormulario', () => new ValidarFormulario())
    .directive('validarFormularioAlerta', () => new ValidarFormularioAlerta())
    .name;

export default UiComponentsModule;
