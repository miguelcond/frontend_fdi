'use strict';

import controller from './c-textarea.controller';

const CTextareaComponent = {
  bindings:{
    name: '@',
    label: '@',
    title: '@',
    ngModel: '=',
    ngDisabled: '=',
    ngRequired: '=',
    rows: '@?',
    ngMinlength: '@?',
    ngMaxlength: '@?',
    maxlength: '@?'
  },
  require:{
    form:'^form'
  },
  template:
  `   <div class="md-form-group" md-event-label>
        <textarea name="{{$ctrl.name}}"
                  ng-required="$ctrl.ngRequired"
                  ng-disabled="$ctrl.ngDisabled"
                  ng-readonly="$ctrl.ngReadonly"
                  ng-model="$ctrl.ngModel"
                  ng-minlength="$ctrl.ngMinlength"
                  ng-maxlength="$ctrl.ngMaxlength"
                  maxlength="{{$ctrl.maxlength}}"
                  rows="{{$ctrl.rows}}">
        </textarea>
        <label for="{{$ctrl.name}}" title="{{$ctrl.title}}" class="md-control-label" ng-class="{'requerido':$ctrl.ngRequired}" for="textarea">{{ $ctrl.label }}</label>
        <i class="md-bar" ng-class="{'error':$ctrl.form[$ctrl.name].$invalid && $ctrl.form[$ctrl.name].$touched}"></i>
        <div ng-messages="$ctrl.form[$ctrl.name].$error" ap-error ng-if="$ctrl.form[$ctrl.name].$touched && !$ctrl.form[$ctrl.name].$valid" role="alert">
            <div ng-message="required"> El campo es requerido. </div>
            <div ng-message="minlength"> El contenido es muy corto. </div>
            <div ng-message="maxlength"> El contenido es muy largo. </div>
        </div>
    </div>
  `,
  controller
};

export default CTextareaComponent;
