'use strict';

class CTableController {
  constructor(NgTableParams, $scope) {
    'ngInject';

    this.NgTableParams = NgTableParams;
    this.$scope = $scope;
  }

  $onInit() {
    this.initTableParams();
  }

  $onChanges() {
    this.$scope.$watch('$ctrl.dataset', () => {
      this.initTableParams();
    }, true);
  }

  initTableParams() {
    this.tableParams =  new this.NgTableParams({
        sorting: { },
      }, {
        dataset: this.dataset
      });
  }

}
export default CTableController;
