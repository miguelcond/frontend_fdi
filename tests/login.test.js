require('../node_modules/angular/angular.min.js');
require('../node_modules/angular-mocks/angular-mocks.js');

describe('Pruebas', function() {
  it('Ordenacion de un array',function() {
    var users = ['agetic', 'sistemas', 'desarrollo'];
    var sorted = users.sort();
    expect(sorted).toEqual(['agetic', 'desarrollo', 'sistemas']);
  });
});
